DROP PROCEDURE IF EXISTS `vehicle_export`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `vehicle_export`(VID TEXT, ZEITSTEMPEL TINYTEXT, PFAD TINYTEXT, TYPE TINYTEXT)
BEGIN
	DECLARE Price_In_Vendor TINYTEXT;
	DECLARE Price_In_Vendor_Calc TINYTEXT;
	DECLARE Price_In_Vendor_RegTax TINYTEXT;
	DECLARE Reserve_In_Vendor TINYTEXT;
	DECLARE Reserve_In_Vendor_Calc TINYTEXT;
	DECLARE Reserve_In_Vendor_RegTax TINYTEXT;
	DECLARE Direct_Sale_Flag TINYTEXT;
	DECLARE Lot_number TINYTEXT;
	DECLARE Branch TINYTEXT;
	DECLARE S_Date TINYTEXT;
	DECLARE S_Time TINYTEXT; 
	DECLARE S_Type TINYTEXT;
	DECLARE User_Group TINYTEXT;

	SET @i = 0;

	CASE TYPE
		WHEN 'standard' THEN
			SET Branch = " '-' ";
			SET S_Date = " '' ";
			SET S_Time = " '' ";
			SET S_Type = " '-' ";
			SET User_Group = " '-' ";
			SET Price_In_Vendor = "IF(vehicle.Owner_ID = 11 AND vehicle_details.VAT_Type_ID = 3, ROUND((price_actual.Online_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 0), ROUND(price_actual.Online_Price,0))";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.Online_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.Online_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "sale_type_template.Sale_Type_Template_Code";
			SET Lot_number = "@i := @i+1";
		WHEN 'preperation_standard' THEN
			SET Branch = "IF(peep_configuration.Branch_Code IS NOT NULL, peep_configuration.Branch_Code, '-')";
			SET S_Date = "IFNULL(IF(utilisation.Auction_End_Date > 0,DATE_FORMAT(utilisation.Auction_End_Date, '%Y%m%d'),DATE_FORMAT(peep_configuration.Sale_Date, '%Y%m%d')), '-')";
			SET S_Time = "IFNULL(IF(utilisation.Auction_End_Time > 0,DATE_FORMAT(utilisation.Auction_End_Time, '%H%i'),DATE_FORMAT(peep_configuration.Sale_Time, '%H%i')), '-')";
			SET S_Type = "IF(peep_configuration.Sale_Type IS NOT NULL , peep_configuration.Sale_Type,'-')";
			SET User_Group = "IF(peep_configuration.User_Group_Code IS NOT NULL, peep_configuration.User_Group_Code, '-')";
			SET Price_In_Vendor = "price_actual.Wholesale_Price";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.Wholesale_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.Wholesale_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "'F'";
			SET Lot_number = "@i := @i+1";
		WHEN 'wholesale' THEN
			SET Branch = "IF(peep_configuration.Branch_Code IS NOT NULL, peep_configuration.Branch_Code, '-')";
			SET S_Date = "IFNULL(IF(utilisation.Auction_End_Date > 0,DATE_FORMAT(utilisation.Auction_End_Date, '%Y%m%d'),DATE_FORMAT(peep_configuration.Sale_Date, '%Y%m%d')), '-')";
			SET S_Time = "IFNULL(IF(utilisation.Auction_End_Time > 0,DATE_FORMAT(utilisation.Auction_End_Time, '%H%i'),DATE_FORMAT(peep_configuration.Sale_Time, '%H%i')), '-')";
			SET S_Type = "IF(peep_configuration.Sale_Type IS NOT NULL , peep_configuration.Sale_Type,'-')";
			SET User_Group = "IF(peep_configuration.User_Group_Code IS NOT NULL, peep_configuration.User_Group_Code, '-')";
			SET Price_In_Vendor = "IF(vehicle.Owner_ID = 11 AND vehicle_details.VAT_Type_ID = 3, ROUND((price_actual.Wholesale_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 0), ROUND(price_actual.Wholesale_Price,0))";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.Wholesale_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.Wholesale_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "'F'";
			SET Lot_number = "@i := @i+1";
		WHEN 'bcaonline' THEN
			SET Branch = "IF(peep_configuration.Branch_Code IS NOT NULL, peep_configuration.Branch_Code, '-')";
			SET S_Date = "IFNULL(IF(utilisation.Auction_End_Date > 0,DATE_FORMAT(utilisation.Auction_End_Date, '%Y%m%d'),DATE_FORMAT(peep_configuration.Sale_Date, '%Y%m%d')), '-')";
			SET S_Time = "IFNULL(IF(utilisation.Auction_End_Time > 0,DATE_FORMAT(utilisation.Auction_End_Time, '%H%i'),DATE_FORMAT(peep_configuration.Sale_Time, '%H%i')), '-')";
			SET S_Type = "IF(peep_configuration.Sale_Type IS NOT NULL , peep_configuration.Sale_Type,'-')";
			SET User_Group = "IF(peep_configuration.User_Group_Code IS NOT NULL, peep_configuration.User_Group_Code, '-')";
			SET Price_In_Vendor = "price_actual.BCA_Online_Price";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.BCA_Online_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.BCA_Online_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "sale_type_template.Sale_Type_Template_Code";
			SET Lot_number = "@i := @i+1";
		WHEN 'bcaphysical' THEN
			SET Branch = "IF(peep_configuration.Branch_Code IS NOT NULL, peep_configuration.Branch_Code, '-')";
			SET S_Date = "IFNULL(IF(utilisation.Auction_End_Date > 0,DATE_FORMAT(utilisation.Auction_End_Date, '%Y%m%d'),DATE_FORMAT(peep_configuration.Sale_Date, '%Y%m%d')), '-')";
			SET S_Time = "IFNULL(IF(utilisation.Auction_End_Time > 0,DATE_FORMAT(utilisation.Auction_End_Time, '%H%i'),DATE_FORMAT(peep_configuration.Sale_Time, '%H%i')), '-')";
			SET S_Type = "IF(peep_configuration.Sale_Type IS NOT NULL , peep_configuration.Sale_Type,'-')";
			SET User_Group = "IF(peep_configuration.User_Group_Code IS NOT NULL, peep_configuration.User_Group_Code, '-')";
			SET Price_In_Vendor = "price_actual.BCA_Physical_Price";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.BCA_Physical_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.BCA_Physical_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "sale_type_template.Sale_Type_Template_Code";
			SET Lot_number = "@i := @i+1";
		WHEN 'peep' THEN
			SET Branch = "IF(peep_configuration.Branch_Code IS NOT NULL, peep_configuration.Branch_Code, '-')";
			SET S_Date = "IFNULL(IF(utilisation.Auction_End_Date > 0,DATE_FORMAT(utilisation.Auction_End_Date, '%Y%m%d'),DATE_FORMAT(peep_configuration.Sale_Date, '%Y%m%d')), '-')";
			SET S_Time = "IFNULL(IF(utilisation.Auction_End_Time > 0,DATE_FORMAT(utilisation.Auction_End_Time, '%H%i'),DATE_FORMAT(peep_configuration.Sale_Time, '%H%i')), '-')";
			SET S_Type = "IF(peep_configuration.Sale_Type IS NOT NULL , peep_configuration.Sale_Type,'-')";
			SET User_Group = "IF(utilisation.User_Group IS NULL OR utilisation.User_Group = '', peep_configuration.User_Group_Code, utilisation.User_Group)";
			#SET Price_In_Vendor = "ROUND(price_actual.Wholesale_Price,0)";
			SET Price_In_Vendor = "IF(vehicle.Owner_ID = 11 AND vehicle_details.VAT_Type_ID = 3, ROUND((price_actual.Wholesale_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 0), ROUND(price_actual.Wholesale_Price,0))";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.Wholesale_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 0)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.Wholesale_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "ROUND(utilisation.Bottom_Price,0)";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 0)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "'F'";
			SET Lot_number = "''";
		ELSE
			SET Branch = "";
			SET S_Date = "";
			SET S_Time = "";
			SET S_Type = "";
			SET User_Group = "";
			SET Price_In_Vendor = "price_actual.Online_Price";
			SET Price_In_Vendor_Calc = "ROUND((price_actual.Online_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Price_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(price_actual.Online_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Reserve_In_Vendor = "utilisation.Bottom_Price";
			SET Reserve_In_Vendor_Calc = "ROUND((utilisation.Bottom_Price * 100) / (IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value) + 100), 2)";
			SET Reserve_In_Vendor_RegTax = "get_Peep_Gross_Inc_RegTax(utilisation.Bottom_Price,IF(utilisation.Sold_Date, utilisation.Sold_Date, CURDATE()),vehicle.First_Reg_Date,IF(utilisation.Tax_Value IS NOT NULL, utilisation.Tax_Value, taxes.Value),reg_tax.Tax)";
			SET Direct_Sale_Flag = "sale_type_template.Sale_Type_Template_Code";
			SET Lot_number = "@i := @i+1";
	END CASE;

	SET @sql = CONCAT("
		SELECT STRAIGHT_JOIN SQL_BIG_RESULT
			",Branch," AS Branche_Code,
			",S_Date," AS Sale_Date,
			",S_Time," AS Sale_Time,
			",S_Type," AS Sale_Type,
			",Direct_Sale_Flag," AS Direct_Sale_Flag,
			utilisation.Auction_Number AS Sale_Name_Or_Number,
			IFNULL(peep_configuration.Hall_Number, \'-\') AS Hall_Name_Or_Number,
			",User_Group," AS User_Group_Code,
			vehicle.Licence_No_Actual AS Registration,
			",Lot_Number," Lot_Number,
			location.Name AS Vehicle_Location,
			vehicle.Unit_ID AS Manufacturer_Vehicle_Code,
			vehicle_type.Vehicle_Type_Code AS Vehicle_Type,
			LEFT(vehicle_make.Name, 15) AS Make,
			LEFT(vehicle_model.Name, 15) AS Model,
			LEFT(vehicle_engine.Engine_Size, 2) AS Engine_Size,
			IFNULL(LEFT(derivative.Name, 30), '-') AS Derivative,
			body_type.Body_Type_Code AS Body_Type,
			exterior.Number_Of_Doors AS Number_Of_Doors,
			IF(fuel_type.Fuel_Type_Code = 8, '-', fuel_type.Fuel_Type_Code) AS Fuel_Type,
			IF(gearbox.Gearbox_Code = 9, '-', gearbox.Gearbox_Code) AS Gearbox,
			IFNULL(peep_configuration.Market_Segment, \'-\') AS Market_Segment,
			DATE_FORMAT(vehicle.First_Reg_Date, '%Y') AS Model_Year,
			IF(color.Color_Code = 'UNK', '-', color.Color_Code) AS Body_Color,
			LEFT(color_manufacturer_translation.EN, 30) AS Manufacturer_Color,
			'-' AS Metallic_Color,
			DATE_FORMAT(vehicle.First_Reg_Date,'%Y%m%d') AS Registration_Date,
			country.Code AS Registration_Country,
			IF(vehicle_details.Imported = 0, 'N', 'Y') AS Imported,
			vehicle.Mileage_Actual AS Odometry_Reading,
			LEFT(mileage_unit.Name, 1) AS Odometry_Unit,
			'-' AS Number_Of_Hours,
			vat_type.VAT_Type_Code AS Vat_Type,
			'-' AS Gross_Weight,
			'-' AS Maximum_Load,
			vehicle_engine.Power_KW AS Power_KW,
			vehicle_engine.Power_PS AS Power_PS,
			interior.Number_Of_Seats AS Number_Of_Seats,
			cab_type.Car_Type_Code AS Cab_Type,
			IF(drive_type.Drive_Type_Code = 9, '-', drive_type.Drive_Type_Code) AS Drive_Type,
			'-' AS Wheel_Base,
			vehicle.VIN AS VIN,
			IFNULL(peep_configuration.Vendor_System_ID,\'-\') AS Vendor_System_ID,
			IFNULL(peep_configuration.Vendor_Account_No,\'-\') AS Vendor_Account_No, 
			IF(seat_covering.Seat_Covering_Code = 7, \'-\', seat_covering.Seat_Covering_Code) AS Seat_Covering,
			'-' AS Seat_Covering_Name,
			IFNULL(logbook.Loogbook_Code, '-') AS Log_Book_Present,
			IFNULL(certificate_of_conformity.Certificate_Of_Conformity_Code, '-') AS Certificate_Of_Conformity,
			vehicle_details.Number_Owners AS Number_Of_Owners,
			currency.Currency_Code AS Vendor_Currency,
		################################# Price Calculation !!! ####################################
			IF(vehicle.Owner_ID = 35,",Price_In_Vendor_Calc,",IF(vehicle.Owner_ID = 26,",Price_In_Vendor_RegTax,",",Price_In_Vendor,")) AS Price_In_Vendor_Currency,
			IF(vehicle.Owner_ID = 35,",Reserve_In_Vendor_Calc,",",Reserve_In_Vendor,") AS Reserve_In_Vendor_Currency,
			'-' AS Local_Luxury_Tax,
			IFNULL(service_history.Service_History_Code, '-') AS Service_History_Present,
			IFNULL(DATE_FORMAT(mileage_service_1.Mileage_Date,'%Y%m%d'),'-') AS Last_History_Service_Date,
			IFNULL(mileage_service_1.Value,'-') AS Last_History_Service_Odometer,
			IFNULL(mileage_cambelt_1.Value,'-') AS Last_Cambelt_Change_Odometer,
			(CASE vehicle_details.Accident_Damage WHEN 1 THEN '-' WHEN 2 THEN 'Y' WHEN 3 THEN 'N' ELSE '-' END) AS Accident_Damage,
			(CASE vehicle_details.Previous_Major_Accident_Damage WHEN 1 THEN '-' WHEN 2 THEN 'Y' WHEN 3 THEN 'N' ELSE '-' END) AS Previous_Major_Accident_Damage,
			vehicle.Damage_Cost AS Damage_Cost_External,
			'-' AS Damage_Cost_Internal,
			'-' AS Build_Date,
			'-' AS Guide_Code,
			vehicle_source.Vehicle_Source_Code AS Vehicle_Source,
			IFNULL(manufacturer_warranty.Manufacturer_Warranty_Code, 'U') AS Manufacturer_Warranty,
			IFNULL(odometer_warranty.Odometer_Warranty_Code, '-') AS Odometer_Warranty,
			'-' AS Odometer_Changed,
			IFNULL(DATE_FORMAT(vehicle_details.MOT_Date, '%Y%m%d'), '') AS MOT_Date,
			'-' AS Insurance_Total_Loss,
			'-' AS Engine_Rating_CV,
			'' AS Appraisal_URL,
			IFNULL(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(edit_vehicle_details_comment.Comment,'\"','``'),'\n',' '),'\r',''),';',' '),'  ',' '), '') AS Comments,
			vehicle_usage.Vehicle_Usage_Code AS Vehicle_Usage,
			IF(interior.Driver_Side = 0, 'L', 'R') AS Drivers_Side,
			ROUND(vehicle_engine.Engine_Size, 0) AS Engine_Size2,
			ROUND(vehicle_details.Co2, 0) AS CO2_Rating,
			emission_value.Emission_Value AS Emission_Value,
			LEFT(trim_level.Value, 33) AS Trim_Level,
			vehicle_engine.Number_Of_Gears AS Number_Of_Gears,
	#		IF(FIND_IN_SET('124', vehicle_to_equip.Equipment), 'Y', 'N') AS Anti_Lock_Braking,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 124), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 124))
							LIMIT 1),
					'N')) AS Anti_Lock_Braking,
	#		IF(FIND_IN_SET('139', vehicle_to_equip.Equipment), 'Y', 'N') AS Traction_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 139), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 139))
							LIMIT 1),
					'N')) AS Traction_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 139), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 139))
							LIMIT 1),
					'-')) AS Traction_control_Name,
	#		IF(FIND_IN_SET('123', vehicle_to_equip.Equipment), 'Y', 'N') AS Stability_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 123), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 123))
							LIMIT 1),
					'N')) AS Stability_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 123), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 123))
							LIMIT 1),
					'-')) AS Stability_Control_Name,
	#		IF(FIND_IN_SET('121', vehicle_to_equip.Equipment), 'Y', 'N') AS Airbag_Status,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 121), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 121))
							LIMIT 1),
					'N')) AS Airbag_Status,
			'0' AS Airbags,
	#		IF(FIND_IN_SET('101', vehicle_to_equip.Equipment), 'Y', 'N') AS Air_Conditioning,
			# Climate control
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 102), 'C', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 102))
							LIMIT 1),
							# Air_Conditioning
							IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 101), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 101))
							LIMIT 1),
					'N'))) AS Air_Conditioning,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 101), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 101))
							LIMIT 1),
					'-')) AS Air_Conditioning_Name,
	#		IF(FIND_IN_SET('136', vehicle_to_equip.Equipment), 'Y', 'N') AS Power_Assisted_Steering,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 136), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 136))
							LIMIT 1),
					'N')) AS Power_Assisted_Steering,
			IF(powered_windows.Powered_Windows_Code = 'U', '-', powered_windows.Powered_Windows_Code) AS Powered_Windows,
	#		IF(FIND_IN_SET('103', vehicle_to_equip.Equipment), 'Y', 'N') AS Satellite_Navigation,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 103), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 103))
							LIMIT 1),
					'N')) AS Satellite_Navigation,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 103), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 103))
							LIMIT 1),
					'-')) AS Satellite_Navigation_Name,
	#		IF(FIND_IN_SET('104', vehicle_to_equip.Equipment), 'Y', 'N') AS Radio,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 104), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 104))
							LIMIT 1),
					'N')) AS Radio,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 104), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 104))
							LIMIT 1),
					'-')) AS Radio_Name,
	#		IF(FIND_IN_SET('105', vehicle_to_equip.Equipment), 'Y', 'N') AS Cassette_Tape_Player,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 105), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 105))
							LIMIT 1),
					'N')) AS Cassette_Tape_Player,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 105), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 105))
							LIMIT 1),
					'-')) AS Cassette_Name,
	#		IF(FIND_IN_SET('106', vehicle_to_equip.Equipment), 'Y', 'N') AS CD_Single,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 106), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 106))
							LIMIT 1),
					'N')) AS CD_Single,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 106), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 106))
							LIMIT 1),
					'-')) AS CD_Name,
	#		IF(FIND_IN_SET('112', vehicle_to_equip.Equipment), 'Y', 'N') AS Alloys_Wheels,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 112), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 112))
							LIMIT 1),
					'N')) AS Alloys_Wheels,
			'-' AS Alloy_Wheel_Size,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 112), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 112))
							LIMIT 1),
					'-')) AS Alloy_Wheel_Name,
	#		IF(FIND_IN_SET('110', vehicle_to_equip.Equipment), 'Y', 'N') AS Xenon_Headlamps,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 110), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 110))
							LIMIT 1),
					'N')) AS Xenon_Headlamps,
	#		IF(FIND_IN_SET('129', vehicle_to_equip.Equipment), 'Y', 'N') AS Front_Fog_Lamps,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 129), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 129))
							LIMIT 1),
					'N')) AS Front_Fog_Lamps,
			IF(vehicle_accessories.Number_Keys > 1,'Y','N') AS 2nd_Key_Available,
	#		IF(FIND_IN_SET('119', vehicle_to_equip.Equipment), 'Y', 'N') AS Central_Locking,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 119), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 119))
							LIMIT 1),
					'N')) AS Central_Locking,
	#		IF(FIND_IN_SET('120', vehicle_to_equip.Equipment), 'Y', 'N') AS Remote_Central_Locking,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 120), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 120))
							LIMIT 1),
					'N')) AS Remote_Central_Locking,
	#		IF(FIND_IN_SET('137', vehicle_to_equip.Equipment), 'Y', 'N') AS Sports_Seats,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 137), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 137))
							LIMIT 1),
					'N')) AS Sports_Seats,
	#		IF(FIND_IN_SET('138', vehicle_to_equip.Equipment), 'Y', 'N') AS Split_Folding_Rear_Seats,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 138), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 138))
							LIMIT 1),
					'N')) AS Split_Folding_Rear_Seats,
	#		IF(FIND_IN_SET('130', vehicle_to_equip.Equipment), 'Y', 'N') AS Electrically_Adjustable_Seats,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 130), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 130))
							LIMIT 1),
					'N')) AS Electrically_Adjustable_Seats,
			IF(seat_heating.Seat_Heating_Code = 'U', '-', seat_heating.Seat_Heating_Code) AS Seat_Heating,
	#		IF(FIND_IN_SET('131', vehicle_to_equip.Equipment), 'Y', 'N') AS Privacy_Glass,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 131), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 131))
							LIMIT 1),
					'N')) AS Privacy_Glass,
	#		IF(FIND_IN_SET('132', vehicle_to_equip.Equipment), 'Y', 'N') AS Rear_Sunblind,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 132), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 132))
							LIMIT 1),
					'N')) AS Rear_Sunblind,
	#		IF(FIND_IN_SET('118', vehicle_to_equip.Equipment), 'Y', 'N') AS Heated_Windscreen,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 118), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 118))
							LIMIT 1),
					'N')) AS Heated_Windscreen,
	#		IF(FIND_IN_SET('122', vehicle_to_equip.Equipment), 'Y', 'N') AS Sunroof,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 122), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 122))
							LIMIT 1),
					'N')) AS Sunroof,
	#		IF(FIND_IN_SET('125', vehicle_to_equip.Equipment), 'Y', 'N') AS Remote_Adjusting_Wing_Mirrors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 125), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 125))
							LIMIT 1),
					'N')) AS Remote_Adjusting_Wing_Mirrors,
	#		IF(FIND_IN_SET('126', vehicle_to_equip.Equipment), 'Y', 'N') AS Electrically_Foldig_Wing_Mirrors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 126), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 126))
							LIMIT 1),
					'N')) AS Electrically_Foldig_Wing_Mirrors,
	#		IF(FIND_IN_SET('127', vehicle_to_equip.Equipment), 'Y', 'N') AS Heated_Wing_Mirrors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 127), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 127))
							LIMIT 1),
					'N')) AS Heated_Wing_Mirrors,
	#		IF(FIND_IN_SET('111', vehicle_to_equip.Equipment), 'Y', 'N') AS Cruise_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 111), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 111))
							LIMIT 1),
					'N')) AS Cruise_Control,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 111), vehicle_to_equipment.Equipment_Name, 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 111))
							LIMIT 1),
					'-')) AS Cruise_Control_Name,
	#		IF(FIND_IN_SET('128', vehicle_to_equip.Equipment), 'Y', 'N') AS Rain_Sensitive_Windscreen_Wipers,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 128), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 128))
							LIMIT 1),
					'N')) AS Rain_Sensitive_Windscreen_Wipers,
	#		IF(FIND_IN_SET('116', vehicle_to_equip.Equipment), 'Y', 'N') AS Front_Parking_Sensors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 116), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 116))
							LIMIT 1),
					'N')) AS Front_Parking_Sensors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 116), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 116))
							LIMIT 1),
					'-')) AS Front_Parking_Sensors_Name,
	#		IF(FIND_IN_SET('117', vehicle_to_equip.Equipment), 'Y', 'N') AS Rear_Parking_Sensors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 117), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 117))
							LIMIT 1),
					'N')) AS Rear_Parking_Sensors,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 117), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 117))
							LIMIT 1),
					'-')) AS Rear_Parking_Sensors_Name,
	#		IF(FIND_IN_SET('107', vehicle_to_equip.Equipment), 'Y', 'N') AS CD_Multichanger,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 107), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 107))
							LIMIT 1),
					'N')) AS CD_Multichanger,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 107), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 107))
							LIMIT 1),
					'-')) AS CD_Multichanger_Name,
	#		IF(FIND_IN_SET('142', vehicle_to_equip.Equipment), 'Y', 'N') AS Bluetooth_Telephone_Interface,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 142), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 142))
							LIMIT 1),
					'N')) AS Bluetooth_Telephone_Interface,
	#		IF(FIND_IN_SET('143', vehicle_to_equip.Equipment), 'Y', 'N') AS Enhanced_Speaker_System,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 143), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 143))
							LIMIT 1),
					'N')) AS Enhanced_Speaker_System,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 143), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 143))
							LIMIT 1),
					'-')) AS Enhanced_Speaker_System_Name,
			'-' AS Full_Size_Spare_Wheel_Alloy,
			'-' AS Full_Size_Spare_Wheel_Steel,
			'-' AS Space_Saver_Spare_Wheel,
			cabin_trim_inlay.Cabin_Trim_Inlay_Code AS Cabin_Trim_Inlay,
			'-' AS Cabin_Trim_Inlay_Name,
	#		IF(FIND_IN_SET('140', vehicle_to_equip.Equipment), 'Y', 'N') AS Headlamp_Wash_Pipe,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 140), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 140))
							LIMIT 1),
					'N')) AS Headlamp_Wash_Pipe,
	#		IF(FIND_IN_SET('141', vehicle_to_equip.Equipment), 'Y', 'N') AS Roof_Rails,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 141), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 141))
							LIMIT 1),
					'N')) AS Roof_Rails,
	#		IF(FIND_IN_SET('115', vehicle_to_equip.Equipment), 'Y', 'N') AS Tow_Ball,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 115), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 115))
							LIMIT 1),
					'N')) AS Tow_Ball,
	#		IF(FIND_IN_SET('134', vehicle_to_equip.Equipment), 'Y', 'N') AS Diesel_Particulate_Filter,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 134), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 134))
							LIMIT 1),
					'N')) AS Diesel_Particulate_Filter,
	#		IF(FIND_IN_SET('146', vehicle_to_equip.Equipment), 'Y', 'N') AS Engine_Pre_Heating,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 146), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 146))
							LIMIT 1),
					'N')) AS Engine_Pre_Heating,
	#		IF(FIND_IN_SET('135', vehicle_to_equip.Equipment), 'Y', 'N') AS Integrated_Child_Seats,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 135), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 135))
							LIMIT 1),
					'N')) AS Integrated_Child_Seats,
	#		IF(FIND_IN_SET('145', vehicle_to_equip.Equipment), 'Y', 'N') AS Tachograph_Fitted,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 145), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 145))
							LIMIT 1),
					'N')) AS Tachograph_Fitted,
	#		IF(FIND_IN_SET('144', vehicle_to_equip.Equipment), 'Y', 'N') AS Rear_DVD_System,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 144), 'Y', 'N')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 144))
							LIMIT 1),
					'N')) AS Rear_DVD_System,
			(SELECT IFNULL((SELECT IF((vehicle_to_equipment.Equipment_ID = 144), vehicle_to_equipment.Equipment_Name, '-')
							FROM vehicle_to_equipment
							WHERE ((vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID)
							AND (vehicle_to_equipment.Equipment_ID = 144))
							LIMIT 1),
					'-')) AS Package_Name,
			'-' AS Leather_Steering_Wheel,
			IF(extra_wheel.Extra_Wheel_Code = 'U', '-', extra_wheel.Extra_Wheel_Code) AS Extra_Wheel,
			IF(machinery_type.Machinery_Type_Code = 'U', '-', machinery_type.Machinery_Type_Code) AS Machinery_Type,
			'-' AS Machinery_Engine_Make,
			'-' AS Machinery_Alternator_Make,
			'-' AS Machinery_Power_Output,
			'-' AS Machinery_Power_Output_Unit,
			'-' AS Number_Of_Axels,
			'-' AS Cargo_Space_Dimensions,
			IF(hgv_suspension.HGV_Suspension_Code = 'U', '-', hgv_suspension.HGV_Suspension_Code) AS Suspension,
			'-' AS Bogie_AHK,
			'-' AS Circuit
	#		vehicle.Vehicle_ID
		FROM vehicle
			LEFT JOIN peep_configuration ON (peep_configuration.Partner_ID = vehicle.Owner_ID)
			JOIN country ON (vehicle.First_Registration_Country = country.Country_ID)
			JOIN mileage_unit ON (vehicle.Mileage_Unit_ID = mileage_unit.Mileage_Unit_ID)
			JOIN color ON (vehicle.Color_ID = color.Color_ID)
			JOIN currency ON (vehicle.Currency_ID = currency.Currency_ID)
			JOIN price_actual ON (vehicle.Vehicle_ID = price_actual.Vehicle_ID)
			LEFT JOIN edit_vehicle_details_comment ON (vehicle.Vehicle_ID = edit_vehicle_details_comment.Vehicle_ID)
	#		LEFT JOIN vehicle_to_equip ON (vehicle.Vehicle_ID = vehicle_to_equip.Vehicle_ID)
			JOIN vehicle_to_location ON (vehicle.Vehicle_ID = vehicle_to_location.Vehicle_ID AND vehicle_to_location.Active = 1)
				JOIN location ON (vehicle_to_location.Location_ID = location.Location_ID)
			JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
				JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
			JOIN make_to_model ON (vehicle.Make_To_Model_ID = make_to_model.Make_To_Model_ID)
				JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
				JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
			LEFT JOIN utilisation ON (vehicle.Vehicle_ID = utilisation.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID != 4)
				LEFT JOIN sale_type_template ON (utilisation.Sale_Type_Template_ID = sale_type_template.Sale_Type_Template_ID)
			JOIN partner ON (vehicle.Owner_ID = partner.Partner_ID)
				JOIN address ON (partner.Address_ID = address.Address_ID)
					JOIN country partner_country ON (address.Country_ID = partner_country.Country_ID)
						JOIN taxes ON (partner_country.Country_ID = taxes.Country_ID)
			JOIN vehicle_details ON (vehicle.Vehicle_ID = vehicle_details.Vehicle_ID)
				JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
				JOIN vehicle_type ON (vehicle_details.Vehicle_Type_ID = vehicle_type.Vehicle_Type_ID)
				JOIN vat_type ON (vehicle_details.VAT_Type_ID = vat_type.VAT_Type_ID)
				LEFT JOIN vehicle_source ON (vehicle_details.Vehicle_Source_ID = vehicle_source.Vehicle_Source_ID)
				JOIN vehicle_usage ON (vehicle_details.Vehicle_Usage_ID = vehicle_usage.Vehicle_Usage_ID)
				JOIN trim_level ON (vehicle_details.Trim_Level_ID = trim_level.Trim_Level_ID)
				JOIN machinery_type ON (vehicle_details.Machinery_Type = machinery_type.Machinery_Type_ID)
				JOIN vehicle_engine ON (vehicle_details.Vehicle_Details_ID = vehicle_engine.Vehicle_Details_ID)
					JOIN fuel_type ON (vehicle_engine.Fuel_Type_ID = fuel_type.Fuel_Type_ID)
					JOIN gearbox ON (vehicle_engine.Gearbox_ID = gearbox.Gearbox_ID)
					JOIN drive_type ON (vehicle_engine.Drive_Type_ID = drive_type.Drive_Type_ID)
					JOIN emission_value ON (vehicle_engine.Emission_Value_ID = emission_value.Emission_Value_ID)
				JOIN interior ON (vehicle_details.Vehicle_Details_ID = interior.Vehicle_Details_ID)
					JOIN cab_type ON (interior.Cab_Type_ID = cab_type.Cab_Type_ID)
					JOIN seat_covering ON (interior.Seat_Covering_ID = seat_covering.Seat_Covering_ID)
					JOIN seat_heating ON (interior.Seat_Heating_ID = seat_heating.Seat_Heating_ID)
					JOIN cabin_trim_inlay ON (interior.Cabin_Trim_Inlay_ID = cabin_trim_inlay.Cabin_Trim_Inlay_ID)
				JOIN exterior ON (vehicle_details.Vehicle_Details_ID = exterior.Vehicle_Details_ID)
					JOIN body_type ON (exterior.Body_Type_ID = body_type.Body_Type_ID)
					JOIN powered_windows ON (exterior.Powered_Windows_ID = powered_windows.Powered_Windows_ID)
					JOIN extra_wheel ON (exterior.Extra_Wheel_ID = extra_wheel.Extra_Wheel_ID)
					JOIN hgv_suspension ON (exterior.HGV_Suspension_ID = hgv_suspension.HGV_Suspension_ID)
				JOIN vehicle_accessories ON (vehicle_details.Vehicle_Details_ID = vehicle_accessories.Vehicle_Details_ID)
					JOIN logbook ON (vehicle_accessories.Logbook_Present = logbook.Logbook_ID)
					JOIN certificate_of_conformity ON (vehicle_accessories.Certificate_Of_Conformity_ID = certificate_of_conformity.Certificate_Of_Conformity)
					JOIN service_history ON (vehicle_accessories.Service_History_ID = service_history.Service_History_ID)
					JOIN manufacturer_warranty ON (vehicle_accessories.Manufacturer_Warranty = manufacturer_warranty.Manufacturer_Warranty_ID)
					JOIN odometer_warranty ON (vehicle_accessories.Odometer_Warranty_ID = odometer_warranty.Odometer_Warranty_ID)
			LEFT JOIN mileage AS mileage_service_1 ON (mileage_service_1.Vehicle_ID = vehicle.Vehicle_ID AND mileage_service_1.Mileage_Type_ID = 3)
				LEFT JOIN mileage AS mileage_service_2 ON (mileage_service_2.Vehicle_ID = vehicle.Vehicle_ID AND mileage_service_1.Mileage_ID < mileage_service_2.Mileage_ID  AND mileage_service_2.Mileage_Type_ID = 3)
			LEFT JOIN mileage AS mileage_cambelt_1 ON (mileage_cambelt_1.Vehicle_ID = vehicle.Vehicle_ID AND mileage_cambelt_1.Mileage_Type_ID = 4)
				LEFT JOIN mileage AS mileage_cambelt_2 ON (mileage_cambelt_2.Vehicle_ID = vehicle.Vehicle_ID AND mileage_cambelt_1.Mileage_ID < mileage_cambelt_2.Mileage_ID  AND mileage_cambelt_2.Mileage_Type_ID = 4)
				LEFT JOIN reg_tax ON (vehicle_details.Co2 BETWEEN reg_tax.CO2_Min AND reg_tax.CO2_Max)
			");

	IF TYPE != 'preperation_standard' THEN
		SET @sql = CONCAT(@sql,'
				WHERE
				(FIND_IN_SET(vehicle.Vehicle_ID, \'',VID,'\') > 0)
				AND IFNULL(mileage_service_2.Mileage_ID, 0) = 0
				AND IFNULL(mileage_cambelt_2.Mileage_ID, 0) = 0
				AND IFNULL(utilisation.Utilisation_ID,0) != 0
		');		
	ELSE
		SET @sql = CONCAT(@sql,'
				WHERE
				(FIND_IN_SET(vehicle.Vehicle_ID, \'',VID,'\') > 0)
				AND IFNULL(mileage_service_2.Mileage_ID, 0) = 0
				AND IFNULL(mileage_cambelt_2.Mileage_ID, 0) = 0
		');
	END IF;
	
	SET @sql = CONCAT(@sql,"
		INTO OUTFILE '",PFAD,"peep_csv_export_",ZEITSTEMPEL,".csv'
		FIELDS TERMINATED BY ','
		ENCLOSED BY '\"'
		LINES TERMINATED BY '\r\n'
	");
	
	#SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
