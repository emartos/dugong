DROP PROCEDURE IF EXISTS `vehicle_delete`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `vehicle_delete`(VID INT)
BEGIN
	DELETE FROM cap_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM car_group_actual WHERE Vehicle_ID = VID;
	DELETE FROM car_group_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM car_hold_actual WHERE Vehicle_ID = VID;
	DELETE FROM car_hold_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM cost_center_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM days_in_fleet_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM days_location WHERE Vehicle_ID = VID;
	DELETE FROM days_movement WHERE Vehicle_ID = VID;
	DELETE FROM days_revenue WHERE Vehicle_ID = VID;
	DELETE FROM delivery_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM depreciation_amount WHERE Vehicle_ID = VID;
	DELETE FROM depreciation_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM due_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM due_location WHERE Vehicle_ID = VID;
	DELETE FROM due_time WHERE Vehicle_ID = VID;
	DELETE FROM fleet_handling_data USING fleet_handling_data, fleet_handling WHERE fleet_handling.Vehicle_ID = VID AND fleet_handling.Fleet_Handling_ID = fleet_handling_data.Fleet_Handling_ID;
	DELETE FROM fleet_handling WHERE Vehicle_ID = VID;
	DELETE FROM inservice_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM last_departure_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM last_location_actual WHERE Vehicle_ID = VID;
	DELETE FROM last_location WHERE Vehicle_ID = VID;
	DELETE FROM last_movement_number WHERE Vehicle_ID = VID;
	DELETE FROM mileage WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_model_code WHERE Vehicle_ID = VID;
	DELETE FROM movetype_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM operational_days WHERE Vehicle_ID = VID;
	DELETE FROM operational_status_actual WHERE Vehicle_ID = VID;
	DELETE FROM operational_status_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM own_days WHERE Vehicle_ID = VID;
	DELETE FROM price_actual WHERE Vehicle_ID = VID;
	DELETE FROM assumed_value_to_model_code USING assumed_value_to_model_code, price WHERE price.Vehicle_ID = VID AND assumed_value_to_model_code.Price_ID = price.Price_ID;
	DELETE FROM price WHERE Vehicle_ID = VID;
	DELETE FROM pricing_vehicle_status WHERE Vehicle_ID = VID;
	DELETE FROM purchase_order_number_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM revenue_date_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM sales_values WHERE Vehicle_ID = VID;
	DELETE FROM sale_doc_number WHERE Vehicle_ID = VID;
	DELETE FROM sale_invoice_number_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM sale_type_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM service_inspection_status USING service_inspection_status, service_inspection WHERE Vehicle_ID = VID AND service_inspection_status.Service_Inspection_ID = service_inspection.Service_Inspection_ID;
	DELETE FROM service_inspection WHERE Vehicle_ID = VID;
	DELETE FROM type_vente_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM sales_channels_visibility USING sales_channels_visibility, sales_channels_to_utilisation, utilisation WHERE utilisation.Vehicle_ID = VID AND sales_channels_to_utilisation.Utilization_ID = utilisation.Utilisation_ID AND sales_channels_visibility.Sales_Channels_To_Utilisation_ID = sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID;
	DELETE FROM sales_channels_to_utilisation USING sales_channels_to_utilisation, utilisation WHERE utilisation.Vehicle_ID = VID AND sales_channels_to_utilisation.Utilization_ID = utilisation.Utilisation_ID;
	DELETE FROM utilisation_buyer USING utilisation_buyer, utilisation WHERE utilisation.Vehicle_ID = VID AND utilisation_buyer.Utilisation_ID = utilisation.Utilisation_ID;
	DELETE FROM vehicle_to_package USING vehicle_to_package, utilisation WHERE utilisation.Vehicle_ID = VID AND vehicle_to_package.Utilisation_ID = utilisation.Utilisation_ID;
	DELETE FROM utilisation_document USING utilisation_document, utilisation WHERE utilisation.Vehicle_ID = VID AND utilisation_document.Utilisation_ID = utilisation.Utilisation_ID;
	DELETE FROM utilisation_provisional_feedback USING utilisation_provisional_feedback, utilisation WHERE utilisation.Vehicle_ID = VID AND utilisation_provisional_feedback.Utilisation_ID = utilisation.Utilisation_ID;
	DELETE FROM utilisation WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_class_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_costs WHERE Vehicle_ID = VID;
	DELETE FROM exterior USING exterior, vehicle_details WHERE Vehicle_ID = VID AND exterior.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID;
	DELETE FROM interior USING interior, vehicle_details WHERE Vehicle_ID = VID AND interior.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID;
	DELETE FROM vehicle_accessories USING vehicle_accessories, vehicle_details WHERE Vehicle_ID = VID AND vehicle_accessories.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID;
	DELETE FROM power_to_vehicle_engine USING power_to_vehicle_engine, vehicle_engine, vehicle_details WHERE vehicle_details.Vehicle_ID = VID AND power_to_vehicle_engine.Vehicle_Engine_ID = vehicle_engine.Vehicle_Engine_ID AND vehicle_engine.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID;
	DELETE FROM vehicle_engine USING vehicle_engine, vehicle_details WHERE Vehicle_ID = VID AND vehicle_engine.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID;
	DELETE FROM model_code_translation USING model_code_translation, vehicle_details WHERE vehicle_details.Vehicle_ID = VID AND vehicle_details.Vehicle_Details_ID = model_code_translation.Vehicle_Details_ID;
	DELETE FROM vehicle_details WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_equipment WHERE Vehicle_ID = VID;
	DELETE FROM parking_lot_to_location USING parking_lot_to_location, vehicle_to_location WHERE vehicle_to_location.Vehicle_ID = VID AND parking_lot_to_location.Vehicle_To_Location_ID = vehicle_to_location.Vehicle_To_Location_ID;
	DELETE FROM vehicle_to_location WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_package WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_status WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_vehicle_status_partner WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_licence_no WHERE Vehicle_ID = VID;
	DELETE FROM workflow_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM package_changes_history WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_offer_history WHERE Vehicle_ID = VID;
	DELETE FROM price_suggested WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_damage WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_to_document WHERE Vehicle_ID = VID;
	DELETE FROM guide_price_to_vehicle_historie WHERE Vehicle_ID = VID;
	DELETE FROM guide_price_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM change_to_vehicle WHERE Vehicle_ID = VID;
	DELETE FROM vehicle_additional_values WHERE Vehicle_ID = VID;
	DELETE FROM edit_vehicle_details_comment WHERE Vehicle_ID = VID;
	DELETE FROM pickup_document WHERE Vehicle_ID = VID;
	DELETE FROM service_logistic_status USING service_logistic_status, logistic WHERE Vehicle_ID = VID AND service_logistic_status.Logistic_ID = logistic.Logistic_ID;
	DELETE FROM logistic WHERE Vehicle_ID = VID;
	DELETE FROM vehicle WHERE Vehicle_ID = VID;
END |;
DELIMITER ;
