DROP FUNCTION IF EXISTS `get_sold_price_goss_incentive`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_sold_price_goss_incentive`(SALEPRICEGROSS DECIMAL(10,2), TAXES DECIMAL(10,2), INCENTIVE DECIMAL(12,4), PID INT, VEHICLE_ID INT, FIRST_REG_DATE DATE,SOLD_DATE DATE, BUYERTYPE INT, VEHICLECOUNT INT, TAXESID INT ,PRICE_TYPE INT) RETURNS decimal(10,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE Sold_Price_Gross DECIMAL(10,2) DEFAULT NULL;
	CASE PID
		# Wenn Hertz Italien
		WHEN 10 THEN
			CASE PRICE_TYPE
					# Case pricing type NET
					WHEN 1 THEN 
						#SELECT ROUND (get_sold_price_net(SALEPRICEGROSS, TAXES, PID, VEHICLE_ID, BUYERTYPE, VEHICLECOUNT, TAXESID), 2) INTO Sold_Price_Gross;
						#############
						CASE BUYERTYPE
							# Case BUYERTYPE 2 buyer_type_export
							WHEN 2 THEN
								SELECT ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
									- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
									- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2) 
									INTO Sold_Price_Gross
								FROM tax_calculation_values
								WHERE
									Buyer_Type_ID = BUYERTYPE
									AND tax_calculation_values.Taxes_ID = TAXESID;
							# Case BUYERTYPE 3 buyer_type_national_sales_vat_exempted
							WHEN 3 THEN 
								SELECT ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
									- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
									- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2)
									+ tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted INTO Sold_Price_Gross
								FROM tax_calculation_values
								WHERE
									Buyer_Type_ID = BUYERTYPE
									AND tax_calculation_values.Taxes_ID = TAXESID;
							ELSE
								SELECT ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) INTO Sold_Price_Gross;

					END CASE;
						############
					# Case pricing type NOT NET(GROSS)
					ELSE
						SELECT ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) INTO Sold_Price_Gross;
			END CASE;
		WHEN 26 THEN
				CASE PRICE_TYPE
					# Case pricing type GROSS INC. REG
					WHEN 3 THEN 
						SELECT 
						ROUND(
							get_sold_price_vat_incentive(SALEPRICEGROSS, TAXES, PID , VEHICLE_ID , FIRST_REG_DATE ,SOLD_DATE , BUYERTYPE , VEHICLECOUNT , TAXESID ,PRICE_TYPE)
							+ IF(DATE_SUB(SOLD_DATE,INTERVAL 2 YEAR) <= FIRST_REG_DATE,((((SALEPRICEGROSS * 100) / (TAXES + 100)) * reg_tax.Tax ) / 100),0.00)
							+ ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) * 100) / (TAXES + 100)),2)
							INTO Sold_Price_Gross
						FROM vehicle_details 
							LEFT JOIN reg_tax ON (vehicle_details.Co2 BETWEEN reg_tax.CO2_Min AND reg_tax.CO2_Max)
							WHERE vehicle_details.Vehicle_ID=VEHICLE_ID;
					# Case pricing type NET
					WHEN 1 THEN 
						#SELECT ROUND (get_sold_price_net(SALEPRICEGROSS, TAXES, PID, VEHICLE_ID, BUYERTYPE, VEHICLECOUNT, TAXESID), 2) INTO Sold_Price_Gross;
						SELECT ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) INTO Sold_Price_Gross;
					# Pricing type GROSS
					ELSE
						SELECT ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) INTO Sold_Price_Gross;
				END CASE;
		ELSE
		CASE PRICE_TYPE
			# For all countries ( Case pricing type NET)
			WHEN 1 THEN 
				#SELECT ROUND (get_sold_price_net(SALEPRICEGROSS, TAXES, PID, VEHICLE_ID, BUYERTYPE, VEHICLECOUNT, TAXESID), 2) INTO Sold_Price_Gross;
				SELECT ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) INTO Sold_Price_Gross;
			ELSE
				SELECT ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) INTO Sold_Price_Gross;
		END CASE;
	END CASE;
	RETURN Sold_Price_Gross;
END |;
DELIMITER ;
