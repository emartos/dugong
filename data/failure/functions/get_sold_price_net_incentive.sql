DROP FUNCTION IF EXISTS `get_sold_price_net_incentive`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_sold_price_net_incentive`(SALEPRICEGROSS DECIMAL(10,2), TAXES DECIMAL(10,2), INCENTIVE DECIMAL(12,4), PID INT, VEHICLE_ID INT, BUYERTYPE INT, VEHICLECOUNT INT, TAXESID INT) RETURNS decimal(10,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE Sold_Price_Net DECIMAL(10,2) DEFAULT NULL;
	CASE PID
		# Wenn Hertz Italien
		WHEN 10 THEN
			#IF VEHICLECOUNT < 1 THEN SELECT ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) INTO Sold_Price_Net;
			IF VEHICLECOUNT < 1 THEN
				CASE BUYERTYPE
					#Case BUYERTYPE 1 buyer_type_retail
					WHEN 1 THEN
						SELECT ROUND(ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values_kw.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values_kw.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values_kw.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2)
							- tax_calculation_values_kw.Transfer_Of_Ownership_Vat_Applied
						
						- INCENTIVE,2)
						
						INTO Sold_Price_Net
						FROM tax_calculation_values_kw_to_vehicle
							JOIN tax_calculation_values_kw ON (tax_calculation_values_kw_to_vehicle.Tax_Calculation_Values_KW_ID = tax_calculation_values_kw.Tax_Calculation_Values_KW_ID)
						WHERE
							tax_calculation_values_kw_to_vehicle.Vehicle_ID = VEHICLE_ID;
							#SELECT ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) INTO Sold_Price_Net;
					# Case BUYERTYPE 2 buyer_type_export
					WHEN 2 THEN
						SELECT ROUND(ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2)
							############
							- tax_calculation_values.Transfer_Of_Ownership_Vat_Applied
							- tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted
							############
						
						- INCENTIVE,2)							
						
						INTO Sold_Price_Net
						FROM tax_calculation_values
						WHERE
							Buyer_Type_ID = BUYERTYPE
							AND tax_calculation_values.Taxes_ID = TAXESID;
					# Case BUYERTYPE 3 buyer_type_national_sales_vat_exempted
					WHEN 3 THEN 
						SELECT ROUND(ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2)
							+ tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted 
							################
							- tax_calculation_values.Transfer_Of_Ownership_Vat_Applied
							- tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted
							######
						
						- INCENTIVE,2)							
							INTO Sold_Price_Net
						FROM tax_calculation_values
						WHERE
							Buyer_Type_ID = BUYERTYPE
							AND tax_calculation_values.Taxes_ID = TAXESID;
					# Buyer type Null (No Buyer)
					WHEN 0 THEN 
						SELECT ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2)
				
					- INCENTIVE,2)							
						INTO Sold_Price_Net;
					# Case BUYERTYPE other 
					ELSE
						SELECT ROUND(ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted)
							- ((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) - tax_calculation_values.Transfer_Of_Ownership_Vat_Exempted) * 100) / (TAXES + 100)), 2)
							- tax_calculation_values.Transfer_Of_Ownership_Vat_Applied
							- INCENTIVE,2)							
							INTO Sold_Price_Net
						FROM tax_calculation_values
						WHERE
							Buyer_Type_ID = BUYERTYPE
							AND tax_calculation_values.Taxes_ID = TAXESID;
				END CASE;
			ELSE
				SELECT ROUND((ROUND((ROUND(ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) * ((TAXES + 100)/100),2) * 100) / (TAXES + 100),2)) - (VEHICLECOUNT * tax_calculation_values.Transfer_Of_Ownership_Vat_Applied) - INCENTIVE,2) 
				INTO Sold_Price_Net
				FROM tax_calculation_values
				WHERE
					Buyer_Type_ID = BUYERTYPE
					AND tax_calculation_values.Taxes_ID = TAXESID;
			END IF;
		ELSE
			SELECT ROUND(ROUND((SALEPRICEGROSS * 100) / (TAXES + 100),2) - INCENTIVE,2) INTO Sold_Price_Net;
	END CASE;

	RETURN Sold_Price_Net;
END |;
DELIMITER ;
