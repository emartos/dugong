DROP PROCEDURE IF EXISTS `count_communication_incoming_requests`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `count_communication_incoming_requests`(PID VARCHAR(255) , MPID VARCHAR(255))
BEGIN
	SELECT
		COUNT(v.Vehicle_ID) AS 'count'
	FROM
		vehicle AS v
        JOIN communications AS c ON (c.vehicle_id = v.vehicle_id)
        LEFT JOIN country cntr ON (cntr.Country_ID = v.First_Registration_Country)
        JOIN make_to_model mtm ON (mtm.Make_To_Model_ID = v.Make_To_Model_ID)
        JOIN vehicle_make vmk ON (mtm.Make_ID = vmk.Make_ID)
        JOIN vehicle_model vmd ON (mtm.Model_ID = vmd.Vehicle_Model_ID)
        JOIN service AS service ON (c.Service_ID = service.Service_ID)
        LEFT JOIN sichersteller AS sichersteller ON (sichersteller.service_id = service.service_id)
        LEFT JOIN service_inspection AS inspection ON (inspection.Service_ID = service.Service_ID)
        LEFT JOIN utilisation AS util ON (util.Service_ID = service.Service_ID)
        LEFT JOIN vehicle_deregistration ON (vehicle_deregistration.Service_ID = service.Service_ID)
        LEFT JOIN document_dispatch ON (document_dispatch.Service_ID = service.Service_ID),
        user AS u
	WHERE
		v.Deleted = 0
		AND c.Deleted = 0
		AND (FIND_IN_SET(service.Partner_ID,PID) > 0
			OR FIND_IN_SET(service.Client_ID,PID) > 0)
		AND c.Answer_Flag = 0
		AND u.User_ID = c.User_ID_Request
		AND u.Main_Partner_ID != MPID;
END |;
DELIMITER ;
