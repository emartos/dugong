DROP PROCEDURE IF EXISTS `count_all_vehicles`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `count_all_vehicles`(PID VARCHAR(255), VID INT)
BEGIN
	DECLARE VEHICLE VARCHAR(255);

	IF VID != 0 THEN SET VEHICLE = CONCAT(' AND v.Vehicle_ID = ',VID);
	ELSE SET VEHICLE = '';
	END IF;

	SET @sql = CONCAT("
		SELECT
			COUNT(v.Vehicle_ID) AS 'count'
		FROM
			vehicle v
		WHERE
			v.Deleted = 0
		AND FIND_IN_SET(v.Owner_ID,'",PID,"') > 0
		AND v.Vehicle_Status_Actual != 3",
		VEHICLE);
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
