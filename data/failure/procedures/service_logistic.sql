DROP PROCEDURE IF EXISTS `procedure_service_logistic`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_service_logistic`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), VISIBILITY_MODE VARCHAR(25), OPT_PID VARCHAR(255), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE FARBE TEXT;
	DECLARE DATUM VARCHAR(15);
	# Declare new var for PRICING_RESULT
	SET @PRICING_RESULT = '';
	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;
	CASE 
		WHEN CALL_TYPE = 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(vehicle.Vehicle_ID) AS 'count' ";
			SET DEFAULT_TABLES = "
					logistic
					JOIN vehicle ON (logistic.Vehicle_ID = vehicle.Vehicle_ID)
					JOIN service ON (logistic.Service_ID = service.Service_ID)
          LEFT JOIN logistic_contact ON(logistic_contact.Logistic_Contact_ID = logistic.Logistic_Contact_ID)
									";
			SET DEFAULT_ROWS = "";
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
		WHEN CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT STRAIGHT_JOIN SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_TABLES = "
					logistic
					JOIN vehicle ON (logistic.Vehicle_ID = vehicle.Vehicle_ID)
					LEFT JOIN vehicle_to_location on (vehicle_to_location.Vehicle_ID = vehicle.Vehicle_ID AND vehicle_to_location.Active = 1)
					LEFT JOIN location on (location.Location_ID = vehicle_to_location.Location_ID)	
					JOIN service ON (logistic.Service_ID = service.Service_ID)
					JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
					JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
					JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
					LEFT JOIN logistic_contact ON(logistic_contact.Logistic_Contact_ID = logistic.Logistic_Contact_ID)
					LEFT JOIN cost_account ON(cost_account.Cost_Account_ID = logistic.Cost_Account_ID)
					";
			SET DEFAULT_ROWS = "
					logistic.Delivery_Date
					,logistic.Logistic_ID
					,logistic.Pickup_Date
					,location.Location_ID
					,location.Name as location_name
					,service.Service_ID
					,service.Set_Date AS Order_Date
					,vehicle.Licence_No_Actual
					,vehicle.Unit_ID
					,vehicle.Vehicle_ID
					,vehicle.VIN
					,vehicle.Frontend_Edited
					,vehicle_make.Name AS Manufacturer
					,vehicle_model.Name AS Model
					,cost_account.Cost_Account_Name
					,CONCAT ('',
							IF (logistic_contact.Name_Pickup = '',			'', CONCAT (TRIM(logistic_contact.Name_Pickup), ', ')),
							IF (logistic_contact.First_Address_Pickup = '',	 '', CONCAT (TRIM(logistic_contact.First_Address_Pickup), ', ')),
							IF (logistic_contact.Second_Address_Pickup = '',	'', CONCAT (TRIM(logistic_contact.Second_Address_Pickup), ', ')),
							IF (logistic_contact.Third_Address_Pickup = '',	 '', CONCAT (TRIM(logistic_contact.Third_Address_Pickup), ', ')),
							IF (logistic_contact.Fourth_Address_Pickup = '',	'', CONCAT (TRIM(logistic_contact.Fourth_Address_Pickup), ', ')),
							IF (logistic_contact.Postal_Code_Pickup = '',	 '', CONCAT (TRIM(logistic_contact.Postal_Code_Pickup), ', ')),
							IF (logistic_contact.City_Pickup = '',			'', CONCAT (TRIM(logistic_contact.City_Pickup), ', ')),
							IF (logistic_contact.Country_Name_Pickup = '',	'', CONCAT (TRIM(logistic_contact.Country_Name_Pickup), ', ')),
							IF (logistic_contact.Contact_Pickup = '',		 '', CONCAT (TRIM(logistic_contact.Contact_Pickup), ', ')),
							IF (logistic_contact.Mobile_Number_Pickup = '',	 '', CONCAT (TRIM(logistic_contact.Mobile_Number_Pickup), ', ')),
							IF (logistic_contact.Phone_Number_Pickup = '',	'', TRIM(logistic_contact.Phone_Number_Pickup))
							) AS Pickup_Name
					,CONCAT ('',
							IF (logistic_contact.Name_Delivery = '',			'', CONCAT (TRIM(logistic_contact.Name_Delivery), ', ')),
							IF (logistic_contact.First_Address_Delivery = '',	 '', CONCAT (TRIM(logistic_contact.First_Address_Delivery), ', ')),
							IF (logistic_contact.Second_Address_Delivery = '',	'', CONCAT (TRIM(logistic_contact.Second_Address_Delivery), ', ')),
							IF (logistic_contact.Third_Address_Delivery = '',	 '', CONCAT (TRIM(logistic_contact.Third_Address_Delivery), ', ')),
							IF (logistic_contact.Fourth_Address_Delivery = '',	'', CONCAT (TRIM(logistic_contact.Fourth_Address_Delivery), ', ')),
							IF (logistic_contact.Postal_Code_Delivery = '',	 '', CONCAT (TRIM(logistic_contact.Postal_Code_Delivery), ', ')),
							IF (logistic_contact.City_Delivery = '',			'', CONCAT (TRIM(logistic_contact.City_Delivery), ', ')),
							IF (logistic_contact.Country_Name_Delivery = '',	'', CONCAT (TRIM(logistic_contact.Country_Name_Delivery), ', ')),
							IF (logistic_contact.Contact_Delivery = '',		 '', CONCAT (TRIM(logistic_contact.Contact_Delivery), ', ')),
							IF (logistic_contact.Mobile_Number_Delivery = '',	 '', CONCAT (TRIM(logistic_contact.Mobile_Number_Delivery), ', ')),
							IF (logistic_contact.Phone_Number_Delivery = '',	'', TRIM(logistic_contact.Phone_Number_Delivery))
							) AS Delivery_Name 
							";
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = " GROUP BY service.Service_ID ";
	WHEN CALL_TYPE = 'export' THEN
		SET DEFAULT_CALL_TYPE = "SELECT STRAIGHT_JOIN ";
		SET DEFAULT_TABLES = CONCAT(" 
					logistic
					JOIN vehicle ON (logistic.Vehicle_ID = vehicle.Vehicle_ID)
					JOIN service ON (logistic.Service_ID = service.Service_ID)
					JOIN partner AS partner_export ON (partner_export.Partner_ID = service.Partner_ID)
					JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
					JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
					JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
					JOIN vehicle_details ON (vehicle_details.Vehicle_ID = vehicle.Vehicle_ID)
					JOIN vehicle_type ON (vehicle_details.Vehicle_Type_ID = vehicle_type.Vehicle_Type_ID)
					JOIN codebase AS cb_vehicle_type ON (vehicle_type.Translation_Code = cb_vehicle_type.Translation_Code)
					JOIN service_logistic_status_template AS slst_export ON (logistic.Service_Logistic_Status_Actual = slst_export.Service_Logistic_Status_Template_ID)
					JOIN codebase cb_logistic_status_export ON (cb_logistic_status_export.Translation_Code = slst_export.Translation_Code)
					LEFT JOIN logistic_contact ON(logistic_contact.Logistic_Contact_ID = logistic.Logistic_Contact_ID)
					LEFT JOIN user AS ressource_user_export ON (ressource_user_export.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person_export ON (ressource_person_export.Person_ID = ressource_user_export.Person_ID)
					");
		SET DEFAULT_ROWS = CONCAT ("
					cb_logistic_status_export.",LANG," AS Transport_Status
					#,cb_vehicle_type.",LANG," AS Vehicle_Type
					,REPLACE(vehicle_type.Translation_Code,'vehicle_type_','') AS Vehicle_Typ_CSV
					,CONCAT(IFNULL(ressource_person_export.First_Name, \'\'), \' \', IFNULL(ressource_person_export.Last_Name, \'\')) AS Ressource_User
					,logistic.Delivery_Date
					,logistic.Distance
					,logistic.Distance_Unit
					,logistic.Effectively_Delivered
					,logistic.Effectively_Delivery_Time
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}',' | ')) AS Logistik_Comment_CSV
					,logistic.Logistic_ID
					#,logistic.Movement_Type
					,REPLACE(logistic.Movement_Type,'movement_type_','') AS Movement_Type_CSV
					,logistic.Pickup_Date
					,logistic.Pickup_Date AS Pickup_From_CSV
					,logistic.Pickup_Time
					,logistic.Effectively_Pickedup
					,logistic.Effectively_Pickup_Time
					,logistic.Transport_Type
					,logistic.Trip_Type
					,logistic.Vehicle_Condition
					,logistic_contact.City_Delivery
					,logistic_contact.City_Pickup
					,logistic_contact.Contact_Delivery
					,logistic_contact.Contact_Pickup
					#,logistic_contact.Country_Name_Delivery
					#,logistic_contact.Country_Name_Pickup
					,REPLACE(logistic_contact.Country_CB_Delivery,'country_','') AS Country_Name_Delivery
					,REPLACE(logistic_contact.Country_CB_Pickup,'country_','') AS Country_Name_Pickup
					,logistic_contact.First_Address_Delivery
					,logistic_contact.First_Address_Pickup
					,logistic_contact.Fourth_Address_Delivery
					,logistic_contact.Fourth_Address_Pickup
					,logistic_contact.Mobile_Number_Delivery
					,logistic_contact.Mobile_Number_Pickup
					,logistic_contact.Name_Delivery
					,logistic_contact.Name_Pickup
					,logistic_contact.Phone_Number_Delivery
					,logistic_contact.Phone_Number_Pickup
					,logistic_contact.Postal_Code_Delivery
					,logistic_contact.Postal_Code_Pickup
					,logistic_contact.Second_Address_Delivery
					,logistic_contact.Second_Address_Pickup
					,logistic_contact.Third_Address_Delivery
					,logistic_contact.Third_Address_Pickup
					,partner_export.Name AS Partner_Name
					,service.Budget
					,service.Reference1
					,service.Reference2
					,service.Reference3
					,service.Service_ID
					#,service.Set_Date AS Order_Date
					,LEFT(service.Set_Date,10) AS Order_Date_CSV
					,service.Workflow_ID
					,vehicle.Licence_No_Actual
					,vehicle.Mileage_Actual as Mileage
					,vehicle.Unit_ID
					,vehicle.VIN
					,vehicle_details.MOT_Date
					,vehicle_make.Name AS Manufacturer
					,vehicle_model.Name AS Model
					,vehicle.Vehicle_ID
					");
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
	END CASE;
	CASE LIST_TYPE
		WHEN 'new_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 5
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					JOIN service_logistic_status_template ON (logistic.Service_Logistic_Status_Actual = service_logistic_status_template.Service_Logistic_Status_Template_ID)
					JOIN codebase cb_logistic_status ON (cb_logistic_status.Translation_Code = service_logistic_status_template.Translation_Code)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Service_Logistic_Status_Actual
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Reference2 AS Reference
					,service.Service_Status_Actual
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicleuser.User_ID
					");
	WHEN 'all_orders_hfp' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 1
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Distance
					,logistic.Distance_Unit
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Transport_Type
					,logistic.Trip_Type
					,service.Budget
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,vehicle.Licence_No_Actual
					,@PRICING_RESULT := get_best_logistic('",PID,"', logistic.Transport_Type, logistic.Distance, logistic.Distance_Unit, service.Cost_Currency_ID) AS best_logistic_info
					,SUBSTRING(@PRICING_RESULT FROM 1 FOR LOCATE('|', @PRICING_RESULT) -1) AS Best_Price
					,SUBSTRING( @PRICING_RESULT FROM LOCATE('|',@PRICING_RESULT) +1) AS Best_Partner
					");
		WHEN 'amended_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND (logistic.Service_Logistic_Status_Actual = 5 OR logistic.Service_Logistic_Status_Actual = 2)
					AND service.Service_Status_Actual = 45
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson on (vehicleuser.Person_ID = vehicleperson.Person_ID)
					JOIN service_logistic_status_template slst ON (slst.Service_Logistic_Status_Template_ID = logistic.Service_Logistic_Status_Actual)
					JOIN codebase cb_transport_status ON (cb_transport_status.Translation_Code = slst.Translation_Code)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,cb_transport_status.",LANG," AS Logistic_Status
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, \'-\') AS Income_Type
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Service_Status_Actual
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicleuser.User_ID
					");
		WHEN 'new_request_quote' THEN 
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER," 
					AND logistic.Service_Logistic_Status_Actual = 9 
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES," 
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					"); 
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Service_Status_Actual
					,service.Workflow_ID
					,vehicleuser.User_ID
					"); 
		WHEN 'answered_quote_requests' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER," 
					AND logistic.Service_Logistic_Status_Actual = 10
					"); 
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, " 
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
									"); 
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL (service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Workflow_ID
					,vehicleuser.User_ID
					");
		WHEN 'offer_not_accepted' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER," 
					AND logistic.Service_Logistic_Status_Actual = 11 
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL (service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Workflow_ID
					,vehicleuser.User_ID
					");
		WHEN 'break_off_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 12
					AND service.Service_Status_Actual = 44
					AND IFNULL(st1.Service_Status_ID,0) = 0
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN service_status ON (service.Service_ID = service_status.Service_ID AND	service_status.Service_Status_Template_ID = 44)
					LEFT JOIN service_status st1 ON (service.Service_ID = st1.Service_ID AND st1.Service_Status_ID > service_status.Service_Status_ID AND st1.Service_Status_Template_ID = 44)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Workflow_ID
					,service_status.Comment AS Service_Comment
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Vehicle_Physical_Status
					,vehicleuser.User_ID
					");
		WHEN 'new_orders_hfp' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 5
					");
		SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "			
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Active
					,client.Name AS Client_Name
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Customer_Price
					,logistic.Distance_Changed
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Trip_Type
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Order_Number
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,vehicle.Licence_No_Actual
					");
		WHEN 'all_logistic' THEN
		SET FARBE = get_translation_string(LANG, 'color');
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
					JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
					JOIN color ON (vehicle.Color_ID = color.Color_ID)
					JOIN codebase cb_color ON (color.Translation_Code = cb_color.Translation_Code)
					LEFT JOIN vehicle_details ON (vehicle_details.Vehicle_ID = vehicle.Vehicle_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN service_invoice_status ON (service_invoice_status.Service_Invoice_Status_ID = service.Invoice_Customer_Status_ID)
					LEFT JOIN derivative ON (derivative.Derivative_ID = vehicle_details.Derivative_ID)
					LEFT JOIN company_to_address AS company_vehicle_to_address ON (company_vehicle_to_address.Company_ID = company_vehicle.Company_ID)
					LEFT JOIN address AS company_vehicle_address ON (company_vehicle_address.Address_ID = company_vehicle_to_address.Address_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,",FARBE,"
					,budget_currency.Currency_Code
					,company_vehicle_address.External_ID AS External_Customer_ID
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,derivative.Name AS Derivative
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Customer_Price
					,logistic.Delivery_Address_ID
					,logistic.Delivery_Time
					,logistic.Distance
					,logistic.Distance_Unit
					,logistic.Effectively_Delivered
					,logistic.Effectively_Pickedup
					,logistic.Income_Price
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Pickup_Address_ID
					,logistic.Pickup_ID
					,logistic.Pickup_Time
					,logistic.Pickup_Type
					,logistic.Transport_Type
					,logistic_contact.Postal_Code_Pickup AS Postal_Code
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Client_ID
					,service.Contact_ID
					,service.Cost AS Transport_Costs
					,service.External_ID
					,service.Invoice_Customer_Status_ID
					,service.Invoice_Partner
					,service.Partner_ID
					,service.Recepient_of_Invoice_Address_ID
					,service.Reference2 AS SalesForceID
					,service_invoice_status.Status
					,vehicle.First_Reg_Date
					,vehicle.Licence_No_Actual
					,vehicle.Mileage_Actual AS Odometry
					,vehicle.Owner_ID AS Fleet_Owner
					");
		WHEN 'all_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 1
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,vehicleuser.User_ID
					");
		WHEN 'allocate' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 1
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_invoice_status ON (service.Invoice_Customer_Status_ID = service_invoice_status.Service_Invoice_Status_ID AND service.Service_ID = service_invoice_status.Service_ID AND service_invoice_status.Deleted = 0)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,logistic.Distance
					,logistic.Distance_Unit
					,logistic.Logistic_Intern_Comment
					,logistic.Transport_Type
					,logistic.Trip_Type
					,service.Budget
					,service.Prepayment
					,service_invoice_status.Status
					,@PRICING_RESULT := get_best_logistic('",PID,"', logistic.Transport_Type, logistic.Distance, logistic.Distance_Unit, service.Cost_Currency_ID) AS best_logistic_info
					,SUBSTRING(@PRICING_RESULT FROM 1 FOR LOCATE('|', @PRICING_RESULT) -1) AS Best_Price
					,SUBSTRING( @PRICING_RESULT FROM LOCATE('|',@PRICING_RESULT) +1) AS Best_Partner
					");
		WHEN 'reallocate_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 3
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,logistic.Distance
					,logistic.Distance_Unit
					,logistic.Logistic_Intern_Comment
					,logistic.Transport_Type
					,logistic.Trip_Type
					,service.Budget
					,@PRICING_RESULT := get_best_logistic('",PID,"', logistic.Transport_Type, logistic.Distance, logistic.Distance_Unit, service.Cost_Currency_ID) AS best_logistic_info
					,SUBSTRING(@PRICING_RESULT FROM 1 FOR LOCATE('|', @PRICING_RESULT) -1) AS Best_Price
					,SUBSTRING(@PRICING_RESULT FROM LOCATE('|',@PRICING_RESULT) +1) AS Best_Partner
					");
		WHEN 'accepted_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 2
					AND IFNULL(uc.User_Communication_ID ,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Active
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Distance
					,logistic.Distance_Unit
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Ressource_User_ID
					,logistic.Service_Logistic_Status_Actual
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Reference2 AS Reference
					,service.Service_Status_Actual
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Licence_No_Actual
					,vehicleuser.User_ID
					");
		WHEN 'accepted_orders_overdue' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 2
					AND TIMESTAMPDIFF(MINUTE,CONCAT_WS(' ', Pickup_Date, Pickup_Time), NOW()) > 0
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(ressource_person.First_Name, '') AS Ressource_First_Name
					,IFNULL(ressource_person.Last_Name , '') AS Ressource_Last_Name
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Distance
					,logistic.Distance_Unit
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Reference2 AS Reference
					,service.Service_Status_Actual
					,service.Workflow_ID 
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Licence_No_Actual
					,vehicleuser.User_ID
					");		
		WHEN 'on_transport' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 7
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Active
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(ressource_person.First_Name, '') AS Ressource_First_Name
					,IFNULL(ressource_person.Last_Name , '') AS Ressource_Last_Name
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Distance
					,logistic.Distance_Unit
					,logistic.Effectively_Pickedup
					,logistic.Effectively_Pickup_Time
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Licence_No_Actual
					,vehicleuser.User_ID
					,logistic.Ressource_User_ID
					");
		WHEN 'on_transport_overdue' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 7
					AND TIMESTAMPDIFF(MINUTE,DATE(CONCAT_WS(\' \', logistic.Delivery_Date, IFNULL(Delivery_Time,\'00:00:00\'))), now()) > 0
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(ressource_person.First_Name, '') AS Ressource_First_Name
					,IFNULL(ressource_person.Last_Name , '') AS Ressource_Last_Name
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Distance
					,logistic.Distance_Unit
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Licence_No_Actual
					,vehicleuser.User_ID
									");
		WHEN 'rejected_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 3
					AND IFNULL(uc.User_Communication_ID,0) = 0
									");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
									");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(ressource_person.First_Name, '') AS Ressource_First_Name
					,IFNULL(ressource_person.Last_Name , '') AS Ressource_Last_Name
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Distance
					,logistic.Distance_Unit
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Cost_Decision
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicle.Licence_No_Actual
					,vehicleuser.User_ID
					");
		WHEN 'completed_orders' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 4
					AND IFNULL(uc.User_Communication_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					LEFT JOIN user AS ressource_user ON (ressource_user.User_ID = logistic.Ressource_User_ID)
					LEFT JOIN person AS ressource_person ON (ressource_person.Person_ID = ressource_user.Person_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					LEFT JOIN user_communication ON (user_communication.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = user_communication.Service_ID AND user_communication.Is_Read=0)
					LEFT JOIN user_communication uc ON (uc.Receiver_User_ID = ",OPT_PID," AND service.Service_ID = uc.Service_ID AND uc.Is_Read=0 AND uc.User_Communication_ID > user_communication.User_Communication_ID)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(logistic.Effectively_Delivered,logistic.Delivery_Date) AS Delivery_Date
					,IFNULL(ressource_person.First_Name, '') AS Ressource_First_Name
					,IFNULL(ressource_person.Last_Name , '') AS Ressource_Last_Name
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,user_communication.User_Communication_ID AS Available_Messages
					,vehicleuser.User_ID
					");
		WHEN 'completed_orders_hfp' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual = 4
					");
		SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, " 
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN currency AS budget_currency ON (budget_currency.Currency_ID = service.Cost_Currency_ID)
					LEFT JOIN service_income_type ON (service.Service_Income_Type_ID = service_income_type.Service_Income_Type_ID)
					");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,budget_currency.Position AS Budget_Position
					,budget_currency.Symbol AS Budget_Currency
					,client.Active
					,client.Name AS Client_Name
					,IF(vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,IFNULL(service_income_type.Income_Type, '-') AS Income_Type
					,logistic.Agreed_Price
					,logistic.Effectively_Delivered
					,logistic.Effectively_Pickedup
					,logistic.Income_Price
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Trip_Type
					,service.Budget
					,service.Cost AS Transport_Costs
					,service.Reference2 AS Reference
					,service.Workflow_ID
					");
			END IF;
		WHEN 'all_orders_overview' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND logistic.Service_Logistic_Status_Actual NOT IN (9,10,11)
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					JOIN service_logistic_status_template slst ON (slst.Service_Logistic_Status_Template_ID = logistic.Service_Logistic_Status_Actual)
					JOIN codebase cb_transport_status ON (cb_transport_status.Translation_Code = slst.Translation_Code)
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
					,logistic_contact.Postal_Code_Delivery AS Postal_Code
					,cb_transport_status.",LANG," AS Logistic_Status
					,client.Name AS Client_Name
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IFNULL(logistic.Effectively_Delivered,logistic.Delivery_Date) AS Effectively_Delivery_Date
					,IFNULL(logistic.Effectively_Pickedup,logistic.Pickup_Date) AS Effectively_Pickup_Date
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,IF (logistic.Multi_Vehicle_Movement = 1, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Service_Logistic_Status_Actual
					,logistic.Trip_Type
					,service.Reference2 AS Reference
					,service.Workflow_ID
					,vehicleuser.User_ID
                    ,logistic_contact.City_Delivery AS City
					");
		WHEN 'all_orders_providers' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
          JOIN partner AS client ON (client.Partner_ID = service.Client_ID)
					JOIN user AS vehicleuser ON (IF (IFNULL(service.Owner_ID,0) = 0 ,vehicle.User_ID, service.Owner_ID) = vehicleuser.User_ID)
					JOIN person AS vehicleperson ON (vehicleuser.Person_ID = vehicleperson.Person_ID)
					JOIN service_logistic_status_template slst ON (slst.Service_Logistic_Status_Template_ID = logistic.Service_Logistic_Status_Actual)
					JOIN codebase cb_transport_status ON (cb_transport_status.Translation_Code = slst.Translation_Code)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)			
					");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,cb_transport_status.",LANG," AS Logistic_Status
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,CONCAT(vehicleperson.First_Name, ' ', vehicleperson.Last_Name) AS User
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,TRIM(REPLACE(REPLACE(logistic.Logistic_Comment,'{VALUE_DELIMITER}',', '),'{ENTRY_DELIMITER}','</br></br>')) AS Logistic_Comment
					,logistic.Movement_Type
					,IF (logistic.Multi_Vehicle_Movement, 'Multivehicle', 0) AS Multi_Vehicle_Movement
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Workflow_ID
          ,client.Name AS Client_Name
					");
		WHEN 'all_orders_providers_hfp' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND (logistic.Service_Logistic_Status_Actual IN (2,3,4,5,7,9,10,11,12) 
						OR service.Service_Status_Actual = 44)
					AND IFNULL(st1.Service_Status_ID,0) = 0
					");
			SET DEFAULT_TABLES = CONCAT (DEFAULT_TABLES, "
					JOIN partner ON (partner.Partner_ID = service.Partner_ID)
					JOIN partner AS owner ON (vehicle.Owner_ID = owner.Partner_ID)
					LEFT JOIN company AS company_vehicle ON (company_vehicle.Company_ID = vehicle.Company_ID)
					LEFT JOIN service_status ON (service.Service_ID = service_status.Service_ID AND	service_status.Service_Status_Template_ID = 44)
					LEFT JOIN service_status st1 ON (service.Service_ID = st1.Service_ID AND st1.Service_Status_ID > service_status.Service_Status_ID AND st1.Service_Status_Template_ID = 44)
					");
		IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
					,CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident
					,IF (vehicle.Company_ID != 0, concat('c',vehicle.Company_ID), concat('p',vehicle.Owner_ID)) AS Owner_ID
					,IF (vehicle.Company_ID = 0, owner.Name, company_vehicle.Name) AS Owner
					,logistic.Movement_Type
					,logistic.Transport_Type
					,logistic.Trip_Type
					,partner.Name AS Partner_Name
					,service.Workflow_ID
					");
		END IF;
	END CASE;
	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	
	IF VISIBILITY_MODE = 'logistic_company'
		THEN SET BEDINGUNG = CONCAT ('WHERE
								(FIND_IN_SET(service.Client_ID,\'',PID,'\') > 0)
								AND vehicle.Deleted = 0
								AND service.Deleted = 0
							');
	ELSE
		SET BEDINGUNG = CONCAT ('WHERE
								(FIND_IN_SET(service.Client_ID,\'',PID,'\') > 0 OR FIND_IN_SET(service.Partner_ID,\'',PID,'\') > 0)
								AND vehicle.Deleted = 0
								AND service.Deleted = 0
							');
	END IF;
	
	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);
	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");
		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;		
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
