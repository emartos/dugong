DROP PROCEDURE IF EXISTS `procedure_user_management_active`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_user_management_active`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255),VISIBILITY_MODE VARCHAR (100), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT DEFAULT " ";
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;
	SET DEFAULT_ROWS = "";
	SET DEFAULT_FILTER = "";
	SET GRUPPIERUNG = "";
	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(DISTINCT user.User_ID) AS 'count' ";
			SET DEFAULT_TABLES = " user ";
			SET GRUPPIERUNG = "";
		WHEN 'list' OR 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_ROWS = CONCAT("
								user.User_ID,
								user.Username AS Username,
								person.First_Name AS First_Name,
								person.Last_Name AS Last_Name,
								partner.Name AS Partner_Name,
								section.Translation_Code AS Section,
								section.Section_ID,
								person.Person_ID,
								partner.Partner_ID AS Partner_ID,
								section.Section_ID AS Section_ID,
								e_mail.E_Mail,
								user.Active,
								usphone_number.Phone AS User_Phone,
								person.Person_Department,
								fcm_version_template.Version_Name
								");
			SET DEFAULT_TABLES = "
								user 
								JOIN person ON (person.Person_ID = user.Person_ID)
								JOIN partner ON (user.Main_Partner_ID = partner.Partner_ID)
								JOIN user_to_section ON (user.User_ID = user_to_section.User_ID)
								JOIN section ON (section.Section_ID = user_to_section.Section_ID)
								LEFT JOIN fcm_version_template ON (fcm_version_template.FCM_Version_Template_ID = partner.FCM_Version_Template_ID)
								LEFT JOIN person_to_e_mail ON (person.Person_ID = person_to_e_mail.Person_ID)
								LEFT JOIN e_mail ON (person_to_e_mail.E_Mail_ID = e_mail.E_Mail_ID)
								LEFT JOIN person_to_phone_number ustpn ON (ustpn.Person_ID = person.Person_ID)
								LEFT JOIN phone_number usphone_number ON (usphone_number.Phone_Number_ID = ustpn.Phone_Number_ID)
								";
			SET GRUPPIERUNG = "";
	END CASE;
	CASE LIST_TYPE
		WHEN 'all_users' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
								,user.Email_Notification
			");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"");
			IF CALL_TYPE = 'count' THEN
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"");
			END IF;
			IF PID != '1' THEN
				SET BEDINGUNG = CONCAT(BEDINGUNG," where user.Main_Partner_ID IN( ",PID,")");
			ELSE
				SET BEDINGUNG = CONCAT(BEDINGUNG," where person.Is_User = ",1);
			END IF;
		WHEN 'salesman' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,First_Address_Field,
								postal_code.Postal_Code AS Postal_Code,
								City AS City,
								CONCAT(phone_number.Phone_Prefix,' ',phone_number.Phone) AS Tel,
								web.Webaddress AS Webaddress,
								CONCAT(pal.value,' %') AS Level,
								pal.value AS Level_Value,
								IF(pal.Auth_Type = '1', 'percent', IF(pal.Auth_Type = '2', 'Currency', '')) AS cb_auth_type,
								pal.Auth_Type,
								currency.Symbol,
								currency.Position
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN address ON (address.Address_ID = partner.Address_ID)
								LEFT JOIN postal_code ON (address.Postal_Code_ID = postal_code.Postal_Code_ID) 
								LEFT JOIN partner_to_phone_number ON (partner_to_phone_number.Partner_ID = partner.Partner_ID)
								LEFT JOIN phone_number ON (partner_to_phone_number.Phone_Number_ID = phone_number.Phone_Number_ID)
								LEFT JOIN partner_to_web ON (partner.Partner_ID = partner_to_web.Partner_ID)
								LEFT JOIN web ON (partner_to_web.Web_ID = web.Web_ID)
						 		LEFT JOIN pricing_authorisation_level AS pal ON (pal.Person_ID = user.Person_ID AND pal.Sales_Channel_ID in (",VISIBILITY_MODE,") )
						 		LEFT JOIN pricing_authorisation_level AS pal1 ON (pal1.Person_ID = user.Person_ID AND pal1.Sales_Channel_ID in (",VISIBILITY_MODE,") AND pal1.Pricing_Authorisation_Level_ID > pal.Pricing_Authorisation_Level_ID) 
						 		LEFT JOIN country ON (address.Country_ID = country.Country_ID)
						 		LEFT JOIN currency ON (country.Currency_ID = currency.Currency_ID)
						 		");
			SET BEDINGUNG = CONCAT(BEDINGUNG," where user.Main_Partner_ID in (",PID,")");
			SET BEDINGUNG = CONCAT(BEDINGUNG," AND (section.SECTION_ID = 2 OR section.SECTION_ID = 8)");
			SET BEDINGUNG = CONCAT(BEDINGUNG," AND user.active = 1");
			SET BEDINGUNG = CONCAT(BEDINGUNG," AND IFNULL(pal1.Pricing_Authorisation_Level_ID, 0) = 0");
		WHEN 'fcm_online' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,user.User_ID,
								person.VAT,
								person.Company_Reg,
								person.Company_Name AS Company,
								CONCAT(person.First_Name,' ' ,person.Last_Name) AS Name,
								buyer.Buyer_ID,
								fcm_online_mail.E_Mail AS Mail,
								buyer.Buyer_Status_ID AS Buyer_Status_ID,
								buyer.Buyer_Type_ID,
								buyer_type.Translation_Code AS CB_Buyer_Type,
								buyer.Vendor_No,
								City AS City,
								First_Address_Field,
								user.Active
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN buyer ON (person.Buyer_ID = buyer.Buyer_ID)
								JOIN buyer_type ON (buyer.Buyer_Type_ID = buyer_type.Buyer_Type_ID)
								LEFT JOIN address_to_person AS atp ON (atp.Person_ID = person.Person_ID)
								LEFT JOIN address ON (address.Address_ID = atp.Address_ID)
								LEFT JOIN postal_code ON (address.Postal_Code_ID = postal_code.Postal_Code_ID)
								LEFT JOIN person_to_e_mail AS fcm_online_pte ON (fcm_online_pte.Person_ID = person.Person_ID)
								LEFT JOIN e_mail AS fcm_online_mail ON (fcm_online_mail.E_Mail_ID = fcm_online_pte.E_Mail_ID)
						 		");
			SET BEDINGUNG = CONCAT(BEDINGUNG," where user.Main_Partner_ID = ",PID);
			SET BEDINGUNG = CONCAT(BEDINGUNG," AND section.SECTION_ID = 37");
		WHEN 'logistic_ressource' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
				,CONCAT_WS(person.First_Name,',',person.Last_Name) AS Name_Concat
				,user.User_Type
				,user.Comment
				,user.Password
				
			");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"");
			IF CALL_TYPE = 'count' THEN
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"");
			END IF;
			SET BEDINGUNG = CONCAT(BEDINGUNG,"
														WHERE user.User_Type in ('driver', 'truck')
														AND user.Main_Partner_ID = ",PID);
														
	END CASE;
	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;		
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
