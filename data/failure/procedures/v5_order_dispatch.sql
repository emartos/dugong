DROP PROCEDURE IF EXISTS `procedure_v5_order_dispatch`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_v5_order_dispatch`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE FARBE TEXT;

	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;

	SET FARBE = get_translation_string(LANG, 'color');

	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(vehicle.Vehicle_ID) AS 'count' ";
			SET DEFAULT_TABLES = " 
								vehicle
								JOIN service ON(vehicle.Vehicle_ID = service.Service_Vehicle_ID)
								JOIN v5_order_dispatch ON(service.Service_ID = v5_order_dispatch.Service_ID)
								";
			SET DEFAULT_ROWS = "";
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
		WHEN 'list' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_TABLES = "
								vehicle
								JOIN service ON(vehicle.Vehicle_ID = service.Service_Vehicle_ID)
								JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
								JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
								JOIN vehicle_details ON (vehicle_details.Vehicle_ID = vehicle.Vehicle_ID)
                				JOIN codebase cb_specsheet ON (cb_specsheet.Translation_Code = vehicle_details.Specsheet_Required)
                				JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
								JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
								JOIN codebase cb_color ON (color.Translation_Code = cb_color.Translation_Code)
								JOIN v5_order_dispatch ON(service.Service_ID = v5_order_dispatch.Service_ID)
								JOIN address ON (v5_order_dispatch.v5_dispatch_address_id = address.Address_ID)
								JOIN country ON address.Country_ID = country.Country_ID
								JOIN codebase ON codebase.Translation_Code = country.Translation_Code
								LEFT JOIN vehicle_to_location ON (vehicle_to_location.Vehicle_ID=vehicle.Vehicle_ID AND vehicle_to_location.Active=1)								
								LEFT JOIN location USING(Location_ID)
								LEFT JOIN postal_code ON (postal_code.Postal_Code_ID = address.Postal_Code_ID)
								LEFT JOIN contract_information ON (contract_information.Vehicle_ID = vehicle.Vehicle_ID )
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle_make.Name AS Make,
								vehicle_model.Name AS Model,
								vehicle_make.Name AS Manufacturer,
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Licence_No_Actual,
								vehicle.VIN,
								service.Service_ID,
								service.Set_Date AS Set_Date,
								vehicle.Mileage_Actual AS Mileage,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Mileage_Unit,
								CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident,
								location.Name AS Location,
								vehicle.Unit_ID AS Unit_ID,
								First_Address_Field,
								concat(address.First_Address_Field,', ',codebase.",LANG,") as Destination_Address,
								concat(address.First_Address_Field, \', \', codebase.EN, \'<br>\', postal_code.Postal_Code, \', \', postal_code.City) as Full_Destination_Address,
								contract_information.Contract_Type AS Contract_Type,		
              					cb_specsheet.Translation_Code AS CB_Specsheet,
								");
			SET DEFAULT_FILTER = " ";
			SET GRUPPIERUNG = "";
		WHEN 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_TABLES = "
								vehicle
								JOIN service ON(vehicle.Vehicle_ID = service.Service_Vehicle_ID)
								JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
								JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
               					JOIN vehicle_details ON (vehicle_details.Vehicle_ID = vehicle.Vehicle_ID)
                				JOIN codebase cb_specsheet ON (cb_specsheet.Translation_Code = vehicle_details.Specsheet_Required)
								JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
								JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
								JOIN codebase cb_color ON (color.Translation_Code = cb_color.Translation_Code)
								JOIN v5_order_dispatch ON(service.Service_ID = v5_order_dispatch.Service_ID)
								JOIN address ON (v5_order_dispatch.v5_dispatch_address_id = address.Address_ID)
								JOIN country ON address.Country_ID = country.Country_ID
								JOIN codebase ON codebase.Translation_Code = country.Translation_Code
                				LEFT JOIN postal_code ON (postal_code.Postal_Code_ID = address.Postal_Code_ID)
								LEFT JOIN vehicle_to_location ON (vehicle_to_location.Vehicle_ID=vehicle.Vehicle_ID AND vehicle_to_location.Active=1)								
								LEFT JOIN location USING(Location_ID)
								LEFT JOIN contract_information ON (contract_information.Vehicle_ID = vehicle.Vehicle_ID )
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle_make.Name AS Make,
								vehicle_model.Name AS Model,
								vehicle_make.Name AS Manufacturer,
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Licence_No_Actual,
								vehicle.VIN,
								service.Service_ID,
								service.Set_Date AS Set_Date,
								vehicle.Mileage_Actual AS Mileage,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Mileage_Unit,
								CONCAT('<b>REG:&nbsp;</b>',vehicle.Licence_No_Actual,'<br /><b>VIN:&nbsp;</b>',vehicle.VIN,'<br /><b>UNIT:&nbsp;</b>',vehicle.Unit_ID) AS Vehicle_Ident,
								location.Name AS Location,
								vehicle.Unit_ID AS Unit_ID,
								First_Address_Field,
								concat(address.First_Address_Field,', ',codebase.",LANG,") as Destination_Address,
								concat(address.First_Address_Field, \', \', codebase.EN, \'<br>\', postal_code.Postal_Code, \', \', postal_code.City) as Full_Destination_Address,
								contract_information.Contract_Type AS Contract_Type,
                				cb_specsheet.Translation_Code AS CB_Specsheet,
								");
			SET DEFAULT_FILTER = " ";
			SET GRUPPIERUNG = "";
	END CASE;

	CASE LIST_TYPE
		WHEN 'to_send_auction' THEN
			SET DEFAULT_FILTER = " 
							and v5_dispatch_status = 1
                			and Created_From = 'sca'
							";
			IF CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
							vehicle.First_Reg_Date AS First_Reg_Date,
							service.Service_ID,
							vehicle_make.Name AS Manufacturer,
							IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
							vehicle.Mileage_Actual AS Odometry,
							",FARBE);
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
									");
			ELSEIF CALL_TYPE = 'export' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
									");
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
									");
			END IF;
   WHEN 'to_send_buyer' THEN
			SET DEFAULT_FILTER = " 
							and v5_dispatch_status = 1
              				and Created_From = 'psp'
							";
			IF CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
							vehicle.First_Reg_Date AS First_Reg_Date,
							service.Service_ID,
							vehicle_make.Name AS Manufacturer,
							IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
							vehicle.Mileage_Actual AS Odometry,
							",FARBE);
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
										");
			ELSEIF CALL_TYPE = 'export' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
									");
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
									");
			END IF;
		WHEN 'to_send' THEN
			SET DEFAULT_FILTER = " 
								and v5_dispatch_status = 1
							";
			IF CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
							vehicle.First_Reg_Date AS First_Reg_Date,
							service.Service_ID,
							vehicle_make.Name AS Manufacturer,
							IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
							vehicle.Mileage_Actual AS Odometry,
							",FARBE);
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
										");
			ELSEIF CALL_TYPE = 'export' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
									");
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
									");
			END IF;
		WHEN 'sent' THEN
			SET DEFAULT_FILTER = " 
								and v5_dispatch_status = 2
							";
			IF CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "  
							vehicle.First_Reg_Date AS First_Reg_Date,
							service.Service_ID,
							vehicle_make.Name AS Manufacturer,
							IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
							vehicle.Mileage_Actual AS Odometry,
							vehicle.Unit_ID AS Unit_ID,
							v5_dispatch_date,
							v5_dispatch_comment,							
							",FARBE);
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
										
										");
			ELSEIF CALL_TYPE = 'export' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
									");
				SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, " 
										
										");
			END IF;
	END CASE;

	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	
	SET BEDINGUNG = CONCAT ('WHERE
							(FIND_IN_SET(service.Client_ID,\'',PID,'\') > 0 OR FIND_IN_SET(service.Partner_ID,\'',PID,'\') > 0)
							AND vehicle.Deleted = 0
							AND service.Deleted = 0
							');
							
	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;		
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
END |;
DELIMITER ;
