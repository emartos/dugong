DROP PROCEDURE IF EXISTS `procedure_wholesale`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_wholesale`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE FARBE TEXT;
	DECLARE FUEL_TYPE TEXT;
	DECLARE DATUM VARCHAR(15);
	DECLARE VEHICLE_TYPE TEXT;

	SELECT CONCAT("'",CURDATE(),"'") INTO DATUM;

	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;
	
	SET FARBE = get_translation_string(LANG, 'color');
	SET FUEL_TYPE = get_translation_string(LANG, 'fuel_type');
	SET VEHICLE_TYPE = get_translation_string(LANG, 'vehicle_type');

	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(DISTINCT(vehicle.Vehicle_ID)) AS 'count' ";
			SET DEFAULT_TABLES = "
								utilisation
								JOIN vehicle ON (vehicle.Vehicle_ID = utilisation.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID != 4)
								JOIN sales_channels_to_utilisation ON (utilisation.Utilisation_ID = sales_channels_to_utilisation.Utilization_ID)
								JOIN service ON (utilisation.Service_ID = service.Service_ID)
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
								LEFT JOIN fleet_handling_data fhd1 ON (fleet_handling.Fleet_Handling_ID = fhd1.Fleet_Handling_ID AND fhd1.Data_Type = 2)
								LEFT JOIN fleet_handling_data fhd2 ON (fleet_handling.Fleet_Handling_ID = fhd2.Fleet_Handling_ID AND fhd2.Data_Type = 2 AND fhd1.Fleet_Handling_Data_ID < fhd2.Fleet_Handling_Data_ID)
								LEFT JOIN vehicle_to_hold vth1 ON (vehicle.Vehicle_ID = vth1.Vehicle_ID)
								LEFT JOIN vehicle_to_hold vth2 ON (vehicle.Vehicle_ID = vth2.Vehicle_ID AND vth1.Vehicle_To_Hold_ID < vth2.Vehicle_To_Hold_ID)
								";
			SET DEFAULT_ROWS = "";
			SET DEFAULT_FILTER = "
								AND IFNULL(vth2.Vehicle_To_Hold_ID, 0) = 0
								AND IFNULL(fhd2.Fleet_Handling_Data_ID, 0) = 0
								";
			SET GRUPPIERUNG = "";
		WHEN 'list' THEN
			SET DEFAULT_CALL_TYPE = "SELECT STRAIGHT_JOIN SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_TABLES = "
								utilisation
								JOIN vehicle ON (vehicle.Vehicle_ID = utilisation.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID != 4)								JOIN currency ON (vehicle.Currency_ID = currency.Currency_ID)
								JOIN make_to_model ON (vehicle.Make_To_Model_ID = make_to_model.Make_To_Model_ID)
									JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
									JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
								JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
									JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
									JOIN codebase AS cb_color ON (color.Translation_Code = cb_color.Translation_Code)
									JOIN service ON (utilisation.Service_ID = service.Service_ID)
									JOIN sales_channels_to_utilisation ON (utilisation.Utilisation_ID = sales_channels_to_utilisation.Utilization_ID)
								LEFT JOIN price_actual ON (vehicle.Vehicle_ID = price_actual.Vehicle_ID)
								LEFT JOIN vehicle_to_location ON (vehicle.Vehicle_ID = vehicle_to_location.Vehicle_ID AND vehicle_to_location.Active = 1)
									LEFT JOIN parking_lot_to_location pltl1 ON (vehicle_to_location.Vehicle_To_Location_ID = pltl1.Vehicle_To_Location_ID)
									LEFT JOIN parking_lot_to_location pltl2 ON (vehicle_to_location.Vehicle_To_Location_ID = pltl2.Vehicle_To_Location_ID AND pltl1.Parking_Lot_To_Location_ID < pltl2.Parking_Lot_To_Location_ID)
									LEFT JOIN location ON (vehicle_to_location.Location_ID = location.Location_ID)
								LEFT JOIN guide_price_to_vehicle gptv1 ON (vehicle.Vehicle_ID = gptv1.Vehicle_ID)
								LEFT JOIN guide_price_to_vehicle gptv2 ON (vehicle.Vehicle_ID = gptv2.Vehicle_ID AND gptv1.Guide_Price_To_Vehicle_ID < gptv2.Guide_Price_To_Vehicle_ID)
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
									LEFT JOIN fleet_handling_data fhd1 ON (fleet_handling.Fleet_Handling_ID = fhd1.Fleet_Handling_ID AND fhd1.Data_Type = 2)
									LEFT JOIN fleet_handling_data fhd2 ON (fleet_handling.Fleet_Handling_ID = fhd2.Fleet_Handling_ID AND fhd2.Data_Type = 2 AND fhd1.Fleet_Handling_Data_ID < fhd2.Fleet_Handling_Data_ID)
								LEFT JOIN vehicle_to_hold vth1 ON (vehicle.Vehicle_ID = vth1.Vehicle_ID)
								LEFT JOIN vehicle_to_hold vth2 ON (vehicle.Vehicle_ID = vth2.Vehicle_ID AND vth1.Vehicle_To_Hold_ID < vth2.Vehicle_To_Hold_ID)
								JOIN vehicle_details ON (vehicle.Vehicle_ID = vehicle_details.Vehicle_ID)
									JOIN trim_level ON (vehicle_details.Trim_Level_ID = trim_level.Trim_Level_ID)
									JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
									JOIN vehicle_type ON (vehicle_details.Vehicle_Type_ID = vehicle_type.Vehicle_Type_ID)
										JOIN codebase cb_vehicle_type ON (vehicle_type.Translation_Code = cb_vehicle_type.Translation_Code)
									JOIN vehicle_engine ON (vehicle_details.Vehicle_Details_ID = vehicle_engine.Vehicle_Details_ID)
										JOIN fuel_type ON (vehicle_engine.Fuel_Type_ID = fuel_type.Fuel_Type_ID)
											JOIN codebase AS cb_fuel_type ON (fuel_type.Translation_Code = cb_fuel_type.Translation_Code)
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Owner_ID AS Owner_ID,
								utilisation.Utilisation_ID,
								vehicle_make.Name AS Manufacturer,
								vehicle_model.Name AS Model,
								vehicle_details.Imported AS Reimport,
								pltl1.Parking_Lot AS Parking_Lot,
								",FUEL_TYPE,",
								",VEHICLE_TYPE,",
								vehicle.Mileage_Actual AS Odometry,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
								vehicle.Licence_No_Actual AS Licence_No_Actual,
								IF(vehicle_to_location.Active = 1, location.Name, '') AS Location,
								vehicle.First_Reg_Date AS First_Reg_Date,
								DATEDIFF(",DATUM,", fhd1.Set_Date) AS Defleet_Date_Diff,
								DATEDIFF(",DATUM,", sales_channels_to_utilisation.Set_Date) AS Channel_Days,
								vehicle.Damage_Cost AS Damage_Cost,
								price_actual.Wholesale_Price AS Wholesale_Price,
								price_actual.Wholesale_Price AS Wholesale_Price_Net,
								price_actual.Wholesale_Price AS Wholesale_Price_Reg,
								gptv1.Guide_Price AS Guide_Price,
								(SELECT ptps.Package_Status_ID FROM package AS pack JOIN vehicle_to_package AS vtp ON (vtp.Package_ID = pack.Package_ID) JOIN package_to_package_status AS ptps ON (ptps.Package_ID = pack.Package_ID) WHERE vehicle.Vehicle_ID = vtp.Vehicle_ID AND pack.Deleted = 0 ORDER BY vtp.Vehicle_To_Package_ID, ptps.Package_To_Package_Status_ID DESC LIMIT 1) AS Status_Actual,
								IFNULL((SELECT sales_channels_visibility.Sales_Channels_Visibility_Template_ID FROM sales_channels_visibility JOIN 
								sales_channels_to_utilisation on (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID=sales_channels_visibility.Sales_Channels_To_Utilisation_ID) where 
								sales_channels_to_utilisation.Utilization_ID=utilisation.Utilisation_ID order by sales_channels_visibility.Sales_Channels_Visibility_ID DESC LIMIT 1),2) AS vis,
								currency.Symbol AS Currency,
								currency.Position AS Currency_Pos,
								trim_level.Value AS Trim_Level,
								derivative.Name AS Derivative,
								",FARBE);
			SET DEFAULT_FILTER = "
								AND IFNULL(vth2.Vehicle_To_Hold_ID, 0) = 0
								AND IFNULL(fhd2.Fleet_Handling_Data_ID, 0) = 0
								AND IFNULL(gptv2.Guide_Price_To_Vehicle_ID, 0) = 0
								AND IFNULL(pltl2.Parking_Lot_To_Location_ID, 0) = 0
								";
			SET GRUPPIERUNG = "";
		WHEN 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT STRAIGHT_JOIN SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_TABLES = "
								utilisation
								JOIN vehicle ON (vehicle.Vehicle_ID = utilisation.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID != 4)
								JOIN currency ON (vehicle.Currency_ID = currency.Currency_ID)
								JOIN model_code ON (vehicle.Model_Code_ID = model_code.Model_Code_ID)
								JOIN make_to_model ON (vehicle.Make_To_Model_ID = make_to_model.Make_To_Model_ID)
									JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
									JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
								JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
									JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
									JOIN codebase AS cb_color ON (color.Translation_Code = cb_color.Translation_Code)
									JOIN service ON (utilisation.Service_ID = service.Service_ID)
									JOIN sales_channels_to_utilisation ON (utilisation.Utilisation_ID = sales_channels_to_utilisation.Utilization_ID)
								LEFT JOIN price_actual ON (vehicle.Vehicle_ID = price_actual.Vehicle_ID)
								LEFT JOIN vehicle_to_location ON (vehicle.Vehicle_ID = vehicle_to_location.Vehicle_ID AND vehicle_to_location.Active = 1)
									LEFT JOIN parking_lot_to_location pltl1 ON (vehicle_to_location.Vehicle_To_Location_ID = pltl1.Vehicle_To_Location_ID)
									LEFT JOIN parking_lot_to_location pltl2 ON (vehicle_to_location.Vehicle_To_Location_ID = pltl2.Vehicle_To_Location_ID AND pltl1.Parking_Lot_To_Location_ID < pltl2.Parking_Lot_To_Location_ID)
									LEFT JOIN location ON (vehicle_to_location.Location_ID = location.Location_ID)
								LEFT JOIN guide_price_to_vehicle gptv1 ON (vehicle.Vehicle_ID = gptv1.Vehicle_ID)
								LEFT JOIN guide_price_to_vehicle gptv2 ON (vehicle.Vehicle_ID = gptv2.Vehicle_ID AND gptv1.Guide_Price_To_Vehicle_ID < gptv2.Guide_Price_To_Vehicle_ID)
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
									LEFT JOIN fleet_handling_data fhd1 ON (fleet_handling.Fleet_Handling_ID = fhd1.Fleet_Handling_ID AND fhd1.Data_Type = 2)
									LEFT JOIN fleet_handling_data fhd2 ON (fleet_handling.Fleet_Handling_ID = fhd2.Fleet_Handling_ID AND fhd2.Data_Type = 2 AND fhd1.Fleet_Handling_Data_ID < fhd2.Fleet_Handling_Data_ID)
								LEFT JOIN vehicle_to_hold vth1 ON (vehicle.Vehicle_ID = vth1.Vehicle_ID)
								LEFT JOIN vehicle_to_hold vth2 ON (vehicle.Vehicle_ID = vth2.Vehicle_ID AND vth1.Vehicle_To_Hold_ID < vth2.Vehicle_To_Hold_ID)
								JOIN vehicle_details ON (vehicle.Vehicle_ID = vehicle_details.Vehicle_ID)
									JOIN trim_level ON (vehicle_details.Trim_Level_ID = trim_level.Trim_Level_ID)
									JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
									JOIN vehicle_type ON (vehicle_details.Vehicle_Type_ID = vehicle_type.Vehicle_Type_ID)
										JOIN codebase cb_vehicle_type ON (vehicle_type.Translation_Code = cb_vehicle_type.Translation_Code)
									JOIN vehicle_engine ON (vehicle_details.Vehicle_Details_ID = vehicle_engine.Vehicle_Details_ID)
										JOIN fuel_type ON (vehicle_engine.Fuel_Type_ID = fuel_type.Fuel_Type_ID)
											JOIN codebase AS cb_fuel_type ON (fuel_type.Translation_Code = cb_fuel_type.Translation_Code)
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Owner_ID AS Owner_ID,
								utilisation.Utilisation_ID,
								vehicle_make.Name AS Manufacturer,
								vehicle_model.Name AS Model,
								vehicle_details.Imported AS Reimport,
								pltl1.Parking_Lot AS Parking_Lot,
								",FUEL_TYPE,",
								",VEHICLE_TYPE,",
								vehicle.Mileage_Actual AS Odometry,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
								vehicle.Licence_No_Actual AS Licence_No_Actual,
								IF(vehicle_to_location.Active = 1, location.Name, '') AS Location,
								vehicle.First_Reg_Date AS First_Reg_Date,
								DATEDIFF(",DATUM,", fhd1.Set_Date) AS Defleet_Date_Diff,
								DATEDIFF(",DATUM,", sales_channels_to_utilisation.Set_Date) AS Channel_Days,
								vehicle.Damage_Cost AS Damage_Cost,
								price_actual.Wholesale_Price AS Wholesale_Price,
								price_actual.Wholesale_Price AS Wholesale_Price_Net,
								price_actual.Wholesale_Price AS Wholesale_Price_Reg,
								gptv1.Guide_Price AS Guide_Price,
								(SELECT ptps.Package_Status_ID FROM package AS pack JOIN vehicle_to_package AS vtp ON (vtp.Package_ID = pack.Package_ID) JOIN package_to_package_status AS ptps ON (ptps.Package_ID = pack.Package_ID) WHERE vehicle.Vehicle_ID = vtp.Vehicle_ID AND pack.Deleted = 0 ORDER BY vtp.Vehicle_To_Package_ID, ptps.Package_To_Package_Status_ID DESC LIMIT 1) AS Status_Actual,
								IFNULL((SELECT sales_channels_visibility.Sales_Channels_Visibility_Template_ID FROM sales_channels_visibility JOIN 
								sales_channels_to_utilisation on (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID=sales_channels_visibility.Sales_Channels_To_Utilisation_ID) where 
								sales_channels_to_utilisation.Utilization_ID=utilisation.Utilisation_ID order by sales_channels_visibility.Sales_Channels_Visibility_ID DESC LIMIT 1),2) AS vis,
								currency.Symbol AS Currency,
								currency.Position AS Currency_Pos,
								trim_level.Value AS Trim_Level,
								derivative.Name AS Derivative,
								",FARBE);
			SET DEFAULT_FILTER = "
								AND IFNULL(vth2.Vehicle_To_Hold_ID, 0) = 0
								AND IFNULL(fhd2.Fleet_Handling_Data_ID, 0) = 0
								AND IFNULL(gptv2.Guide_Price_To_Vehicle_ID, 0) = 0
								AND IFNULL(pltl2.Parking_Lot_To_Location_ID, 0) = 0
								";
			SET GRUPPIERUNG = "";
	END CASE;

	CASE LIST_TYPE
		WHEN 'all_wholesale_vehicles' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								");
		WHEN 'all_wholesale_vehicles_sales_manager' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								#AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
								,(SELECT(SELECT package_to_package_status.Package_Status_ID FROM package 
										JOIN vehicle_to_package ON (vehicle_to_package.Package_ID = package.Package_ID) 
										JOIN package_to_package_status ON (package_to_package_status.Package_ID = package.Package_ID) 
										WHERE vehicle.Vehicle_ID = vehicle_to_package.Vehicle_ID AND package.Deleted = 0 
										ORDER BY vehicle_to_package.Vehicle_To_Package_ID, package_to_package_status.Package_To_Package_Status_ID DESC LIMIT 1) IN (1,2)) AS Active_Package
								");
		WHEN 'all_available_wholesale_vehicles' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								");
		WHEN 'not_visible_vehicles' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								AND IFNULL(scv2.Sales_Channels_Visibility_ID, 0) = 0
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
								AND ((SELECT sales_channels_visibility.Sales_Channels_Visibility_Template_ID FROM sales_channels_visibility WHERE 
									sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = sales_channels_visibility.Sales_Channels_To_Utilisation_ID
									ORDER BY sales_channels_visibility.Sales_Channels_Visibility_ID DESC LIMIT 1) = 2 
									OR 
									NOT EXISTS ((SELECT * FROM sales_channels_visibility WHERE 
									sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = sales_channels_visibility.Sales_Channels_To_Utilisation_ID)))
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN sales_channels_visibility scv1 ON (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = scv1.Sales_Channels_To_Utilisation_ID)
								LEFT JOIN sales_channels_visibility scv2 ON (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = scv2.Sales_Channels_To_Utilisation_ID AND scv1.Sales_Channels_Visibility_ID < scv2.Sales_Channels_Visibility_ID)
								");
		WHEN 'visible_vehicles' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								AND IFNULL(scv2.Sales_Channels_Visibility_ID, 0) = 0
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
								AND (SELECT sales_channels_visibility.Sales_Channels_Visibility_Template_ID FROM sales_channels_visibility WHERE 
									sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = sales_channels_visibility.Sales_Channels_To_Utilisation_ID
									ORDER BY sales_channels_visibility.Sales_Channels_Visibility_ID DESC LIMIT 1) = 1 
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN sales_channels_visibility scv1 ON (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = scv1.Sales_Channels_To_Utilisation_ID)
								LEFT JOIN sales_channels_visibility scv2 ON (sales_channels_to_utilisation.Sales_Channels_To_Utilisation_ID = scv2.Sales_Channels_To_Utilisation_ID AND scv1.Sales_Channels_Visibility_ID < scv2.Sales_Channels_Visibility_ID)
								");
		WHEN 'sold_within_48' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 2
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND DATEDIFF(",DATUM,", utilisation.Sold_Date) < 2
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								");
		WHEN '10_days_in_channel' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND DATEDIFF(",DATUM,", sales_channels_to_utilisation.Set_Date) >= 10
								AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								");
		WHEN '30_days_defleet' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								AND DATEDIFF(",DATUM,", fhd1.Set_Date) >= 30
								AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
								AND IFNULL(vth1.Hold_Template_ID, 2) = 2
								");
		WHEN 'vehicles_in_packages' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
								,package.Package_ID	
								");
            SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "                    
                                JOIN vehicle_to_package ON (vehicle_to_package.vehicle_ID = vehicle.vehicle_ID)
                                JOIN package ON (vehicle_to_package.Package_ID = package.Package_ID AND package.Deleted = 0 AND Sale_Cancelled = 0)
								");
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND (SELECT package_to_package_status.Package_Status_ID FROM package 
									JOIN vehicle_to_package ON (vehicle_to_package.Package_ID = package.Package_ID AND Sale_Cancelled = 0) 
									JOIN package_to_package_status ON (package_to_package_status.Package_ID = package.Package_ID) 
									WHERE vehicle.Vehicle_ID = vehicle_to_package.Vehicle_ID AND package.Deleted = 0 
									ORDER BY vehicle_to_package.Vehicle_To_Package_ID, package_to_package_status.Package_To_Package_Status_ID DESC LIMIT 1) IN (1,2)
								AND sales_channels_to_utilisation.Sales_Channels_ID = 3
								");
		WHEN 'on_hold' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
									AND utilisation.Utilisation_Result_Type_ID = 1
									AND sales_channels_to_utilisation.Sales_Channels_ID = 3
									AND NOT EXISTS (SELECT * FROM vehicle_to_package JOIN package using(package_id) where vehicle_to_package.Vehicle_ID=vehicle.Vehicle_ID AND deleted=0 AND Sale_Cancelled=0)
									AND vth1.Hold_Template_ID = 1
									");
		WHEN 'sold' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
									,utilisation.Sales_Price AS Sold_Price_Gross
									");
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
									AND utilisation.Utilisation_Result_Type_ID = 2
									#AND IFNULL(vth1.Hold_Template_ID, 2) = 2
									AND sales_channels_to_utilisation.Sales_Channels_ID = 3
									");
	END CASE;

	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT(" FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	SET BEDINGUNG = CONCAT (' WHERE
									vehicle.Deleted = 0
									AND vehicle.Vehicle_Status_Actual != 3
									AND (FIND_IN_SET(service.Partner_ID,\'',PID,'\') > 0 OR FIND_IN_SET(service.Client_ID,\'',PID,'\') > 0)
									');

	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;		
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
