DROP PROCEDURE IF EXISTS `export_package`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `export_package`(PID VARCHAR(255), LANG VARCHAR(2))
BEGIN
	SET @sql = CONCAT("
						SELECT STRAIGHT_JOIN SQL_BIG_RESULT
							#buyer.Buyer_Type_ID AS Buyer_Type_ID,
							#package.Package_ID AS PackageID,
							IF(package.Package_ID is NOT Null, package.Package_ID, utilisation_document.Sales_Agreement_No) AS PackageID,
							u.utilisation_id AS SaleChannelKeyID,
							IFNULL(DATE_FORMAT(package.Package_Offered, '%d/%m/%Y %H:%i:%s'), '') AS PackageOfferedDate,
							IFNULL(DATE_FORMAT(package.Package_Valid, '%d/%m/%Y %H:%i:%s'), '') AS PackageValidDate,
							IF(ptps1.Package_Status_ID = 1 ,IFNULL(DATE_FORMAT(ptps1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''), '') AS PackageCreatedDate,
							IF(ptps1.Package_Status_ID = 1 ,ptps1.User_ID, '') AS PackageCreatedUserID,
							IF(ptps1.Package_Status_ID = 1 ,ptps1.Comment, '') AS PackageCreatedComment,
							cb_ps.",LANG," AS PackageStatus,
							IFNULL(DATE_FORMAT(ptps4.Set_Date, '%d/%m/%Y %H:%i:%s'), '') AS PackageStatusDate,
							ptps4.User_ID AS PackageStatusUserID,
							ptps4.Comment AS PackageStatusComment,
							IFNULL(cb_pas.",LANG,",'') AS PackageAuthorisationStatus,
							IFNULL(DATE_FORMAT(pa1.Set_Date, '%d/%m/%Y %H:%i:%s'), '') AS PackageAuthorisationDate,
							pa1.User_ID AS PackageAuthorisationUserID,
							IFNULL(pa1.Comment,'') AS PackageAuthorisationComment,
							IF(ptps4.Set_Date > pch1.Set_Date AND ptps4.Set_Date > pa1.Set_Date AND ptps4.Set_Date > poh1.Set_Date,
								IFNULL(DATE_FORMAT(ptps4.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
								IF(pch1.Set_Date > pa1.Set_Date AND pch1.Set_Date > poh1.Set_Date,
									IFNULL(DATE_FORMAT(pch1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
									IF(pa1.Set_Date > poh1.Set_Date,
										IFNULL(DATE_FORMAT(pa1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
										IFNULL(DATE_FORMAT(poh1.Set_Date, '%d/%m/%Y %H:%i:%s'), '')
									)
								)
							) AS PackageUpdatedDate,
							package.Offered_Complete_Price AS PackageTotalPrice,
							package.Offered_Complete_Price AS PackageTotalPriceGross,
							#ROUND((package.Offered_Complete_Price * 100) / (IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value) + 100), 2) AS PackageTotalOfferPriceNet,
							@net:=get_sold_price_net(package.Offered_Complete_Price,IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value),v.Owner_ID,v.Vehicle_ID,buyer.Buyer_Type_ID,@vcount:=(SELECT COUNT(vtp_count.Vehicle_ID)
																															FROM vehicle_to_package vtp_count
																															WHERE package.Package_ID = vtp_count.Package_ID
																															GROUP BY vtp_count.Package_ID
							),taxes.Taxes_ID) AS PackageTotalOfferPriceNet,
							IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value) AS VatPercentage,
							#package.Offered_Complete_Price - ROUND((package.Offered_Complete_Price * 100) / (IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value) + 100), 2) AS PackageVATAmount,
							get_sold_price_vat(package.Offered_Complete_Price,IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value),v.Owner_ID,v.Vehicle_ID,v.First_Reg_Date,u.Sold_Date,buyer.Buyer_Type_ID,@vcount,taxes.Taxes_ID,IF(package.PDF_Name != \'deleted\' , package.PDF_Price_Type ,vehicle_to_package.Payment_Net_Gross_Template_ID)) AS PackageVATAmount,
							package.Offered_Complete_Price AS PackageCostGross,
							IF(v.Owner_ID = 26 AND buyer.Buyer_Type_ID = 4 AND DATE_SUB(CURDATE(),INTERVAL 2 YEAR) <= v.First_Reg_Date,
								ROUND(@net * (1 + (reg_tax.Tax / 100)) * (1 + (IF(u.Tax_Value IS NOT NULL ,u.Tax_Value,taxes.Value) / 100)), 2),
								package.Offered_Complete_Price
							) AS OfferedGrossInclReg,
							currency.Currency_Code AS Currency,
							v.Owner_ID AS OwnerID
						FROM utilisation u
							JOIN service ON (service.Service_ID = u.Service_ID)
							LEFT JOIN sales_channels_to_utilisation  sctu ON (sctu.Utilization_ID = u.Utilisation_ID)
								LEFT JOIN sales_channels  sc ON (sc.Sales_Channels_ID = sctu.Sales_Channels_ID)
							LEFT JOIN vehicle_to_package ON (u.Vehicle_ID = vehicle_to_package.Vehicle_ID)
								LEFT JOIN package ON (vehicle_to_package.Package_ID = package.Package_ID)
									LEFT JOIN package_to_package_status ptps1 ON (package.Package_ID = ptps1.Package_ID)
									LEFT JOIN package_to_package_status ptps2 ON (ptps1.Package_ID = ptps2.Package_ID AND ptps1.Package_To_Package_Status_ID > ptps2.Package_To_Package_Status_ID)
									LEFT JOIN package_to_package_status ptps4 ON (package.Package_ID = ptps4.Package_ID)
									LEFT JOIN package_to_package_status ptps3 ON (ptps4.Package_ID = ptps3.Package_ID AND ptps4.Package_To_Package_Status_ID < ptps3.Package_To_Package_Status_ID)
										LEFT JOIN package_status ON (ptps4.Package_Status_ID = package_status.Package_Status_ID)
											LEFT JOIN codebase cb_ps ON (package_status.Translation_Code = cb_ps.Translation_Code)
									LEFT JOIN package_authorisation pa1 ON (package.Package_ID = pa1.Package_ID)
									LEFT JOIN package_authorisation pa2 ON (package.Package_ID = pa2.Package_ID AND pa1.Package_Authorisation_ID < pa2.Package_Authorisation_ID)
										LEFT JOIN package_authorisation_status ON (pa1.Package_Authorisation_Status_ID = package_authorisation_status.Package_Authorisation_Status_ID)
											LEFT JOIN codebase cb_pas ON (package_authorisation_status.Translation_Code = cb_pas.Translation_Code)
									LEFT JOIN package_changes_history pch1 ON (package.Package_ID = pch1.Package_ID)
									LEFT JOIN package_changes_history pch2 ON (package.Package_ID = pch2.Package_ID AND pch1.Package_Changes_History_ID < pch2.Package_Changes_History_ID)
									LEFT JOIN package_offer_history poh1 ON (package.Package_ID = poh1.Package_ID)
									LEFT JOIN package_offer_history poh2 ON (package.Package_ID = poh2.Package_ID AND poh1.Package_Offer_History_ID < poh2.Package_Offer_History_ID)
									LEFT JOIN buyer ON (package.Buyer_ID = buyer.Buyer_ID)
							JOIN vehicle v ON (v.Vehicle_ID = u.Vehicle_ID)
								JOIN price_actual ON (price_actual.Vehicle_ID = v.Vehicle_ID)
								JOIN partner ON (v.Owner_ID = partner.Partner_ID)
									JOIN address ON (partner.Address_ID = address.Address_ID)
										JOIN taxes ON (address.Country_ID = taxes.Country_ID)
								JOIN currency ON (v.Currency_ID = currency.Currency_ID)
							JOIN vehicle_details ON (u.Vehicle_ID = vehicle_details.Vehicle_ID)
								LEFT JOIN reg_tax ON (vehicle_details.Co2 BETWEEN reg_tax.CO2_Min AND reg_tax.CO2_Max)
							LEFT JOIN utilisation_document ON (utilisation_document.Utilisation_ID = u.Utilisation_ID)
						WHERE
							v.Owner_ID IN (",PID,")
							AND v.Owner_ID != 1
							AND u.Utilisation_Result_Type_ID != 4
							AND v.Deleted = 0
							#AND package.Deleted = 0
							AND IFNULL(package.Deleted, 0) = 0
							AND IFNULL(ptps2.Package_To_Package_Status_ID, 0) = 0
							AND IFNULL(ptps3.Package_To_Package_Status_ID, 0) = 0
							AND IFNULL(pa2.Package_Authorisation_ID, 0) = 0
							AND IFNULL(pch2.Package_Changes_History_ID, 0) = 0
							AND IFNULL(poh2.Package_Offer_History_ID, 0) = 0
							#AND vehicle_to_package.Sale_Cancelled = 0
							AND IFNULL(vehicle_to_package.Sale_Cancelled, 0) = 0
						");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
