DROP PROCEDURE IF EXISTS `export_compound`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `export_compound`(PID VARCHAR(255), LANG VARCHAR(2))
BEGIN
	DECLARE DATUM VARCHAR(15);
	SELECT CONCAT("'",CURDATE(),"'") INTO DATUM;
	SET @sql = CONCAT("
						SELECT DISTINCT
							vehicle.Vehicle_ID AS VehicleID,
							location.Location_ID AS LocationID,
							'' AS DaysInFleet,
							IF(vehicle_to_location.Active = 1,
								DATEDIFF(",DATUM,",vehicle_to_location.Set_Date),
								'-'
							) AS DaysOnLocation,
							IF(vehicle_to_location.Active = 1, 'Yes', 'No') AS IsVehicleInCompound,
							IFNULL(DATE_FORMAT(vehicle_to_location.Set_Date, '%d/%m/%Y %H:%i:%s'), '') AS LoggedToCompoundDate,
							user_vehicle_to_location.Username AS LoggedToCompoundUser,
							location.Name AS Location,
							location.Comment AS LocationComment,
							location.Active AS Active,
							e_mail.E_Mail AS LocationEmail,
							phone_number.Phone AS LocationTelephoneNumber,
							web.Webaddress AS LocationWebsite,
							IF(vts_preperation1.Vehicle_To_Status_ID,
								IFNULL(DATE_FORMAT(vts_preperation1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
								'-'
							) AS PreparationCompletedDate,
							IF(vts_preperation1.Vehicle_To_Status_ID,
								user_vts_preperation1.Username,
								'-'
							) AS PreparationCompletedUser,
							IF(ss_release.Service_Status_ID AND vts_release1.Vehicle_To_Status_ID,
								IFNULL(DATE_FORMAT(vts_release1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
								'-'
							) AS ReleaseToCustomerCreatedDate,
							IF(vts_delivered1.Vehicle_To_Status_ID,
								IFNULL(DATE_FORMAT(vts_delivered1.Set_Date, '%d/%m/%Y %H:%i:%s'), ''),
								'-'
							) AS ReleaseToCustomerCompletedDate,
							IF(vts_delivered1.Vehicle_To_Status_ID,
								user_vts_delivered1.Username,
								'-'
							) AS ReleaseToCustomerCompletedUser,
							IF(location.Active = 0, 'Yes', 'No') AS LocationDeleted,
							IF(location.Active = 0, location.Set_Date, 'No') AS LocationDeletedDate,
							IF(location.Active = 0, location.User_ID, 'No') AS LocationDeleted_User,
							' ' AS LoggedOutOfCompoundDate,
							#cb_vehicle_status.EN AS CompoundStatus,
							IF(vehicle_status.Translation_Code = 'vehicle_status_delivered',
								'Delivered',
								IF(vehicle_status.Translation_Code = 'status_authorised_release',
									'Release to customer',
									IF(vehicle_status.Translation_Code = 'vehicle_status_arrived',
										(CASE vehicle.Owner_ID WHEN 11 THEN 'Waiting for inspection' ELSE 'In preparation' END),
										IF(vehicle_status.Translation_Code = 'vehicle_status_preparation_completed',
											(CASE vehicle.Owner_ID WHEN 11 THEN 'In preparation' ELSE 'Waiting for inspection' END),
											IF(vehicle_status.Translation_Code = 'vehicle_status_on_hold' OR vth_hold1.Hold_Template_ID = 1,
												'On hold',
												IF(vehicle.Owner_ID = 35 AND vehicle_status.Translation_Code = 'vehicle_status_awaiting_signed_pickup_document'
													AND EXISTS (SELECT Service_Status_ID FROM service_status WHERE service_status.Service_ID = utilisation.Service_ID AND Service_Status_Template_ID IN (3)),
													'Awaiting signed pickup document',
												IF(utilisation.Utilisation_Result_Type_ID = 2 AND NOT EXISTS (SELECT Service_Status_ID FROM service_status WHERE service_status.Service_ID = utilisation.Service_ID AND Service_Status_Template_ID IN (2,3)),
													'Sold not Paid',
													IF(vehicle_status.Translation_Code = 'vehicle_status_inspection_completed' OR vehicle_status.Translation_Code = 'vehicle_status_inactive',
														'On sale/Sold',
													cb_vehicle_status.EN
							)))))))) AS CompoundStatus,
							IF(vts_backInfleet1.Vehicle_Status_ID, 'Yes', 'No') AS HistoricBackInfleet,
							IFNULL(DATE_FORMAT(vts_backInfleet1.Set_Date, '%d/%m/%Y %H:%i:%s'), '')AS HistoricBackInfleetDate,
							vts_backInfleet1.Comment AS HistoricBackInfleetComment,
							vehicle.Owner_ID AS OwnerID
						FROM vehicle
							JOIN vehicle_to_location ON (vehicle.Vehicle_ID = vehicle_to_location.Vehicle_ID)# AND vehicle_to_location.Active = 1)
								JOIN location ON (vehicle_to_location.Location_ID = location.Location_ID)
									LEFT JOIN location_to_e_mail ltem1 ON (location.Location_ID = ltem1.Location_ID)
									LEFT JOIN location_to_e_mail ltem2 ON (location.Location_ID = ltem2.Location_ID AND ltem1.Location_To_E_Mail_ID < ltem2.Location_To_E_Mail_ID)
										LEFT JOIN e_mail ON (ltem1.E_Mail_ID = e_mail.E_Mail_ID)
									LEFT JOIN location_to_phone_number ltpn1 ON (location.Location_ID = ltpn1.Location_ID)
									LEFT JOIN location_to_phone_number ltpn2 ON (location.Location_ID = ltpn2.Location_ID AND ltpn1.Location_To_Phone_Number_ID < ltpn2.Location_To_Phone_Number_ID)
										LEFT JOIN phone_number ON (ltpn1.Phone_Number_ID = phone_number.Phone_Number_ID)
									LEFT JOIN location_to_web ltw1 ON (location.Location_ID = ltw1.Location_ID)
									LEFT JOIN location_to_web ltw2 ON (location.Location_ID = ltw2.Location_ID AND ltw1.Location_To_Web_ID < ltw2.Location_To_Web_ID)
										LEFT JOIN web ON (ltw1.Web_ID = web.Web_ID)
								JOIN `user` user_vehicle_to_location ON (vehicle_to_location.User_ID = user_vehicle_to_location.User_ID)
							#JOIN partner ON (vehicle.Owner_ID = partner.Partner_ID)
							LEFT JOIN vehicle_to_status vts_preperation1 ON (vehicle.Vehicle_ID = vts_preperation1.Vehicle_ID AND vts_preperation1.Vehicle_Status_ID = 5)
							LEFT JOIN vehicle_to_status vts_preperation2 ON (vehicle.Vehicle_ID = vts_preperation2.Vehicle_ID AND vts_preperation2.Vehicle_Status_ID = 5 AND vts_preperation1.Vehicle_To_Status_ID < vts_preperation2.Vehicle_To_Status_ID)
								LEFT JOIN `user` user_vts_preperation1 ON (vts_preperation1.User_ID = user_vts_preperation1.User_ID)
							LEFT JOIN vehicle_to_status vts_release1 ON (vehicle.Vehicle_ID = vts_release1.Vehicle_ID AND vts_release1.Vehicle_Status_ID = 7)
							LEFT JOIN vehicle_to_status vts_release2 ON (vehicle.Vehicle_ID = vts_release2.Vehicle_ID AND vts_release2.Vehicle_Status_ID = 7 AND vts_release1.Vehicle_To_Status_ID < vts_release2.Vehicle_To_Status_ID)
							LEFT JOIN utilisation ON (vehicle.Vehicle_ID = utilisation.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID != 4)
								LEFT JOIN service ON (utilisation.Service_ID = service.Service_ID AND service.Deleted = 0)
									LEFT JOIN service_status ss_release ON (service.Service_ID = ss_release.Service_ID AND ss_release.Service_Status_Template_ID = 3)
							LEFT JOIN vehicle_to_status vts_delivered1 ON (vehicle.Vehicle_ID = vts_delivered1.Vehicle_ID AND vts_delivered1.Vehicle_Status_ID = 6)
							LEFT JOIN vehicle_to_status vts_delivered2 ON (vehicle.Vehicle_ID = vts_delivered2.Vehicle_ID AND vts_delivered2.Vehicle_Status_ID = 6 AND vts_delivered1.Vehicle_To_Status_ID < vts_delivered2.Vehicle_To_Status_ID)
								LEFT JOIN `user` user_vts_delivered1 ON (vts_delivered1.User_ID = user_vts_delivered1.User_ID)
							LEFT JOIN vehicle_to_status vts_backInfleet1 ON (vehicle.Vehicle_ID = vts_backInfleet1.Vehicle_ID AND vts_backInfleet1.Vehicle_Status_ID = 20)
							LEFT JOIN vehicle_to_status vts_backInfleet2 ON (vehicle.Vehicle_ID = vts_backInfleet2.Vehicle_ID AND vts_backInfleet2.Vehicle_Status_ID = 20 AND vts_backInfleet1.Vehicle_To_Status_ID < vts_backInfleet2.Vehicle_To_Status_ID)
							LEFT JOIN vehicle_to_status vts1 ON (vehicle.Vehicle_ID = vts1.Vehicle_ID)
							LEFT JOIN vehicle_to_status vts2 ON (vehicle.Vehicle_ID = vts2.Vehicle_ID AND vts1.Vehicle_To_Status_ID < vts2.Vehicle_To_Status_ID)
								#LEFT JOIN vehicle_status ON (get_vehicle_statusid(vts1.Vehicle_Status_ID, vehicle.Owner_ID) = vehicle_status.Vehicle_Status_ID)
								LEFT JOIN vehicle_status ON (vts1.Vehicle_Status_ID = vehicle_status.Vehicle_Status_ID)
									LEFT JOIN codebase cb_vehicle_status ON (vehicle_status.Translation_Code = cb_vehicle_status.Translation_Code)
							LEFT JOIN vehicle_to_hold vth_hold1 ON (vehicle.Vehicle_ID = vth_hold1.Vehicle_ID )
							LEFT JOIN vehicle_to_hold vth_hold2 ON (vehicle.Vehicle_ID = vth_hold2.Vehicle_ID AND vth_hold1.Vehicle_To_Hold_ID < vth_hold2.Vehicle_To_Hold_ID)
						WHERE
							vehicle.Owner_ID IN (",PID,")
							AND IFNULL(ltem2.Location_To_E_Mail_ID, 0) = 0
							AND IFNULL(ltpn2.Location_To_Phone_Number_ID, 0) = 0
							AND IFNULL(ltw2.Location_To_Web_ID, 0) = 0
							AND IFNULL(vts_preperation2.Vehicle_To_Status_ID, 0) = 0
							AND IFNULL(vts_release2.Vehicle_To_Status_ID, 0) = 0
							AND IFNULL(vts_delivered2.Vehicle_To_Status_ID, 0) = 0
							AND IFNULL(vts_backInfleet2.Vehicle_To_Status_ID, 0) = 0
							AND IFNULL(vts2.Vehicle_To_Status_ID, 0) = 0
							AND IFNULL(vth_hold2.Vehicle_To_Hold_ID, 0) = 0
							AND vehicle.Deleted = 0
							AND vehicle.Owner_ID != 1
						");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
