DROP PROCEDURE IF EXISTS `procedure_x_border_request`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_x_border_request`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
# Variablen definieren
DECLARE DEFAULT_CALL_TYPE TEXT;
DECLARE DEFAULT_TABLES TEXT;
DECLARE DEFAULT_ROWS TEXT;
DECLARE DEFAULT_FILTER TEXT;
DECLARE TABELLEN TEXT;
DECLARE BEDINGUNG TEXT DEFAULT " ";
DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
DECLARE BEGRENZUNG TEXT;
# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
END IF;
# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
END IF;

SET DEFAULT_ROWS = "";
SET DEFAULT_FILTER = "";
SET GRUPPIERUNG = "";
CASE 
	WHEN CALL_TYPE = 'count' THEN
	SET DEFAULT_ROWS = CONCAT(" vehicle.Vehicle_ID ");
	SET DEFAULT_TABLES = " x_border_request ";
	SET GRUPPIERUNG = "";
		WHEN CALL_TYPE = 'list' OR CALL_TYPE = 'freetext' THEN
		SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
		SET DEFAULT_ROWS = CONCAT("
				x_border_request.X_Border_Request_ID,
				x_border_request.Create_Set_Date,
				vehicle_make.Name AS Make,
				vehicle_model.Name AS Model,
				x_border_request.Derivative,
				cb_fuel_type.",LANG," AS Fuel_Type,
				x_border_request.Amount,
        		cb_color.",LANG," AS Color,
				");
		SET DEFAULT_TABLES = "
				x_border_request
				JOIN partner ON (x_border_request.Partner_ID = partner.Partner_ID)
				JOIN address ON (partner.Address_ID = address.Address_ID)
				JOIN country ON (address.Country_ID = country.Country_ID)
				JOIN vehicle_make ON (x_border_request.Make_ID = vehicle_make.Make_ID)
				JOIN vehicle_model ON (x_border_request.Model_ID = vehicle_model.Vehicle_Model_ID)
				JOIN fuel_type ON (x_border_request.Fuel_Type_ID = fuel_type.Fuel_Type_ID)
				JOIN codebase AS cb_fuel_type ON (fuel_type.Translation_Code = cb_fuel_type.Translation_Code)
        		LEFT JOIN color ON (color.Color_ID = x_border_request.Color_ID)
        		LEFT JOIN codebase AS cb_color ON (cb_color.Translation_Code = color.Translation_Code)
				";
		SET DEFAULT_FILTER = CONCAT("
									x_border_request.Deleted = 0 AND
									x_border_request.Partner_ID IN (",PID,")
									");
		SET GRUPPIERUNG = "";
END CASE;

CASE
	WHEN LIST_TYPE = 'open_request' THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
			cb_preferred_country.",LANG," AS Preferred_Country,
			cb_partner_country.",LANG," AS Partner_Country,
			x_border_request.Preferred_Sales_Date,
			x_border_request.Request_Comment
			");
		SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"
					LEFT JOIN country AS preferred_country ON (x_border_request.Preferred_Partner_Country_ID = preferred_country.Country_ID)
					LEFT JOIN codebase AS cb_preferred_country ON (preferred_country.Translation_Code = cb_preferred_country.Translation_Code)
					LEFT JOIN codebase AS cb_partner_country ON (country.Translation_Code = cb_partner_country.Translation_Code)
					");
		SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND x_border_request.X_Border_Request_Status = 'requested'
					");
	WHEN LIST_TYPE = 'answered_request' THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
			cb_preferred_country.",LANG," AS Preferred_Country,
			x_border_request.Preferred_Sales_Date,
			x_border_request.Request_Comment,
			cb_prospective_country.",LANG," AS Prospective_Country,
			x_border_request.Answer_Comment
			");
		SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"
					LEFT JOIN country AS preferred_country ON (x_border_request.Preferred_Partner_Country_ID = preferred_country.Country_ID)
					LEFT JOIN codebase AS cb_preferred_country ON (preferred_country.Translation_Code = cb_preferred_country.Translation_Code)
					LEFT JOIN country AS prospective_country ON (x_border_request.Prospective_Partner_Country_ID = prospective_country.Country_ID)
					LEFT JOIN codebase AS cb_prospective_country ON (prospective_country.Translation_Code = cb_prospective_country.Translation_Code)
					");
		SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND x_border_request.X_Border_Request_Status = 'answered'
					");
		WHEN LIST_TYPE = 'european' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
										cb_preferred_country.",LANG," AS Preferred_Country,
										cb_partner_country.",LANG," AS Partner_Country,
										x_border_request.Preferred_Sales_Date,
										x_border_request.Request_Comment
										");
		SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"
					LEFT JOIN country AS preferred_country ON (x_border_request.Preferred_Partner_Country_ID = preferred_country.Country_ID)
					LEFT JOIN codebase AS cb_preferred_country ON (preferred_country.Translation_Code = cb_preferred_country.Translation_Code)
					LEFT JOIN codebase AS cb_partner_country ON (country.Translation_Code = cb_partner_country.Translation_Code)
					");
		SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
					AND x_border_request.X_Border_Request_Status = 'requested'
					");
END CASE;

# Tabellen zusammensetzen
SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);

SET BEDINGUNG = CONCAT ('WHERE ');

# Bedingung fuer alle abfragen dieser Liste sind gleich
SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;		

# Limit setzen
IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
ELSE SET BEGRENZUNG = '';
END IF;
# Query zusammensetzen
SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
# Query vorbereiten
PREPARE stmt FROM @sql;
# Query ausfuehren
EXECUTE stmt;
# Query aufheben
DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
