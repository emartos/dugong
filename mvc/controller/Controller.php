<?php
/**
 * Controller.php
 * Abstract class for controllers
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
abstract class Controller {
    /**
     * @var Direct output or return
     */
    protected $directOutput;

    /**
     * Execute an action defined in the specific controller
     */
    public function executeAction() {
        $params = Parameters::getInstance();
        $action  = $params->get('action');
        if (method_exists($this, $action)) {
            $this->$action();
        }
    }
}