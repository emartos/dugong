<?php
/**
 * IController.php
 * Interface for controllers
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
interface IController {
    /**
     * Execute the controller
     */
    public static function getName();

    /**
     * Execute the controller
     */
    public function execute();
}