<?php
/**
 * Dispatcher.php
 * Front controller
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Dispatcher {
    /**
     * Route the execution
     */
    public function dispatch() {
        // Set the route
        $route = $this->setRoute();
        if (class_exists($route) || class_exists(ucfirst($route))) {
            try {
                // Execute the controller
                $controller = new $route();
                if (method_exists($controller, 'executeAction')) {
                    $controller->executeAction();
                }
                $controller->execute();
            } catch (CDBException $e) {
                print $e->getMessage();
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
    }

    /**
     * Set the route
     * @return bool
     */
    private function setRoute() {
        $params = Parameters::getInstance();
        $route = $params->getParamValue('route', 'defaultRoute');
        return $route;
    }
}