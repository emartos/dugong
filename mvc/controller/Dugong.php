<?php
/**
 * Dugong.php
 * Abstract class to render PDFs
 * @author Alejandro Galache
 * @author Eduardo Martos <emartos@natiboo.es>
 * All Dugong code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Dugong extends Controller implements IController {
    /**
     * @var String Location of the data to execute
     */
    private $data;
    /**
     * @var String Status of the executed script (pending|failure|success)
     */
    private $viewName;
    /**
     * @var  Array values for the databases stored in the config file
     */
    private $databases;
    /**
     * @var array Scripts failed in the mass action
     */
    private $massFailedScripts;
    /**
     * @var array Scripts executed in the mass action
     */
    private $massPassedScripts;

    /**
     * Class constructor
     * @param bool $directOutput
     */
    public function __construct($directOutput = true) {
        $this->directOutput = $directOutput;
        $params = Parameters::getInstance();
        $this->data = $params->get('dataPath');
        $dblist = $params->get('database');
        foreach($dblist as $key => $data) {
            $this->databases[] = $key;
        }
        $this->viewName = $params->getParamValue('view', 'defaultView');
        $this->massFailedScripts = array();
        $this->massPassedScripts = array();
        $env = $params->get('env') ? $params->get('env') : $params->get('default-env');
        $params->set('env', $env, true);
    }

    /**
     * Retrieve the class name
     * @return string
     */
    public static function getName() {
        return strtolower(__CLASS__);
    }

    /**
     * Moves script from failure to pending section, having the possibility to re-execute it
     */
    public function moveScript () {
        $params = Parameters::getInstance();
        $filename = $params->get('file');
        $type = $params->get('type') . "/";
        $scriptPath = $params->get('dataPath') . $type . $filename;
        MetadataHandler::unsetMetaData($scriptPath);
        $messageBus = MessageBus::getInstance();
        $messageBus->put('', 'Script moved correctly');
    }

    /**
     * Execute a query
     * @param string $filename
     * @param string $file
     * @param string $foldertype
     * @param bool   $massAction
     *
     *  Execute a SQL query from a file
     */
    public function executeQuery($filename = '', $file = '', $foldertype = '', $massAction = false) {
        // @todo This functionality enables demo mode. For correct performance, disable demo-mode in config.json
        $params = Parameters::getInstance();
        $demo_mode = $params->get("demo-mode");
        if($demo_mode == "enabled") {
            $messageBus = MessageBus::getInstance();
            $messageBus->put('', 'You are in demo mode. The scripts has not been executed.');
            return;
        }
        // END demo mode functionality

        $params = Parameters::getInstance();
        $mysqlhandler = new MysqlHandler($params->get('env'));
        if ($massAction != true) {
            $filename = $params->get('file');
            $type = $params->get('type') . "/";
            if (!$filename) {
                $filename = $file;
            }
            if (!$type) {
                $type = $foldertype;
            }
        } else {
            $type = $foldertype;
        }
        $scriptPath = $params->get('dataPath') . $type . $filename;
        $queryText = File::read($scriptPath);
        // If the script contains a procedure, we may split up and execute the different sentences
        $queryText = $mysqlhandler->polishDelimiter($queryText);
        if ($mysqlhandler->execute($queryText)) {
            MetadataHandler::setMetaData($scriptPath, '');
            $this->massPassedScripts[] = $filename;
            $message = 'Execution successful';
        }
        else {
            MetadataHandler::setMetaData($scriptPath, $mysqlhandler->errMessage);
            $this->massFailedScripts[] = array(
                "filename" => $filename,
                "errmsg" => $mysqlhandler->errMessage
            );
            $message = 'There is an error in the executed query '.$mysqlhandler->errMessage;
        }
        if (!$massAction) {
            $messageBus = MessageBus::getInstance();
            $messageBus->put('', $message);
        }
    }

    /**
     * Get the file to execute, and read it
     */
    public function getFile() {
        $params = Parameters::getInstance();
        $file = $params->get('file');
        $content = File::read($file);
        if ($params->get('formatter')) {
            $content = SqlFormatter::format($content);
        }
        print $content;
        die;
    }

    /**
     *  Write new content in a file
     */
    public function saveFile() {
        $params = Parameters::getInstance();
        $file = $params->get('file');
        $content = $params->get('filecontent');
        File::write($file, $content);
    }

    /**
     * Change between environments defined in config file, to execute the scripts on
     */
    public function changeEnvironment() {
        $params = Parameters::getInstance();
        $env = $params->get('env');
        $params->set('env', $env, true);
    }

    /**
     * Render the content
     * @return bool
     */
    public function execute() {
        // Render the content into HTML
        $params = Parameters::getInstance();
        $renderer = new Renderer($this->viewName);
        $renderer->setViewPath($params->get('viewPath'));
        $messageBus = MessageBus::getInstance();
        $files = $this->browseDirectory('/^.+\.sql$/i');
        if (empty($files)) {
            $messageBus->put('', 'You must configure the data folder');
        }
        $messages = $messageBus->getMessages();
        $contentArray = array(
            'folder' => $this->browseDirectory('/^.+\.sql$/i'),
            'databases' => $this->databases,
            'imgurl' => APP_URL . $params->get('imagePath'),
            'environment' => $params->get('env'),
            'currentSection' => $this->viewName,
            'messages' => $messages,
            'intro' => $params->get('intro')
        );
        $content = $renderer->render($contentArray);
        if ($this->directOutput) {
            print $content;
        } else {
            return $content;
        }
    }

    /**
     * Execute multiple scripts from the selected list results
     */
    public function executeMassQuery() {
        $messageBus = MessageBus::getInstance();
        $params = Parameters::getInstance();
        $checked = $params->get('check');
        if ($checked) {
            $fileArray = $params->get('file');
            $typeArray = $params->get('type');
            foreach ($checked as $key => $value) {
                $file = $fileArray[$key];
                $type = $typeArray[$key] . '/';
                $this->executeQuery($value, $file, $type, true);
            }
            if (count($this->massPassedScripts) > 0) {
                $messageBus->put('exec_ok', 'Execution succesful:');
                foreach($this->massPassedScripts as $key => $value) {
                    $messageBus->put('', $value);
                }
            }
            if (count($this->massFailedScripts) > 0) {
                $messageBus->put('exec_ko', 'Execution failed:');
                foreach ($this->massFailedScripts as $key => $value) {
                    $messageBus->put('', $value["filename"] . ': ' . $value["errmsg"]);
                }
            }
        } else {
            $messageBus->put('', 'You have to select at least one script to be executed');
        }
    }

    /**
     * Process a directory to find the suitable files
     * @param string $pattern
     * @return array|string
     */
    public function browseDirectory($pattern = '') {
        $ret = '';
        $params = Parameters::getInstance();

        // @todo This functionality enables demo mode. For correct performance, disable demo-mode in config.json
        $demo_mode = $params->get("demo-mode");
        if($demo_mode == "enabled") {
            $params->set('dataPath', $_SERVER['DOCUMENT_ROOT'].$params->get('demodataPath'));
        }
        // END demo mode functionlity

        $data = MetadataHandler::getMetaData($params->get('dataPath').'/');
        $fileArray = File::getFileArray($params->get('dataPath'), $pattern);
        $params = Parameters::getInstance();
        $viewName = $params->getParamValue('view', 'defaultView');
        if (!empty($fileArray)) {
            foreach($fileArray as $name => $object){
                $pathinfo = pathinfo($name);
                $content = File::read($name);
                $hash = hash('md5', $content);
                if(isset($data[$hash]) && ($data[$hash]['errorMessage'] == '')) {
                    // Successfully Executed
                    $php_date = date("d F Y H:i:s T P", $data[$hash]['date']);
                    $ret[] = array(
                        'modtime' => filemtime($params->get('dataPath') .
                                               basename($pathinfo['dirname']) . '/' . $pathinfo['basename']),
                        'data' => $params->get('dataPath'),
                        'result' => 'success',
                        'file' => $pathinfo['basename'],
                        'type' => basename($pathinfo['dirname']),
                        'exec_date' => $php_date,
                        'action' => ''
                    );
                } elseif (isset($data[$hash]) && ($data[$hash]['errorMessage'] != '')) {
                    // Failed Execution
                    $php_date = date("d F Y H:i:s T P", $data[$hash]['date']);
                    $ret[] = array(
                        'modtime' => filemtime($params->get('dataPath') .
                                               basename($pathinfo['dirname']) . '/' . $pathinfo['basename']),
                        'data' => $params->get('dataPath'),
                        'result' => 'failure',
                        'type' => basename($pathinfo['dirname']),
                        'file' => $pathinfo['basename'],
                        'exec_date' => $php_date,
                        'error_msg' => $data[$hash]['errorMessage'],
                        'actions' => array(
                            array(
                                'name' => 'Move to pending',
                                'href' => '?view='.$viewName.'&action=moveScript&file='.$pathinfo['basename'].'&type='.basename($pathinfo['dirname']),
                                'target' => '_self'
                            )
                        )
                    );
                } else {
                    $ret[] = array(
                        'modtime' => filemtime($params->get('dataPath') .
                                               basename($pathinfo['dirname']) . '/' . $pathinfo['basename']),
                        'data' => $params->get('dataPath'),
                        'result' => 'pending',
                        'type' => basename($pathinfo['dirname']),
                        'checkbox' => 'true',
                        'file' => $pathinfo['basename'],
                        'exec_date' => '(Not executed)',
                        'status' => '(Not executed)',
                        'error_msg' => '(Not executed)',
                        'actions' => array(
                            array(
                                'name' => 'execute',
                                'href' => '?view='.$viewName.'&action=executeQuery&file='.$pathinfo['basename'].'&type='.basename($pathinfo['dirname']),
                                'target' => '_self',
                            ),
                            array(
                                'name' => 'show in Git',
                                'href' => $params->get('jira_root_url').'projects/FCM/repos/fcm/diff/DBScripts/'.basename($pathinfo['dirname']).'/'.$pathinfo['basename'],
                                'target' => '_blank',
                            ),
                        )
                    );
                }
            }
        }
        return $ret;
    }

}