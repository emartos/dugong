<div id="content">
    <h4>Scripts executed successfully:</h4>
    <div class="clear">&nbsp;</div>
    <table id="scriptlist" class="table dataTable table-striped table-bordered" cellspacing="0" width="100%">
        <thead class="step14">
        <tr>
            <th>Modification Date</th>
            <th>Script Name</th>
            <th>Type</th>
            <th>Execution Date</th>
            <th class="step15">Action</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Modification Date</th>
            <th>Script Name</th>
            <th>Type</th>
            <th>Execution Date</th>
            <th>Action</th>
        </tr>
        </tfoot>
        <tbody>
        {% if folder is not empty %}
        {% for colKey,file in folder %}
        {% if file.result == 'success' %}
        <tr>
            <td><div>{{ file.modtime }}</div></td>
            <td><div>{{ file.file }}</div></td>
            <td><div>{{ file.type }}</div></td>
            <td><div>{{ file.exec_date }}</div></td>
            <td>
                <div><a class="show-action" id="editfile_{{ colKey }}" data-root="{{ file.data }}" data-type="{{ file.type }}" data-file="{{ file.file }}" href="#">show Content</a></div>
            </td>
        </tr>
        {% endif %}
        {% endfor %}
        {% else %}
        <tr>
            <td><b>No results found</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        </tr>
        {% endif %}
        </tbody>
    </table>
    <div class="clear">&nbsp;</div>
</div>
<script type="text/javascript">successSteps();</script>