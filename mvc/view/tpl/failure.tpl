<div id="content">
    <h4>Failed execution scripts:</h4>
    <div class="clear">&nbsp;</div>
    <form id ="mass-query" action="?view=failure&action=executeMassQuery" method="post">
        <table id="scriptlist" class="table dataTable table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr id="step10">
                    <th>Modification Date</th>
                    <th>&nbsp;</th>
                    <th>Script Name</th>
                    <th>Type</th>
                    <th id="step11">Execution Date</th>
                    <th id="step12">Error Msg</th>
                    <th id="step13">Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Modification Date</th>
                    <th>&nbsp;</th>
                    <th>Script Name</th>
                    <th>Type</th>
                    <th>Execution Date</th>
                    <th>Error Msg</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
                {% if folder is not empty %}
                    {% for colKey,file in folder %}
                        {% if file is defined %}
                            {% if file.result == 'failure' %}
                                <tr>
                                    <td><div>{{ file.modtime }}</div></td>
                                    <td><input type="checkbox" id="check[{{ loop.index0 }}]" class="check" name="check[{{ loop.index0 }}]" value="{{ file.file }}" /></td>
                                    <td><div>{{ file.file }}</div></td>
                                    <td><div>{{ file.type }}</div></td>
                                    <td><div>{{ file.exec_date }}</div></td>
                                    <td><div>{{ file.error_msg }}</div></td>
                                    <td>
                                        {% for action in file.actions %}
                                        <div><a href="{{ action.href }}"  class="{{ action.name }}" data-env="{{ environment }}" target="{{ action.target }}">{{ action.name }}</a></div>
                                        {% endfor %}
                                        <div><a class="edit-action" id="editfile_{{ colKey }}" data-root="{{ file.data }}" data-type="{{ file.type }}" data-file="{{ file.file }}" href="#">Edit</a></div>
                                        <div><a class="show-action" id="editfile_{{ colKey }}" data-root="{{ file.data }}" data-type="{{ file.type }}" data-file="{{ file.file }}" href="#">Show Content</a></div>
                                    </td>
                                    <input type="hidden" name="file[{{ loop.index0 }}]" id="file" value="{{ file.file }}" />
                                    <input type="hidden" name="type[{{ loop.index0 }}]" id="type" value="{{ file.type }}" />
                                    </tr>
                            {% endif %}
                        {% endif %}
                    {% endfor %}
                {% else %}
                    <tr>
                        <td><b>No results found</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                {% endif %}
            </tbody>
        </table>
        <input type="hidden" name="retry" id="retry" value="true" />
        <div class="clear">&nbsp;</div>
        <button type="button" class="btn" id="check-all">Check All</button>
        <button type="button" class="btn" id="uncheck-all">Uncheck All</button>
        <input type="submit" name="set_mass" id="set_mass" class="btn btn-primary" value="Execute scripts" />
    </form>
</div>
<script type="text/javascript">failureSteps();</script>