<div id="header">
    <div id="top-bar" class="step1">
        <div id="menu-wrapper">
            <ul id="menu" class="step2">
                <li><a class="btn{% if currentSection == "pending" %} selected-section{% endif %}" href="?view=pending">Pending</a></li>
                <li><a class="btn{% if currentSection == "failure" %} selected-section{% endif %}" href="?view=failure">Failure</a></li>
                <li><a class="btn{% if currentSection == "success" %} selected-section{% endif %}" href="?view=success">Success</a></li>
            </ul>
        </div>
        <div id="current-env" class="step4">{{ environment }} Environment Selected</div>
        <div id="change-env" class="step3">
            <form action="?action=changeEnvironment" method="post">
                <span class="chng-env">Change Environment: &nbsp;&nbsp;</span>
                <select name="env" id="select-env" onchange="this.form.submit()">
                    {% for key,item in databases %}
                    <option value="{{ item }}"
                            {% if environment == item %}
                                selected
                            {% endif %}>{{ item }}</option>
                    {% endfor %}
                </select>
            </form>
        </div>
    </div>
    <div class="clear"><p>&nbsp;</p></div>
    <img src="{{ imgurl }}logo.png" id="logo" />
    <h1 class="title">Dugong | Your friendly DBV</h1>
    <div class="clear">&nbsp;</div>
    {% if messages|length > 0 %}
        <div class="jumbotron" id="messages" title="Messages">
        {% for key, value in messages %}
            {% if (key == 'exec_ok' or key == 'exec_ko') %}
                <p><strong><u>{{ value }}</u></strong></p>
            {% else %}
            <p>{{ value }}</p>
            {% endif %}
        {% endfor %}
        </div>
        <script>
            $(document).ready(function() {
                $("#messages").dialog({
                    height: 'auto',
                    width: 720,
                    modal: true,
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        </script>
    {% endif %}
</div>