<div id="content">
    <h4>Pending scripts to execute:</h4>
    <div class="clear">&nbsp;</div>
    <form id ="mass-query" action="?view=pending&action=executeMassQuery" method="post">
        <table id="scriptlist" class="table dataTable table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr class="step5">
                    <th>Modification Date</th>
                    <th class="step7">&nbsp;</th>
                    <th>Script Name</th>
                    <th class="step8">Type</th>
                    <th class="step9">Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr class="step6">
                    <th>Modification Date</th>
                    <th>&nbsp;</th>
                    <th>Script Name</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
                {% if folder is not empty %}
                    {% for colKey,file in folder %}
                        {% if file.result == 'pending' %}
                        <tr>
                            <td><div>{{ file.modtime }}</div></td>
                            <td><input type="checkbox" id="check[{{ loop.index0 }}]" name="check[{{ loop.index0 }}]" value="{{ file.file }}" {% if file.checkbox is empty %}disabled{% else %}class="check"{% endif %} /></td>
                            <td><div>{{ file.file }}</div></td>
                            <td><div>{{ file.type }}</div></td>
                            <td>
                                {% for action in file.actions %}
                                <div><a href="{{ action.href }}"  class="{{ action.name }}" data-env="{{ environment }}" target="{{ action.target }}">{{ action.name }}</a></div>
                                {% endfor %}
                                <div><a class="show-action" id="editfile_{{ colKey }}" data-root="{{ file.data }}" data-type="{{ file.type }}" data-file="{{ file.file }}" href="#">show Content</a></div>
                            </td>
                            <input type="hidden" name="file[{{ loop.index0 }}]" id="file" value="{{ file.file }}" />
                            <input type="hidden" name="type[{{ loop.index0 }}]" id="type" value="{{ file.type }}" />
                        </tr>
                        {% endif %}
                    {% endfor %}
                {% else %}
                    <tr>
                        <td><b>No results found</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    </tr>
                {% endif %}
            </tbody>
        </table>
        <div class="clear">&nbsp;</div>
        <button type="button" class="btn" id="check-all">Check All</button>
        <button type="button" class="btn" id="uncheck-all">Uncheck All</button>
        <input type="submit" name="set_mass" id="set_mass" class="btn btn-primary execute" value="Execute scripts" />
    </form>
    <div id="messages" style="display: none;" title="Messages">
        <p>You are going to execute selected scripts on <strong>{{ environment }}</strong> environment.</p>
        <p>Are you sure?</p>
    </div>
</div>
{% if intro == 'true' %}
<script type="text/javascript">pendingSteps();</script>
{% endif %}
