<?php
/**
 * Renderer.php
 * Render content into HTML format
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Renderer {
    private $basePath;
    private $baseUrl;
    private $cssPath;
    private $cssUrl;
    private $jsPath;
    private $jsUrl;
    private $imgUrl;
    private $htmlHeaderTpl;
    private $htmlFooterTpl;
    private $headerTpl;
    private $footerTpl;
    private $bodyTpl;
    private $includeHeaderFooter;
    private $htmlPath;

    /**
     * Class constructor
     * @param $view
     * @param bool|true $includeHeaderFooter
     */
    public function __construct($view, $includeHeaderFooter = true) {
        $params = Parameters::getInstance();
        $this->basePath = APP_ROOT . $params->get('viewPath');
        $this->baseUrl = APP_URL . $params->get('viewPath');
        $this->cssPath = APP_ROOT . $params->get('cssPath');
        $this->cssUrl = APP_URL . $params->get('cssPath');
        $this->jsPath = APP_ROOT . $params->get('jsPath');
        $this->jsUrl = APP_URL . $params->get('jsPath');
        $this->imgUrl = APP_URL . $params->get('imagePath');
        $this->htmlHeaderTpl = $params->get('htmlHeaderTpl');
        $this->htmlFooterTpl = $params->get('htmlFooterTpl');
        $this->headerTpl = $params->get('headerTpl');
        $this->footerTpl = $params->get('footerTpl');
        $this->bodyTpl = strtolower($view . '.tpl');
        $this->htmlPath = $params->get('htmlPath');
        $this->tempPath = $params->get('tempPath');
        $this->includeHeaderFooter = $includeHeaderFooter;
    }

    /**
     * Set the view path
     * @param $viewPath
     */
    public function setViewPath($viewPath) {
        $this->basePath = APP_ROOT . $viewPath;
        $this->baseUrl = APP_URL . $viewPath;
    }

    /**
     * Set the current view
     * @param $view
     */
    public function setView($view) {
        $this->bodyTpl = strtolower($view . '.tpl');
    }

    /**
     * Set the include header/footer mode
     * @param $includeHeaderFooter
     */
    public function setIncludeHeaderFooter($includeHeaderFooter) {
        $this->includeHeaderFooter = $includeHeaderFooter;
    }

    /**
     * Generate the header/footer
     * @param       $tplObj
     * @param       $tpl
     * @param array $contentArray
     * @return mixed
     */
    private function renderPart($tplObj, $tpl, $contentArray = array()) {
        return $tplObj->render($tpl, $contentArray);
    }

    /**
     * Insert the default header
     * @param $tplObj
     * @return mixed
     */
    private function renderHtmlHeader($tplObj) {
        $params = Parameters::getInstance();
        $content = array(
            'css' => File::browseDirectory($this->cssPath, $this->cssUrl),
            'js' => File::browseDirectory($this->jsPath, $this->jsUrl),
            'imgurl' => $this->imgUrl,
            'nonce' => $params->get('nonce'),
        );
        return $tplObj->render($this->htmlHeaderTpl, $content);
    }

    /**
     * Insert the default footer
     * @param $tplObj
     * @return mixed
     */
    private function renderHtmlFooter($tplObj) {
        return $tplObj->render($this->htmlFooterTpl);
    }

    /**
     * Render content into HTML format
     * @param null $contentArray
     * @return string
     */
    public function render($contentArray = null) {
        if (isset($contentArray)) {
            Twig_Autoloader::register();
            $loader = new Twig_Loader_Filesystem($this->basePath);
            $tplObj = new Twig_Environment($loader);
            $body = $tplObj->render($this->bodyTpl, $contentArray);
            if ($this->includeHeaderFooter) {
                $this->setViewPath($this->htmlPath);
                $htmlHeader = $this->renderHtmlHeader($tplObj);
                $htmlFooter = $this->renderHtmlFooter($tplObj);
                $header     = $this->renderPart($tplObj, $this->headerTpl, $contentArray);
                $footer     = $this->renderPart($tplObj, $this->footerTpl, $contentArray);
            } else {
                $htmlHeader = $htmlFooter = $header = $footer = '';
            }
            $content = $htmlHeader . $header. $body . $footer . $htmlFooter;
        }
        return $content;
    }
}