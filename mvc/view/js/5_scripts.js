$(document).ready(function() {
    function showFileDialog(elem, readonly) {
        // Create the modal form
        var file = $(elem).attr("data-root") + $(elem).attr("data-type")+ "/" + $(elem).attr("data-file");
        var action = "?action=saveFile&file=" + file;
        var wrapper = $('<div id="' + $(elem).attr("id") + '" title="File content"></div>');
        var form = $('<form method="post" class="editform" action="' + action + '"></form>');
        if(readonly) {
            var fileContainer = $('<div></div>');
            var buttons = "";
            var formatter = "&formatter=true";
        }else {
            var fileContainer = $('<textarea name="filecontent" class="editfile-field"></textarea>');
            var buttons = {
                "Save": function() {
                    form.submit();
                    $(wrapper).dialog("close");
                }
            };
            var formatter = "";
        }
        wrapper.append(form);
        form.append(fileContainer);
        // Retrieve the file contents via AJAX
        $.get("?action=getFile&file=" + file + formatter, function(data, status){
            if(readonly){
                fileContainer.html(data);
            } else {
                fileContainer.text(data);
            }
            // Show the modal form with the file contents
            $(wrapper).dialog({
                height: $(window).height() - 120,
                width: $(window).width() - 60,
                modal: true,
                buttons: buttons
            });
        });
    }

    $('#check-all').click(function(){
        $("input:checkbox").prop('checked', true);
    });
    $('#uncheck-all').click(function(){
        $("input:checkbox").prop('checked', false);
    });
    $(".edit-action").click(function() {
       showFileDialog(this, false);
    });
    $(".show-action").click(function() {
        showFileDialog(this, true);
    });
    // Setup - add a text input to each footer cell
    $('#scriptlist tfoot th').each( function () {
        var title = $('#scriptlist thead th').eq($(this).index()).text();
        if(title.length > 1){
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width:166px;" />' );
        }
    });
    // Datatble
    var table = $('#scriptlist').DataTable({
        "order": [[0, "desc"]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ]
    });
    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

    // Execute Confirmation
    $("#dialog").dialog({
        autoOpen: false,
        modal: true
    });
    $(".execute").click(function (e) {
        e.preventDefault();
        var element = e.target;
        var element_id = e.target.id;
        $("#messages").dialog({
            height: 300,
            width: 650,
            modal: true,
            buttons: {
                Close: function() {
                    $(this).dialog("close");
                },
                'Proceed': function() {
                    if(element_id == 'set_mass') {
                        $('#mass-query').submit();
                    }else {
                        var destination = $(element).attr('href');
                        window.location.href = destination;
                    }
                }
            }
        });
    });
});