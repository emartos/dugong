function pendingSteps() {
    var intro = introJs();
    intro.setOption('doneLabel', 'Next page');
    intro.setOptions({
        steps: [
            {
                intro: '<h3 style=\"margin-left:18px;\">Dugong Step-by-Step Guide</h3>'
            },
            {
                element: '.step1',
                intro: 'Here in the top bar we\'ll have different tabs for each execution status, and the posiblity to switch between environments to execute the scripts on.<br> This environments will be defined in the config file database.json'
            },
            {
                element: '.step2',
                intro: 'Status for the scripts are <b>Pending</b>, <b>Failure</b> and <b>Success</b>. In pending part we\'ll have all scripts that\'s been updated, and we haven\'t execute.<br> Depending on the execution result, they will go to <b>failure</b> or <b>success</b> part'
            },
            {
                element: '.step3',
                intro: 'In this part we\'ll be able to change the environment where we want to execute the scripts.<br> Obviously, different environments will have different results depending on which scripts we executed in each environment'
            },
            {
                element: '.step4',
                intro: 'This label shows the current environment, where scripts will be executed when we perfom the actions',
                position: 'right'
            },
            {
                element: '.step5',
                intro: 'This is the table structure for pending section.<br> Above this table, we have functionality for search and choose number of registries to be shown'
            },
            {
                element: '.step6',
                intro: 'Moreover, we\'ll be able to search for the content stored in <i>each</i> data column.'
            },
            {
                element: '.step7',
                intro: 'We\'ll have checkboxes for each row, to execute scripts by mass action. In the bottom part of the screen we have buttons for <b>Check All</b>, <b>Uncheck All</b> and <b>Execute scripts</b>'
            },
            {
                element: '.step8',
                intro: 'Type column will show the type of script (function, procedure, table, etc). This name matches with the folder the file is in.'
            },
            {
                element: '.step9',
                intro: 'For pending screen, we have this actions available:<br><ul><li><b>execute</b>: executes the script</li><li><b>show in Git</b>: link to the script in git, where we can compare it between different versions and branches</li><li><b>show Content</b>: Opens a dialog with the content of the file, formatted in SQL language.</li></ul>'
            }
        ]
    });
    intro.start().oncomplete(function() {
        window.location.href = '?view=failure&multipage=true';
    });
};

function failureSteps() {
    if (RegExp('multipage', 'gi').test(window.location.search)) {
        var intro = introJs();
        intro.setOption('doneLabel', 'Next page');
        intro.setOptions({
            steps: [
                {
                    intro: 'In the failure section, we will have the scripts with errors, and here we \'ll be able to modifie and re-execute it.'
                },
                {
                    element: '#step10',
                    intro: 'This is the table structure for the failure section'
                },
                {
                    element: '#step11',
                    intro: 'In addition to the other fields in pending section, we\'ll have a field with the execution date...'
                },
                {
                    element: '#step12',
                    intro: '...and the Error Message that was thrown during the execution'
                },
                {
                    element: '#step13',
                    intro: 'The actions available for this part are: <b><i>Edit script</i></b> and <b><i>Show content</i></b>.<br>Both actions will open a dialog with the script content, formatted in SQL language.<br>The action <b><i>Move to pending</i></b> will send the selected script back to the pending section. That way we will be able to execute the script, without modifying its content.'
                },
                {
                    intro: 'When you edit the script file either through the interface or manually, it will be moved automatically to the <i>pending</i> section, since the application detects any change in the file, in order to have the posibility to execute the new <i>version</i> of the file.'
                }
            ]
        });
        intro.start().oncomplete(function() {
            window.location.href = '?view=success&multipage=true';
        });
    }
};

function successSteps() {
    if (RegExp('multipage', 'gi').test(window.location.search)) {
        var intro = introJs();
        intro.setOptions({
            steps: [
                {
                    intro: 'In success part, we will see the script that have been executed correctly in the chosen database. When that script is modified, it will be moved to pending section.'
                },
                {
                    element: '.step14',
                    intro: 'For the table in success section, we will have a sort of columns we saw before'
                },
                {
                    element: '.step15',
                    intro: 'The actions available will only view the <b><i>show Content</i></b> action. If the file placed here is manually modified, it will return to the Pending part.'
                }
            ]
        });
        intro.start();
    }
};