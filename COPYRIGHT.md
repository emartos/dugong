Dugong, your friendly Database Version Control system.

Copyright (C) 2015 Eduardo Martos & Alejandro Galache

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Dugong includes works under other copyright notices and distributed
according to the terms of the GNU General Public License or a compatible
license, including:

    phpRAD - Copyright (c) 2014 by Eduardo Martos
    Twig - Copyright (c) 2009-2014 by the Twig Team
    SqlFormatter - Copyright (c) 2013 by Jeremy Dorn