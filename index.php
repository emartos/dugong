<?php
/**
 * Entry point
 * @author Eduardo Martos <emartos@natiboo.es>
 * @author Alejandro Galache
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
// Environment constants
ini_set('display_errors', true);
define('APP_ROOT', dirname($_SERVER['SCRIPT_FILENAME']) . '/');
$phpSelf = pathinfo($_SERVER['PHP_SELF']);
define('APP_URL', 'http://' . $_SERVER['HTTP_HOST'] . $phpSelf['dirname'] . '/');
define('APP_PORT', intval($_SERVER['SERVER_PORT']) != 80 ? ':' . $_SERVER['SERVER_PORT'] : '');
define('APP_CONFIG', APP_ROOT . 'config/');
define('CURRENT_ENV', 'none');
define('SCRIPTS_ROOT', dirname($_SERVER['SCRIPT_FILENAME']).'/versioning/');
// Autoloader
require(APP_ROOT . 'lib/twig/lib/Twig/Autoloader.php');
require(APP_ROOT . 'Autoloader.php');
// Session start
$session = Session::getInstance();
$session->start();
// Security nonce
$nonce  = chr(mt_rand(97, 122)) . substr(md5(time()), 1);
$params = Parameters::getInstance();
$params->set('nonce', $nonce);
// Dispatcher
$dispatcher = new Dispatcher();
$dispatcher->dispatch();