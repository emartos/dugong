<?php
/**
 * MysqlHandler.php
 * MySQL DBMS handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class MysqlHandler {
    /**
     * Configuration key
     *
     * @var string
     */
    protected $key;

    /**
     * Database hostname or IP
     *
     * @var string
     */
    protected $host;

    /**
     * Database port
     *
     * @var string
     */
    protected $port;

    /**
     * Database name
     *
     * @var string
     */
    protected $dbName;

    /**
     * Database username
     *
     * @var string
     */
    protected $user;

    /**
     * Database password
     *
     * @var string
     */
    protected $password;

    /**
     * Attempts to reconnect
     *
     * @var integer
     */
    protected $attempts;

    /**
     * Connection identifier
     *
     * @var resource
     */
    protected $connId;

    /**
     * Error message for failed script execution
     *
     * @var errMessage
     */
    public $errMessage;

    /**
     * Constant for stored procedure delimiter token
     */
    const PROC_DELIMITER_TOKEN = 'DELIMITER ';

    /**
     * Constant for stored procedure end token
     */
    const PROC_END_TOKEN = 'END';

    /**
     * Class constructor
     *
     * @param $db
     */
    public function __construct($db) {
        $params = Parameters::getInstance();
        $this->key = $db;
        $this->errMessage = '';
        $dbParams = $params->get('database')[$db];
        $this->host = $dbParams['host'];
        $this->port = $dbParams['port'];
        $this->dbName = $dbParams['db_name'];
        $this->user = $dbParams['user'];
        $this->password = $dbParams['password'];
        $this->attempts = $dbParams['attempts'];
        $this->timeout = $dbParams['timeout'];
    }

    /**
     * Try to connect to the database
     * Return true if success, otherwise return false
     *
     * @return boolean
     */
    public function connect() {
        if ($this->connId = mysqli_connect($this->host, $this->user, $this->password)) {
            $ret = mysqli_select_db($this->connId, $this->dbName);
        }
        return $ret;
    }

    /**
     * Try to close the open connection with the database
     * Return true if success, otherwise return false
     *
     * @return boolean
     */
    private function close() {
        return @mysqli_close($this->connId);
    }

    /**
     * Execute a query
     *
     * @param $query
     * @return bool|resource
     */
    public function execute($query) {
        $ret = false;
        if (!$this->connect()) {
            $this->errMessage = 'MySQL connection error: ' . mysqli_connect_error();
        } else {
            $ret = mysqli_multi_query($this->connId, $query);
            if(mysqli_more_results($this->connId)) {
                mysqli_next_result($this->connId);
            }
            $errorList = mysqli_error_list($this->connId);
            if (count($errorList)) {
                foreach ($errorList as $error) {
                    $errorArray[] = $error['errno'] . ' ' . $error['error'];
                }
                $this->errMessage = implode('<br>', $errorArray);
                $ret              = false;
            } else if (mysqli_errno($this->connId)) {
                $this->errMessage = mysqli_errno($this->connId) . ' ' . mysqli_error($this->connId);
                $ret              = false;
            } else {
                $ret = true;
            }
            if (!$this->close()) {
                $this->errMessage .= 'Error closing MySQL connection';
            }
        }
        return $ret;
    }

    /**
     * Remove the DELIMITER token if present
     * @param $text Script content
     *
     * @return array|string script text after polishing
     */
    public function polishDelimiter($text) {
        if(stripos($text, self::PROC_DELIMITER_TOKEN) === false) {
            return $text;
        }
        $token = '';
        $textArray = explode("\n",$text);
        for ($sentenceCount = 0; $sentenceCount < count($textArray); $sentenceCount++) {
            if (stripos($textArray[$sentenceCount], self::PROC_DELIMITER_TOKEN) !== false) {
                $token = trim(str_ireplace(self::PROC_DELIMITER_TOKEN, '', $textArray[$sentenceCount]));
                unset($textArray[$sentenceCount]);
            } else {
                if (stripos($textArray[$sentenceCount], self::PROC_END_TOKEN . ' ') !== false &&
                    stripos($textArray[$sentenceCount], $token) !== false) {
                    $textArray[$sentenceCount] = self::PROC_END_TOKEN;
                }
            }
        }
        $textArray = implode("\n", $textArray);
        return $textArray;
    }
}