<?php
/**
 * MetadataHandler.php
 * Error Handler for Failed script execution
 * @author Alejandro Galache
 * All Dugong code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class MetadataHandler {
    /**
     * Clear a metadata source
     * @param $scriptPath
     */
    public static function unsetMetaData($scriptPath) {
        $content = File::read($scriptPath);
        $hash = hash('md5', $content);
        $params = Parameters::getInstance();
        $content_json = File::read($params->get('dataPath') . $params->get('env') .'_metadata.json');
        $values = json_decode($content_json, true);
        unset($values[$hash]);
        File::write($params->get('dataPath') . $params->get('env') . '_metadata.json', json_encode($values));
    }

    /**
     * Set error message data for failed scripts execution
     *
     * @param $scriptPath Path of the sql script
     * @param $errorMsg error message that the query returns
     */
    public static function setMetaData($scriptPath, $errorMsg) {
        $content = File::read($scriptPath);
        $hash = hash('md5', $content);
        $date = (new DateTime())->getTimestamp();
        $params = Parameters::getInstance();
        $content_json = File::read($params->get('dataPath') . $params->get('env') .'_metadata.json');
        $values = json_decode($content_json, true);
        $values[$hash]['date'] = $date;
        $values[$hash]['errorMessage'] = $errorMsg;
        File::write($params->get('dataPath') . $params->get('env') . '_metadata.json', json_encode($values));
    }

    /**
     * Get log for failed script executions stored in metadata.json
     *
     * @return values content of metadata file
     */
    public static function getMetaData() {
        $params = Parameters::getInstance();
        $content = File::read($params->get('dataPath') . $params->get('env') . '_metadata.json');
        $values = json_decode($content, true);
        return $values;
    }
}