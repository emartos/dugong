DROP FUNCTION IF EXISTS `get_garage_module_roles`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_garage_module_roles`() RETURNS text CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE Role_Types TEXT;
    SELECT GROUP_CONCAT(Role_Type_ID) INTO Role_Types
    FROM garage_modules_template;
    RETURN Role_Types;
END |;
DELIMITER ;
