DROP FUNCTION IF EXISTS `get_unit`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_unit`(PID INT) RETURNS varchar(2) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE unit VARCHAR(2);
    CASE PID
        WHEN 7 THEN SET unit = 'mi';
    ELSE
        SET unit = 'km';
    END CASE;
    RETURN unit;
END |;
DELIMITER ;
