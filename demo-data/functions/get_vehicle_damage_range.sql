DROP FUNCTION IF EXISTS `get_vehicle_damage_range`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_vehicle_damage_range`(MAKEID INT, MODELID INT, MODELCODE VARCHAR(20), PARTNERGROUPID INT, DAMAGE INT) RETURNS text CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE Vehicle_Damage_Range TEXT DEFAULT NULL;
    SELECT CONCAT(pricing_damage_range.Damage_Min, ' - ', pricing_damage_range.Damage_Max, ';', pricing_damage_range.Value, ' ', IF(pricing_damage_range.Measurement = 0, "%", "€"), ';', pricing_damage_range.User, ';', pricing_damage_range.Model_Code) INTO Vehicle_Damage_Range
    FROM pricing_damage_range
    WHERE
        pricing_damage_range.Make_ID = MAKEID
        AND pricing_damage_range.Model_ID = MODELID
        AND (IF(pricing_damage_range.Model_Code = MODELCODE,
                pricing_damage_range.Model_Code = MODELCODE,
                IF(pricing_damage_range.Model_Code = 'all_diesel',
                    pricing_damage_range.Model_Code = 'all_diesel',
                    IF(pricing_damage_range.Model_Code = 'all_petrol',
                        pricing_damage_range.Model_Code = 'all_petrol',
                        IFNULL(pricing_damage_range.Model_Code = 'all',
            0) != 0))))
        AND pricing_damage_range.Partner_Group_ID = PARTNERGROUPID
        AND pricing_damage_range.Deleted = 0
        AND DAMAGE BETWEEN pricing_damage_range.Damage_Min AND pricing_damage_range.Damage_Max
    ORDER BY pricing_damage_range.Model_Code DESC
    LIMIT 1;
    
    RETURN IFNULL(Vehicle_Damage_Range, '-');
END |;
DELIMITER ;
