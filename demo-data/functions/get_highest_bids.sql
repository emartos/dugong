DROP FUNCTION IF EXISTS `get_highest_bids`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` FUNCTION `get_highest_bids`(BID_COUNT INT, VEHICLE_ID INT, OWNER_ID INT) RETURNS text CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE BIDS INT;
    DECLARE RETURN_VALUE TEXT;
    SELECT
        SUBSTRING_INDEX(GROUP_CONCAT(`peep_live_bids`.`Bid`, '|', `address`.`External_ID` ORDER BY `peep_live_bids`.`Bid` DESC), ',', BID_COUNT), COUNT(`peep_live_bids`.`Bid`) INTO RETURN_VALUE, BIDS
    FROM `vehicle`
        JOIN `utilisation` ON `vehicle`.`vehicle_ID` = `utilisation`.`vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` = 2
        JOIN `peep_live_bids` ON `vehicle`.`vehicle_ID` = `peep_live_bids`.`vehicle_ID` AND `utilisation`.`Auction_Number` = `peep_live_bids`.`Sale_Name_Number`
        JOIN `person` USING (Buyer_ID)
        JOIN `address_to_person` USING (Person_ID)
        JOIN `address` USING (Address_ID)
    WHERE
        `vehicle`.`Vehicle_ID` = VEHICLE_ID
        AND `vehicle`.`Owner_ID` = OWNER_ID;
        # loop to fill RETURN_VALUE with the missing bids
        label: LOOP
            IF(BIDS < BID_COUNT)THEN
                SELECT CONCAT (RETURN_VALUE, ',|') INTO RETURN_VALUE;
                SET BIDS = BIDS + 1;
                ITERATE label;
            END IF;
            LEAVE label;
        END LOOP label;
    RETURN RETURN_VALUE;
END |;
DELIMITER ;
