DROP PROCEDURE IF EXISTS `procedure_buy_back_light`;
DELIMITER |;
SOME WRONG CODE TO MAKE THE SCRIPT FAIL;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_buy_back_light`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE FARBE TEXT;

	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;

	SET FARBE = get_translation_string(LANG, 'color');

	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(vehicle.Vehicle_ID) AS 'count' ";
			SET DEFAULT_TABLES = " 
								vehicle
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
								LEFT JOIN vehicle_damage ON (vehicle.Vehicle_ID = vehicle_damage.Vehicle_ID)
								LEFT JOIN vehicle_damage vd2 ON (vehicle_damage.Vehicle_Damage_ID < vd2.Vehicle_Damage_ID AND vd2.Vehicle_ID = vehicle.Vehicle_ID)
								";
			SET DEFAULT_ROWS = "";
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
		WHEN 'list' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS STRAIGHT_JOIN ";
			SET DEFAULT_TABLES = "
								vehicle
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
								JOIN currency ON (currency.Currency_ID = vehicle.Currency_ID)
								JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
								JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
								JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
								JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
								JOIN codebase cb_color ON (color.Translation_Code = cb_color.Translation_Code)
								LEFT JOIN vehicle_reference ON (vehicle_reference.Vehicle_ID = vehicle.Vehicle_ID)
								LEFT JOIN vehicle_to_status vts1 ON (vts1.Vehicle_ID = vehicle.Vehicle_ID AND vehicle.Vehicle_Status_Actual = 4)
								LEFT JOIN vehicle_to_status vts2 ON (vts2.Vehicle_ID = vehicle.Vehicle_ID AND vehicle.Vehicle_Status_Actual = 4 AND vts1.Vehicle_To_Status_ID < vts2.Vehicle_To_Status_ID)
								LEFT JOIN vehicle_damage ON (vehicle.Vehicle_ID = vehicle_damage.Vehicle_ID)
								LEFT JOIN vehicle_damage vd2 ON (vehicle_damage.Vehicle_Damage_ID < vd2.Vehicle_Damage_ID AND vd2.Vehicle_ID = vehicle.Vehicle_ID)
								JOIN vehicle_details ON (vehicle.Vehicle_ID = vehicle_details.Vehicle_ID)
								JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle_make.Name AS Make,
								vehicle_model.Name AS Model,
								vehicle_make.Name AS Manufacturer,
								vehicle.Mileage_Actual AS Odometry,
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Unit_ID AS Unit_ID,
								vehicle.Licence_No_Actual,
								vehicle.First_Reg_Date,
								vehicle.Damage_Cost,
								vehicle.VIN,
								currency.Symbol as Currency,
								currency.Position as Currency_Pos,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
								vts1.Set_Date AS Inspection_Date,
								vehicle_damage.Further_Damage_Costs,
								vehicle_damage.Glass_Damage_Costs,
								vehicle_damage.Missing_Parts_Costs,
								vehicle_reference.Reference1 AS Missing_Inspection,
								vehicle_reference.Reference2 AS Additional_KM,
								vehicle_reference.Reference3 AS Defects,
								vehicle_reference.Reference4 AS Price_VS,
								vehicle_reference.Reference5 AS WM_VS,
								vehicle_reference.Reference6 AS WM_Defects,
								derivative.Name AS Derivative,
								",FARBE);
			SET DEFAULT_FILTER = " AND IFNULL(vts2.Vehicle_To_Status_ID, 0) = 0 ";
			SET GRUPPIERUNG = "";
		WHEN 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS STRAIGHT_JOIN ";
			SET DEFAULT_TABLES = "
								vehicle
								JOIN fleet_handling ON (vehicle.Vehicle_ID = fleet_handling.Vehicle_ID)
								JOIN currency ON (currency.Currency_ID = vehicle.Currency_ID)
								JOIN make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								JOIN vehicle_make ON (make_to_model.Make_ID = vehicle_make.Make_ID)
								JOIN vehicle_model ON (make_to_model.Model_ID = vehicle_model.Vehicle_Model_ID)
								JOIN vehicle_details ON (vehicle_details.Vehicle_ID = vehicle.Vehicle_ID)
								JOIN color_manufacturer ON (vehicle.Color_Manufacturer = color_manufacturer.Color_Manufacturer_ID)
								JOIN color_manufacturer_translation ON (color_manufacturer.Color_Manufacturer_Translation_ID = color_manufacturer_translation.Color_Manufacturer_Translation_ID)
								JOIN color ON (vehicle.Color_ID = color.Color_ID)
								JOIN codebase cb_color ON (color.Translation_Code = cb_color.Translation_Code)
								LEFT JOIN vehicle_reference ON (vehicle_reference.Vehicle_ID = vehicle.Vehicle_ID)
								LEFT JOIN vehicle_status_partner ON (vehicle.Vehicle_Status_Partner_Actual = vehicle_status_partner.Vehicle_Status_Partner_ID)
								LEFT JOIN codebase cb_status ON (vehicle_status_partner.Translation_Code = cb_status.Translation_Code)
								JOIN model_code ON (vehicle.Model_Code_ID = model_code.Model_Code_ID)
								LEFT JOIN vehicle_to_status vts1 ON (vts1.Vehicle_ID = vehicle.Vehicle_ID AND vehicle.Vehicle_Status_Actual = 4)
								LEFT JOIN vehicle_to_status vts2 ON (vts2.Vehicle_ID = vehicle.Vehicle_ID AND vehicle.Vehicle_Status_Actual = 4 AND vts1.Vehicle_To_Status_ID < vts2.Vehicle_To_Status_ID)
								LEFT JOIN vehicle_damage ON (vehicle.Vehicle_ID = vehicle_damage.Vehicle_ID)
								LEFT JOIN vehicle_damage vd2 ON (vehicle_damage.Vehicle_Damage_ID < vd2.Vehicle_Damage_ID AND vd2.Vehicle_ID = vehicle.Vehicle_ID)
								JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle_make.Name AS Make,
								vehicle_model.Name AS Model,
								vehicle_make.Name AS Manufacturer,
								vehicle.Mileage_Actual AS Odometry,
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Unit_ID AS Unit_ID,
								vehicle.Licence_No_Actual,
								vehicle.First_Reg_Date,
								vehicle.Damage_Cost,
								vehicle.VIN,
								currency.Symbol as Currency,
								currency.Position as Currency_Pos,
								IF(vehicle.Mileage_Unit_ID = 1, 'km', 'mi') AS Odometry_Unit,
								vts1.Set_Date AS Inspection_Date,
								vehicle_damage.Further_Damage_Costs,
								vehicle_damage.Glass_Damage_Costs,
								vehicle_damage.Missing_Parts_Costs,
								vehicle_reference.Reference1 AS Missing_Inspection,
								vehicle_reference.Reference2 AS Additional_KM,
								vehicle_reference.Reference3 AS Defects,
								vehicle_reference.Reference4 AS Price_VS,
								vehicle_reference.Reference5 AS WM_VS,
								vehicle_reference.Reference6 AS WM_Defects,
								derivative.Name AS Derivative,
								",FARBE);
			SET DEFAULT_FILTER = " AND IFNULL(vts2.Vehicle_To_Status_ID, 0) = 0 ";
			SET GRUPPIERUNG = "";
	END CASE;

	CASE LIST_TYPE
		WHEN 'infleeted_buy_back' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND vehicle.Vehicle_Status_Actual NOT IN (0,6,7,3,24,26)
								AND fleet_handling.Fleet_Status = 1
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								");
			END IF;
		WHEN 'inspection_completed' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND utilisation.Utilisation_Result_Type_ID = 1
								AND IF(utilisation.Sales_Category_ID = 1,IFNULL(sales_channels_to_utilisation.Utilization_ID,0) = 0, IFNULL(sales_channels_to_utilisation.Utilization_ID,0) != 0)
								AND service.Service_Status_Actual IN (0, 27)
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON utilisation.Vehicle_ID = vehicle.Vehicle_ID
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								LEFT JOIN sales_channels_to_utilisation ON sales_channels_to_utilisation.Utilization_ID = utilisation.Utilisation_ID
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								");
			END IF;
		WHEN 'above_max_damage' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND service.Service_Status_Actual = 38
								AND service.Deleted = 0
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 1)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								");
			END IF;
		WHEN 'below_max_damage' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND service.Service_Status_Actual = 39
								AND service.Deleted = 0
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 1)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								");
			END IF;
		WHEN 'clarified' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND service.Service_Status_Actual = 40
								AND service.Deleted = 0
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 1)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								");
			END IF;
		WHEN 'clarified_invoice' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND IFNULL(st2.Service_Status_ID,0) = 0
								AND module_status.Module_Status LIKE CONCAT ('%|buy_back_pro;buy_back_pro_clarified;', vehicle.Vehicle_ID, '%')
								AND service.Deleted = 0
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								JOIN service_status on (service.Service_ID = service_status.Service_ID AND service_status.Service_Status_Template_ID in (38,39))
								LEFT JOIN service_status st2 ON (st2.Service_Status_ID > service_status.Service_Status_ID AND st2.Service_ID = service_status.Service_ID AND st2.Service_Status_Template_ID in (38,39))
								JOIN module_status ON(module_status.Vehicle_ID = vehicle.Vehicle_ID)
								#JOIN vehicle_details ON (vehicle.Vehicle_ID = vehicle_details.Vehicle_ID)
								#JOIN derivative ON (vehicle_details.Derivative_ID = derivative.Derivative_ID)
								");
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								#,derivative.Name AS Derivative
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								");
			END IF;
		WHEN 'invoice_completed' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND module_status.Module_Status LIKE CONCAT ('%|buy_back_pro;buy_back_pro_invoiced;', vehicle.Vehicle_ID, '%')
								AND service.Deleted = 0
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								JOIN module_status ON(module_status.Vehicle_ID = vehicle.Vehicle_ID)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								");
			END IF;
		
		WHEN 'open_appraisal' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND service.Service_Status_Actual = 38
								AND service.Deleted = 0
								AND rental_service.Service_Status_Actual = 27
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 1)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								JOIN service rental_service ON (rental_service.Service_Vehicle_ID = vehicle.Vehicle_ID AND rental_service.Role_Type_ID = 45)
								JOIN service_status status_rental ON (rental_service.Service_ID = status_rental.Service_ID AND status_rental.Service_Status_Template_ID = 27)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								,status_rental.Comment AS Rental_Comment
								,rental_service.Service_ID AS Rental_Service_ID
								");
			END IF;
		WHEN 'accepted_appraisal' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND service.Service_Status_Actual in (38, 40)
								AND service.Deleted = 0
								AND rental_service.Service_Status_Actual in (1,28)
								");
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 1)
								JOIN service ON (service.Service_ID = utilisation.Service_ID)
								JOIN service rental_service ON (rental_service.Service_Vehicle_ID = vehicle.Vehicle_ID AND rental_service.Role_Type_ID = 45)
								JOIN service_status status_rental ON (rental_service.Service_ID = status_rental.Service_ID AND status_rental.Service_Status_Template_ID = 28)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,service.Service_ID
								,status_rental.Comment AS Rental_Comment
								,rental_service.Service_ID AS Rental_Service_ID
								");
			END IF;
		
		
	END CASE;

	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	
	SET BEDINGUNG = CONCAT ('WHERE
								FIND_IN_SET(vehicle.Owner_ID,\'',PID,'\') > 0
								AND vehicle.Deleted = 0
								AND vehicle.Vehicle_Status_Partner_Actual = 1
								AND IFNULL(vd2.Vehicle_Damage_ID, 0) = 0
							');
	IF	LIST_TYPE = 'open_appraisal' OR 
			LIST_TYPE = 'accepted_appraisal' THEN
		SET BEDINGUNG = CONCAT ('WHERE
							FIND_IN_SET (rental_service.Client_ID,\'',PID,'\') > 0
							AND vehicle.Deleted = 0
							AND vehicle.Vehicle_Status_Partner_Actual = 1
							AND IFNULL(vd2.Vehicle_Damage_ID, 0) = 0
							');
	END IF;
	
	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;	
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		#SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
