DROP PROCEDURE IF EXISTS `procedure_guide_pricing`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_guide_pricing`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE FARBE TEXT;
	DECLARE DATUM VARCHAR(15);
	DECLARE AGE_FILTER VARCHAR(255) DEFAULT " ";
	
	SELECT CONCAT("'",CURDATE(),"'") INTO DATUM;

	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG,", `vehicle`.`Licence_No_Actual` ");
	END IF;

	SET FARBE = get_translation_string(LANG, 'color');

	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(vehicle.Vehicle_ID) AS 'count' ";
			SET DEFAULT_TABLES = " 
								vehicle
								JOIN `model_code` ON (`vehicle`.`Model_Code_ID`=`model_code`.`Model_Code_ID`)
								JOIN provider_codes ON (provider_codes.Model_Code_ID = model_code.Model_Code_ID)
								LEFT JOIN provider_codes pc2 ON (pc2.Model_Code_ID = model_code.Model_Code_ID AND provider_codes.`Provider_Codes_ID` < pc2.`Provider_Codes_ID`)
								JOIN `make_to_model` ON (`vehicle`.`Make_To_Model_ID` = `make_to_model`.`Make_To_Model_ID`)
								JOIN `guide_to_partner` ON (`guide_to_partner`.`Partner_ID` = `vehicle`.`Owner_ID`)
								JOIN `partner_to_partner_group` ON (`partner_to_partner_group`.`Partner_ID` = `guide_to_partner`.`Partner_ID`)
								JOIN `vehicle_details` ON (`vehicle_details`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								JOIN `guide_configuration` ON (`guide_configuration`.`Vehicle_Type_ID` =`vehicle_details`.`Vehicle_Type_ID` and `guide_configuration`.`Partner_Group_ID` =`partner_to_partner_group`.`Partner_Group_ID`)
								";
			SET DEFAULT_ROWS = "";
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
		WHEN 'list' OR 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS STRAIGHT_JOIN ";
			SET DEFAULT_TABLES = "
								vehicle
								JOIN `model_code` ON (`vehicle`.`Model_Code_ID`=`model_code`.`Model_Code_ID`)
								JOIN provider_codes ON (provider_codes.Model_Code_ID = model_code.Model_Code_ID)
								LEFT JOIN provider_codes pc2 ON (pc2.Model_Code_ID = model_code.Model_Code_ID AND provider_codes.`Provider_Codes_ID` < pc2.`Provider_Codes_ID`)
								JOIN `make_to_model` ON (`vehicle`.`Make_To_Model_ID` = `make_to_model`.`Make_To_Model_ID`)
								JOIN `vehicle_make` ON (`make_to_model`.`Make_ID` = `vehicle_make`.`Make_ID`)
								JOIN `vehicle_model` ON (`make_to_model`.`Model_ID` = `vehicle_model`.`Vehicle_Model_ID`)
								JOIN `guide_to_partner` ON (`guide_to_partner`.`Partner_ID` = `vehicle`.`Owner_ID`)
								JOIN `partner_to_partner_group` ON (`partner_to_partner_group`.`Partner_ID` = `guide_to_partner`.`Partner_ID`)
								JOIN `vehicle_details` ON (`vehicle_details`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								JOIN `guide_configuration` ON (`guide_configuration`.`Vehicle_Type_ID` =`vehicle_details`.`Vehicle_Type_ID` and `guide_configuration`.`Partner_Group_ID` =`partner_to_partner_group`.`Partner_Group_ID`)
								JOIN currency ON (currency.Currency_ID = vehicle.Currency_ID)
								";
			SET DEFAULT_ROWS = CONCAT("
								vehicle_make.Name AS Manufacturer,
								vehicle_model.Name AS Model,
								model_code.Model_Code,
								model_code.Assumed_Age,
								model_code.Assumed_Odometry,
								model_code.Model_Code_ID,
								provider_codes.Provider_Codes_ID,
								provider_codes.Provider_Code,
								currency.Symbol AS Currency,
								currency.Position AS Currency_Pos,
								IFNULL(provider_codes.Model_Year,0) AS Model_Year
								");
			SET DEFAULT_FILTER = "";
			SET GRUPPIERUNG = "";
	END CASE;

	CASE LIST_TYPE
		WHEN 'model_code_req_guide_price' THEN
			SET GRUPPIERUNG = "GROUP BY model_code.Model_Code_ID , guide_price_to_model_code.Guide_Price_To_Model_Code_ID , provider_codes.Provider_Code";
			SET DEFAULT_FILTER = "
								AND ((`guide_price_to_model_code`.`Active` != 1 and `guide_price_to_model_code`.`Active` != 0) or `guide_price_to_model_code`.`Active` is null)
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_model_code` ON (`guide_price_to_model_code`.`Model_Code_ID` = `model_code`.`Model_Code_ID` AND `guide_price_to_model_code`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_model_code`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_model_code.Guide_Price,
								guide_price_to_model_code.Guide_Price_To_Model_Code_ID,
								CONCAT(model_code.Model_Code_ID, provider_codes.Provider_Code) AS Unique_ID
								");
			END IF;
		WHEN 'model_code_with_guide_price' THEN
			SET GRUPPIERUNG = "GROUP BY model_code.Model_Code_ID , guide_price_to_model_code.Guide_Price_To_Model_Code_ID , provider_codes.Provider_Code";
			SET DEFAULT_FILTER = "
								AND `guide_price_to_model_code`.`Active` = 1 
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_model_code` ON (`guide_price_to_model_code`.`Model_Code_ID` = `model_code`.`Model_Code_ID` AND `guide_price_to_model_code`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_model_code`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_model_code.Guide_Price,
								guide_price_to_model_code.Guide_Price_To_Model_Code_ID,
								CONCAT(model_code.Model_Code_ID, provider_codes.Provider_Code) AS Unique_ID
								");
			END IF;	
		WHEN 'defleet_req_guide_price' THEN
			IF PID = 2 THEN
				SET AGE_FILTER = "AND DATE_SUB(SUBDATE(ADDDATE(CURDATE(), INTERVAL 1 MONTH), INTERVAL DAYOFMONTH(CURDATE()) DAY), INTERVAL (IFNULL (guide_configuration.Min_Age,12)) MONTH) >= First_Reg_Date";
			ELSE
				SET AGE_FILTER = "AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)";
			END IF;
			SET DEFAULT_FILTER = CONCAT("
								AND IFNULL(guide_price_to_vehicle.Active, 3) IN (2,3)
								AND fleet_handling.Fleet_Status = 2
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								",AGE_FILTER);
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								JOIN partner ON partner.partner_ID = vehicle.Owner_ID
								JOIN address ON address.Address_ID = partner.Address_ID
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
								ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
	WHEN 'all_req_guide_price' THEN
			IF PID = 2 THEN
				SET AGE_FILTER = "AND DATE_SUB(SUBDATE(ADDDATE(CURDATE(), INTERVAL 1 MONTH), INTERVAL DAYOFMONTH(CURDATE()) DAY), INTERVAL (IFNULL (guide_configuration.Min_Age,12)) MONTH) >= First_Reg_Date";
			ELSE
				SET AGE_FILTER = "AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)";
			END IF;
			SET DEFAULT_FILTER = CONCAT("
								AND IFNULL(guide_price_to_vehicle.Active, 3) IN (2,3)
								AND fleet_handling.Fleet_Status IN (1,2)
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								",AGE_FILTER);
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								JOIN partner ON partner.partner_ID = vehicle.Owner_ID
								JOIN address ON address.Address_ID = partner.Address_ID
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
								ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
		WHEN 'defleet_with_guide_price' THEN
			IF PID = 2 THEN
				SET AGE_FILTER = "AND DATE_SUB(SUBDATE(ADDDATE(CURDATE(), INTERVAL 1 MONTH), INTERVAL DAYOFMONTH(CURDATE()) DAY), INTERVAL (IFNULL (guide_configuration.Min_Age,12)) MONTH) >= First_Reg_Date";
			ELSE
				SET AGE_FILTER = "AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)";
			END IF;
			SET DEFAULT_FILTER = CONCAT("
								AND guide_price_to_vehicle.Active = 1
								AND fleet_handling.Fleet_Status = 2
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								",AGE_FILTER);
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								JOIN partner ON partner.partner_ID = vehicle.Owner_ID
								JOIN address ON address.Address_ID = partner.Address_ID
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
								ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
	WHEN 'all_with_guide_price' THEN
			IF PID = 2 THEN
				SET AGE_FILTER = "AND DATE_SUB(SUBDATE(ADDDATE(CURDATE(), INTERVAL 1 MONTH), INTERVAL DAYOFMONTH(CURDATE()) DAY), INTERVAL (IFNULL (guide_configuration.Min_Age,12)) MONTH) >= First_Reg_Date";
			ELSE
				SET AGE_FILTER = "AND TIMESTAMPDIFF(MONTH,`vehicle`.`First_Reg_Date`,CURDATE()) >IFNULL (`guide_configuration`.`Min_Age`,12)";
			END IF;
			SET DEFAULT_FILTER = CONCAT("
								AND guide_price_to_vehicle.Active = 1
								AND fleet_handling.Fleet_Status IN (1,2)
								AND IFNULL(utilisation.Utilisation_result_Type_ID,'0') != '2'
								AND vehicle.Vehicle_Status_Actual NOT IN (3,6,7)
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								",AGE_FILTER);
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								JOIN partner ON partner.partner_ID = vehicle.Owner_ID
								JOIN address ON address.Address_ID = partner.Address_ID
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
								ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
		WHEN 'sold_without_guide_price' THEN
			SET DEFAULT_FILTER = "
								AND IFNULL(guide_price_to_vehicle.Active, 3) = 3
								AND vehicle.Vehicle_Status_Partner_Actual != 1 
								";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` = 2)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								utilisation.Sold_Date,
								utilisation.Sales_Price,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								currency.Symbol,
								currency.Position,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
		WHEN 'sold_with_guide_price' THEN
			SET DEFAULT_FILTER = "
								AND guide_price_to_vehicle.Active = 1
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` = 2)
								JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								");
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								utilisation.Sold_Date,
								utilisation.Sales_Price,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price,
								currency.Symbol,
								currency.Position
								");
			END IF;

	 WHEN 'awaiting_guide_price_request' THEN 
	 	 SET DEFAULT_FILTER = "
								AND partner.Pricing > 0
								AND IFNULL(guide_price_to_vehicle.Active, 3) IN (2,3)
								AND IFNULL(guide_price_to_vehicle.Auto_Guide_Price_Status, 0) < 2		/* Nicht durch fehler deaktiviert*/
								AND vehicle.Vehicle_Status_Actual NOT IN (3 , 6, 7)		/* inactive ausschliessen*/
								AND IFNULL(utilisation.Utilisation_Result_Type_ID,0) NOT IN (2,4,5,6)		/*im Verkauf befindliche cars*/
								AND vehicle.Vehicle_Status_Partner_Actual != 1
								# The rest of where condition done in script side
								";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN partner ON partner.Partner_Id = vehicle.Owner_ID
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								JOIN address ON address.Address_ID = partner.Address_ID
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
		IF CALL_TYPE != 'count' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.First_Reg_Date,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Unit_ID,
								vehicle.Vehicle_ID,
								guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
                ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								CONCAT(vehicle.Vehicle_ID, provider_codes.Provider_Code) AS Unique_ID,
								guide_price_to_vehicle.Auto_Guide_Price
								");
			END IF;
			
	WHEN 'failed_guide_price_request' THEN
		SET DEFAULT_FILTER = "
							AND partner.Pricing > 0
							AND guide_price_to_vehicle.Active IN (2,3)
							AND guide_price_to_vehicle.Auto_Guide_Price_Status > 1
							AND guide_price_to_vehicle.Auto_Guide_Price IN (1,4)
							AND vehicle.Vehicle_Status_Actual NOT IN (3 , 6, 7)		/* inactive ausschliessen*/
							AND IFNULL(utilisation.Utilisation_Result_Type_ID,0) NOT IN (2,4,5,6)		/*im Verkauf befindliche cars*/
							AND vehicle.Vehicle_Status_Partner_Actual != 1
							# The rest of where condition done in script side 
							";
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN partner ON partner.Partner_Id = vehicle.Owner_ID
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								");
		IF CALL_TYPE != 'count' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, " 
								,vehicle.Vehicle_ID,
								vehicle.Licence_No_Actual,
								vehicle.Frontend_Edited,
								if(fleet_handling.Fleet_Status=1,'menu_infleet','menu_defleet')AS Fleet_Status,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								guide_price_to_vehicle.Auto_Guide_Price_Status,
								guide_price_to_vehicle.Auto_Guide_Price_Error
								");
			END IF;

		WHEN 'guide_price_manual' THEN
		SET DEFAULT_FILTER = "
							AND partner.Pricing > 0
							AND guide_price_to_vehicle.Active IN (2,3)
							AND guide_price_to_vehicle.Auto_Guide_Price_Status > 1
							AND guide_price_to_vehicle.Auto_Guide_Price IN (1,4)
							AND vehicle.Vehicle_Status_Actual NOT IN (3 , 6, 7)		/* inactive ausschliessen*/
							AND IFNULL(utilisation.Utilisation_Result_Type_ID,0) NOT IN (2,4,5,6)		/*im Verkauf befindliche cars*/
							AND vehicle.Vehicle_Status_Partner_Actual != 1
							";
		SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES, "
								JOIN partner ON partner.Partner_Id = vehicle.Owner_ID
								JOIN address ON address.Address_ID = partner.Address_ID
								JOIN `fleet_handling` ON (`fleet_handling`.`Vehicle_ID` = `vehicle`.`Vehicle_ID`)
								LEFT JOIN `utilisation` ON (`utilisation`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `utilisation`.`Utilisation_Result_Type_ID` != 4)
								LEFT JOIN `guide_price_to_model_code` ON (`guide_price_to_model_code`.`Model_Code_ID` = `model_code`.`Model_Code_ID` AND `guide_price_to_model_code`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_price_to_vehicle` ON (`guide_price_to_vehicle`.`Vehicle_ID` = `vehicle`.`Vehicle_ID` AND `guide_price_to_vehicle`.`Provider_Codes_ID` = `provider_codes`.`Provider_Codes_ID`)
								LEFT JOIN `guide_edition` ON (`guide_price_to_vehicle`.`Guide_Edition_ID` = `guide_edition`.`Guide_Edition_ID`)
								LEFT JOIN taxes on taxes.Country_ID = address.Country_ID
								");
		IF CALL_TYPE != 'count' THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS, "
								,guide_edition.Edition_Name,
								guide_edition.Guide_Edition_ID,
								guide_price_to_model_code.Guide_Price,
								guide_price_to_model_code.Guide_Price_To_Model_Code_ID,
								vehicle.Vehicle_ID,
								vehicle.First_Reg_Date,
								vehicle.Unit_ID,
								vehicle.Mileage_Actual,
								vehicle.Licence_No_Actual,
								vehicle.Frontend_Edited,
								if(fleet_handling.Fleet_Status=1,'menu_infleet','menu_defleet')AS Fleet_Status,
								guide_price_to_vehicle.Guide_Price_To_Vehicle_ID,
								guide_price_to_vehicle.Auto_Guide_Price_Status,
								guide_price_to_vehicle.Auto_Guide_Price_Error,
								guide_price_to_vehicle.Auto_Guide_Price,
								guide_price_to_vehicle.Guide_Price AS Guide_Price_Gross,
								ROUND((guide_price_to_vehicle.Guide_Price * 100) / (100+ taxes.Value),2) AS Guide_Price_Net
								");
		END IF;
	END CASE;

	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	SET BEDINGUNG = CONCAT  ('WHERE
								FIND_IN_SET(vehicle.Owner_ID,\'',PID,'\') > 0
								AND vehicle.Deleted = 0
								AND vehicle.Stolen = 0
								AND vehicle.Wrecked = 0
								AND IFNULL(pc2.`Provider_Codes_ID`, 0) = 0
								 ');

	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);
	
	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;	
	
	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN 
		# SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
