DROP PROCEDURE IF EXISTS `procedure_document_management`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_document_management`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2), GROUPBY VARCHAR(255))
BEGIN
	# Variablen definieren
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;
	DECLARE DATUM VARCHAR(15);

	SELECT CONCAT("'",CURDATE(),"'") INTO DATUM;

	# Wenn kein Filter dann leer, sonst angegebenen Filter nutzen
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	
	# Wenn keine Sortierung dann leer, sonst ORDER BY einfuegen
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;

	SET DEFAULT_ROWS = "";
	SET DEFAULT_FILTER = "";
	SET GRUPPIERUNG = "";

	CASE CALL_TYPE
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(DISTINCT vehicle.Vehicle_ID) AS 'count' ";
			SET DEFAULT_TABLES = " workflow_to_vehicle 
								join vehicle ON (workflow_to_vehicle.Vehicle_ID = vehicle.Vehicle_ID)
							    join utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 2)
							    join workflow ON (workflow.Workflow_ID = workflow_to_vehicle.Workflow_ID)
							    join service_to_workflow ON (service_to_workflow.Workflow_ID = workflow.Workflow_ID)
							    join service ON (service.Service_ID = service_to_workflow.Service_ID)
							    join document_dispatch ON (document_dispatch.Service_ID = service.Service_ID)
							    left join document_dispatch_deregisterer ON (document_dispatch_deregisterer.Document_Dispatch_ID = document_dispatch.Document_Dispatch_ID)
							    join country ON (vehicle.First_Registration_Country = country.Country_ID)
							    join make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
							    join vehicle_make ON (vehicle_make.Make_ID = make_to_model.Make_ID)
							    join vehicle_model ON (vehicle_model.Vehicle_Model_ID = make_to_model.Model_ID)
								JOIN partner ON (partner.Partner_ID = vehicle.Owner_ID)
								JOIN partner AS pd ON (pd.Partner_ID = document_dispatch_deregisterer.Partner_ID)";
			SET GRUPPIERUNG = "";
		WHEN 'list' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_ROWS = CONCAT("
								'-' AS Buyer_Name,
								'-' AS Buyer_Type,
								'-' AS Buyer_Company,
								partner.Name AS partner,
								service.Set_Date,
								service.Service_ID,
								vehicle_make.Name AS manufacturer,
								vehicle.VIN,
								vehicle.Licence_No_Actual,
								vehicle.Vehicle_ID,
								utilisation.Sales_Price,
								pd.Name,
								document_dispatch.Document_Dispatch_ID,
								document_dispatch.Destination_Address_ID,
								document_dispatch.Destination_ID,
								document_dispatch.Destination_Type,
								vehicle_model.Name AS vehicle_type
								");
			SET DEFAULT_TABLES = "
								workflow_to_vehicle
								join vehicle ON (workflow_to_vehicle.Vehicle_ID = vehicle.Vehicle_ID)
								join utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 2)
								join workflow ON (workflow.Workflow_ID = workflow_to_vehicle.Workflow_ID)
								join service_to_workflow ON (service_to_workflow.Workflow_ID = workflow.Workflow_ID)
								join service ON (service.Service_ID = service_to_workflow.Service_ID)
								join document_dispatch ON (document_dispatch.Service_ID = service.Service_ID)
								left join document_dispatch_deregisterer ON (document_dispatch_deregisterer.Document_Dispatch_ID = document_dispatch.Document_Dispatch_ID)
								join country ON (vehicle.First_Registration_Country = country.Country_ID)
								join make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								join vehicle_make ON (vehicle_make.Make_ID = make_to_model.Make_ID)
								join vehicle_model ON (vehicle_model.Vehicle_Model_ID = make_to_model.Model_ID)
								JOIN partner ON (partner.Partner_ID = vehicle.Owner_ID)
								JOIN partner AS pd ON (pd.Partner_ID = document_dispatch_deregisterer.Partner_ID)
								JOIN color_manufacturer ON (color_manufacturer.color_manufacturer_id = vehicle.color_manufacturer)
								JOIN color_manufacturer_translation ON (color_manufacturer_translation.color_manufacturer_translation_id = color_manufacturer.color_manufacturer_translation_id)
								JOIN color c ON (vehicle.Color_ID = c.Color_ID)
								JOIN codebase AS cb_color ON (cb_color.translation_code = c.translation_code)
								";
		WHEN 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_ROWS = CONCAT("
								vehicle.Vehicle_ID AS Vehicle_ID,
								vehicle.Unit_ID AS Unit_ID,
								utilisation.Utilisation_ID AS Utilisation_ID,
								service.Service_ID,
								CONCAT(vehicle_make.Name,' ',vehicle_model.Name) AS Vehicle,
								-- vehicle.First_Reg_Date AS First_Reg_Date,
								utilisation.Sold_Date AS Sold_Date,
								utilisation.Sales_Price AS Sold_Price
								");
			SET DEFAULT_TABLES = "
								workflow_to_vehicle
								join vehicle ON (workflow_to_vehicle.Vehicle_ID = vehicle.Vehicle_ID)
								join utilisation ON (utilisation.Vehicle_ID = vehicle.Vehicle_ID AND utilisation.Utilisation_Result_Type_ID = 2)
								join workflow ON (workflow.Workflow_ID = workflow_to_vehicle.Workflow_ID)
								join service_to_workflow ON (service_to_workflow.Workflow_ID = workflow.Workflow_ID)
								join service ON (service.Service_ID = service_to_workflow.Service_ID)
								join document_dispatch ON (document_dispatch.Service_ID = service.Service_ID)
								left join document_dispatch_deregisterer ON (document_dispatch_deregisterer.Document_Dispatch_ID = document_dispatch.Document_Dispatch_ID)
								join country ON (vehicle.First_Registration_Country = country.Country_ID)
								join make_to_model ON (make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID)
								join vehicle_make ON (vehicle_make.Make_ID = make_to_model.Make_ID)
								join vehicle_model ON (vehicle_model.Vehicle_Model_ID = make_to_model.Model_ID)
								JOIN partner ON (partner.Partner_ID = vehicle.Owner_ID)
								JOIN partner AS pd ON (pd.Partner_ID = document_dispatch_deregisterer.Partner_ID)
								JOIN color_manufacturer ON (color_manufacturer.color_manufacturer_id = vehicle.color_manufacturer)
								JOIN color_manufacturer_translation ON (color_manufacturer_translation.color_manufacturer_translation_id = color_manufacturer.color_manufacturer_translation_id)
								JOIN color c ON (vehicle.Color_ID = c.Color_ID)
								JOIN codebase AS cb_color ON (cb_color.translation_code = c.translation_code))
								";
	END CASE;

	CASE LIST_TYPE
		WHEN 'awaiting_bill' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (21)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE 
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (7)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								");
		WHEN 'awaiting_signature' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (21)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (7)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (8)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								
								");
		WHEN 'bill_to_check_out' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (8)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (11)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
							
								");
		WHEN 'awaiting_final_send' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (21)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (11)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM  service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND NOT (EXISTS(SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (10)
										AND NOT EXISTS( SELECT  service_status_resolved.Service_Status_Resolved_ID
														FROM service_status_resolved
														WHERE
															service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (25)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID)))
								");
		WHEN 'finaly_sent' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND NOT EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (21)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND EXISTS( SELECT service_status.Service_Status_ID
												FROM service_status
												WHERE
													service_status.Service_ID = service.Service_ID
													AND service_status.Service_Status_Template_ID IN (10)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (25)
								AND NOT EXISTS( SELECT  service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
													service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								");
		WHEN 'cancelled_order' THEN
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
								AND EXISTS( SELECT service_status.Service_Status_ID
											FROM service_status
											WHERE
												service_status.Service_ID = service.Service_ID
												AND service_status.Service_Status_Template_ID IN (21)
								AND NOT EXISTS( SELECT service_status_resolved.Service_Status_Resolved_ID
												FROM service_status_resolved
												WHERE
												service_status.Service_Status_ID = service_status_resolved.Service_Status_ID))
								");
		WHEN 'all_vehicles' THEN
			SET DEFAULT_FILTER = DEFAULT_FILTER;
	END CASE;

	# Tabellen zusammensetzen
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
	# Bedingung fuer alle abfragen dieser Liste sind gleich
	SET BEDINGUNG = CONCAT ('WHERE
								service.Deleted = 0
								AND vehicle.Deleted = 0
								AND ( FIND_IN_SET(service.Partner_ID,\'',PID,'\') > 0
									OR service.Client_ID IN (\'',PID,'\')
									OR IFNULL(document_dispatch_deregisterer.Partner_ID,\'0\') IN (\'',PID,'\'))
							');

	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);
	
	IF GROUPBY != "" THEN
		SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,", vehicle_make.Name as Manufacturer_Name ");
		IF GRUPPIERUNG != "" THEN
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",
				REPLACE(GRUPPIERUNG,"GROUP BY", "distinct"),") as manufacturer_model_count,
								",GROUPBY," as manufacturer_model_field ");

		else
			SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS," ,count(",GROUPBY,") as manufacturer_model_count, 
								",GROUPBY," as manufacturer_model_field ");
		END IF;
		SET GRUPPIERUNG = CONCAT(" GROUP BY ",GROUPBY);
	END IF;	

	# Limit setzen
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	# Query zusammensetzen
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
		# ZUM DEBUGGING NAECHSTE ZEILE AUSKOMMENTIEREN
		#SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	# Query vorbereiten
	PREPARE stmt FROM @sql;
	# Query ausfuehren
	EXECUTE stmt;
	# Query aufheben
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
