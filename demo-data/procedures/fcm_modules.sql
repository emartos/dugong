DROP PROCEDURE IF EXISTS `procedure_fcm_modules`;
DELIMITER |;
CREATE DEFINER=`routines`@`localhost` PROCEDURE `procedure_fcm_modules`(CALL_TYPE TEXT, LIST_TYPE VARCHAR(255), PID VARCHAR(255), FILTER TEXT, SORTIERUNG TEXT, LIM VARCHAR(255), LANG VARCHAR(2))
BEGIN
	-- declare variables
	DECLARE DEFAULT_CALL_TYPE TEXT;
	DECLARE DEFAULT_TABLES TEXT;
	DECLARE DEFAULT_ROWS TEXT;
	DECLARE DEFAULT_FILTER TEXT;
	DECLARE TABELLEN TEXT;
	DECLARE BEDINGUNG TEXT;
	DECLARE GRUPPIERUNG VARCHAR(255) DEFAULT " ";
	DECLARE BEINHALTET VARCHAR(255) DEFAULT " ";
	DECLARE BEGRENZUNG TEXT;

	-- custom filter from logic/frontend
	IF LENGTH(FILTER) < 4 THEN SET FILTER = "";
	END IF;
	-- order by
	IF SORTIERUNG = '0' THEN SET SORTIERUNG = "";
	ELSE SET SORTIERUNG = CONCAT(" ORDER BY ",SORTIERUNG," ");
	END IF;

	-- initialize default values
	SET DEFAULT_ROWS = "";
	SET DEFAULT_FILTER = "";
	SET GRUPPIERUNG = "";

	CASE CALL_TYPE
		-- count case for tracking
		WHEN 'count' THEN
			SET DEFAULT_CALL_TYPE = "SELECT COUNT(module.Module_ID) AS 'count' ";
			SET DEFAULT_TABLES = "
				module
				JOIN menu_to_module ON(module.Module_ID = menu_to_module.Module)
				JOIN module AS menu ON(menu_to_module.Menu = menu.Module_ID)
			";
			SET GRUPPIERUNG = "";
			-- list case
		WHEN 'list' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_ROWS = CONCAT("
				module.Translation_Code AS Module_Name,
				module.Class_Name,
				module.Module_Number,
				module.Module_ID,
				menu_to_module.Favourite,
				menu_to_module.Visible,
				menu.Translation_Code AS Menu_Name,
				menu.Class_Name AS Menu_Class_Name,
				menu.Module_Number AS Menu_Number,
				module.Image_Home
			");
			SET DEFAULT_TABLES = "
				module
				JOIN menu_to_module ON(module.Module_ID = menu_to_module.Module)
				JOIN module AS menu ON(menu_to_module.Menu = menu.Module_ID)
			";
			SET GRUPPIERUNG = "";
			-- freetext case, not neccassary if freetext dynamic is in use
		WHEN 'freetext' THEN
			SET DEFAULT_CALL_TYPE = "SELECT SQL_CALC_FOUND_ROWS ";
			SET DEFAULT_ROWS = CONCAT("
				module.Translation_Code AS Module_Name,
				module.Class_Name,
				module.Module_Number,
				module.Module_ID,
				menu_to_module.Favourite,
				menu_to_module.Visible,
				menu.Translation_Code AS Menu_Name,
				menu.Class_Name AS Menu_Class_Name,
				menu.Module_Number AS Menu_Number,
				module.Image_Home
			");
			SET DEFAULT_TABLES = "
				module
				JOIN menu_to_module ON(module.Module_ID = menu_to_module.Module)
				JOIN module AS menu ON(menu_to_module.Menu = menu.Module_ID)
			";
	END CASE;

	CASE LIST_TYPE
		-- modules
		WHEN 'modules' THEN
			-- special columns
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"

				");
			END IF;
			-- additional tables
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"

			");
			-- custom list filter
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"

			");
		-- partner channels
		WHEN 'partner_sales_channels' THEN
			-- special columns
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
					,sales_channels.Translation_Code AS Module_Name
				");
			END IF;
			-- additional tables
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"
				JOIN sales_channels ON(sales_channels.Module_ID=module.Module_ID)
				JOIN partner_sales_channels ON(partner_sales_channels.Sales_Channel_ID=sales_channels.Sales_Channels_ID)
			");
			-- custom list filter
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
				AND FIND_IN_SET(partner_sales_channels.Partner_ID,\'",PID,"\') > 0
			");
		-- user channels
		WHEN 'user_sales_channels' THEN
			-- special columns
			IF CALL_TYPE != 'count' THEN
				SET DEFAULT_ROWS = CONCAT(DEFAULT_ROWS,"
					,sales_channels.Translation_Code AS Module_Name,
					user_to_sales_channel.Read_Only
				");
			END IF;
			-- additional tables
			SET DEFAULT_TABLES = CONCAT(DEFAULT_TABLES,"
				JOIN sales_channels ON(sales_channels.Module_ID=module.Module_ID)
				JOIN user_to_sales_channel ON(user_to_sales_channel.Sales_Channel_ID=sales_channels.Sales_Channels_ID)
			");
			-- custom list filter
			SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER,"
				AND FIND_IN_SET(user_to_sales_channel.User_ID,\'",PID,"\') > 0
				AND user_to_sales_channel.Deleted = 0
			");
	END CASE;

	-- concat tables
	SET TABELLEN = CONCAT("FROM ", DEFAULT_TABLES);
-- example for vehicle tables
	SET BEDINGUNG = CONCAT ('WHERE
			1=1
		');
	SET DEFAULT_FILTER = CONCAT(DEFAULT_FILTER, FILTER);

	-- limit
	IF LIM != '0' THEN SET BEGRENZUNG = CONCAT(" LIMIT ",LIM);
	ELSE SET BEGRENZUNG = '';
	END IF;
	-- build query
	SET @sql = CONCAT(DEFAULT_CALL_TYPE,DEFAULT_ROWS,TABELLEN,BEDINGUNG,DEFAULT_FILTER,GRUPPIERUNG,BEINHALTET,SORTIERUNG,BEGRENZUNG);
	-- debugging, makes only query string after call. to display and repair/optimize query
	-- SET @sql = CONCAT('SELECT "',@sql,'" AS QUERY;');
	-- prepare query
	PREPARE stmt FROM @sql;
	-- execute
	EXECUTE stmt;
	-- deallocate
	DEALLOCATE PREPARE stmt;
END |;
DELIMITER ;
