<?php
/**
 * Autoloader.php
 * Autoloader class
 * @author Eduardo Martos <emartos@natiboo.es> - emartos@natiboo.es
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Autoloader {
    /**
     * List of folders where classes may be stored
     * @var array
     */
    protected static $folders = array(
        'classes/',
        'classes/exceptions/',
        'mvc/controller/',
        'mvc/controller/entities/',
        'mvc/model/',
        'mvc/view/',
        'lib/',
    );

    /**
     * Load the class attending to the folders waterfall
     * @param string $class
     */
    public static function loader($class) {
        $appRoot = defined('APP_ROOT') ? APP_ROOT : __DIR__ . '/';
        $ext = '.php';
        foreach (self::$folders as $folder) {
            if ($ret = is_readable($appRoot . $folder . $class . $ext)) {
                require_once($appRoot . $folder . $class . $ext);
                break;
            }
        }
    }
}
spl_autoload_register('Autoloader::loader');